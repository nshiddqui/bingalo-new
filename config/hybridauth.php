<?php

use Cake\Core\Configure;

return [
    'HybridAuth' => [
        'providers' => [
            'Google' => [
                'enabled' => true,
                'keys' => [
                    'id' => '869511632481-9rn2rtdgjm3fuvb6lbn8n5nqcuuk6uj2.apps.googleusercontent.com',
                    'secret' => 'GxjgsdZpje_A78phj5BZJHZF'
                ]
            ],
            'Facebook' => [
                'enabled' => true,
                'keys' => [
                    'id' => '132300071391277',
                    'secret' => '09c09b96a11bfc0da6d4b90786a49a1f'
                ],
                'scope' => 'email'
            ],
            'Twitter' => [
                'enabled' => true,
                'keys' => [
                    'key' => 'L0DVT1zigEivAuxFxufQ3KPj4',
                    'secret' => 'XoznP9wEvNu5v0IlrKjgcZmuVH5Cr5NP2qjj5M8Q98WDOOv8HZ'
                ],
                'includeEmail' => true // Only if your app is whitelisted by Twitter Support
            ],
            "Apple" => [
                "enabled" => true,
                "keys" => [
                    "id" => "com.bingalollc.service.app",
                    "team_id" => "4W3GG54XTU",
                    "key_id" => "SW7GD268SL",
                    "key_content" => "-----BEGIN PRIVATE KEY-----\nMIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgmn+Evl4IWBQ2S9/Mp2oUfHB4ncVTsdmHiNyzXYtEdfugCgYIKoZIzj0DAQehRANCAAQQLymY0XwyHZfZvmp75Xt3BTR5YfPW8o482uTGqyWs4iclstSdvfp4gEEUZbRs064y/pRm0J0kvFgpm6iH4kj8\n-----END PRIVATE KEY-----"
                ],
                "scope" => "name email",
                "verifyTokenSignature" => true
            ]
        ],
        'debug_mode' => Configure::read('debug'),
        'debug_file' => LOGS . 'hybridauth.log',
    ]
];
