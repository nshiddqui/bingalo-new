$(function () {
    loadElement();
});
$(document).ajaxComplete(function () {
    loadElement();
});

function loadElement() {
    //Initialize Select2 Elements
    //Initialize Select2 Elements
    $('select.select2').select2({width: '100%'});
    $('input[datepicker]').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
    $('input[datetimepicker]').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
    $('[fancybox]').fancybox();
}