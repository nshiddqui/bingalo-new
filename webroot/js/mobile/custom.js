$(document).ready(function () {

    //MENU START
    $('.mobile-menu-button').on('click', function () {
        $('.menu-content').removeClass('hide-menu');
    });
    $('.menu-content .close-btn').on('click', function () {
        $('.menu-content').addClass('hide-menu');
    });
    //MENU END

    //LOGIN START
    $('.account-form .login-btn').on('click', function () {
        $('.signup-popup').addClass('hide-popup-box');
        $('.login-popup').removeClass('hide-popup-box');
    });
    $('.login-popup .close-btn').on('click', function () {
        $('.login-popup').addClass('hide-popup-box');
    });
    //LOGIN END

    //SIGNUP START
    $('.account-form .signup-btn').on('click', function () {
        $('.login-popup').addClass('hide-popup-box');
        $('.signup-popup').removeClass('hide-popup-box');
    });
    $('.signup-popup .close-btn').on('click', function () {
        $('.signup-popup').addClass('hide-popup-box');
    });
    //SIGNUP END


    //FORGET START
    $('.forget-password .forget-btn').on('click', function () {
        $('.login-popup').addClass('hide-popup-box');
        $('.signup-popup').addClass('hide-popup-box');
        $('.forget-password').removeClass('hide-popup-box');
    });
    $('.forget-content .close-forget').on('click', function () {
        $('.forget-content').addClass('hide-popup-box');
    });
    //FORGET END


    //FILTER START
    $('.filter-container').on('click', function () {
        $('.filter-content').removeClass('hide-popup-box');
    });
    $('.filter-content .close-filter').on('click', function () {
        $('.filter-content').addClass('hide-popup-box');
    });
    //FILTER END

    //LOCAION START
    $('.location-box').on('click', function () {
        $('.location-content').removeClass('hide-popup-box');
    });
    $('.location-content .close-location').on('click', function () {
        $('.location-content').addClass('hide-popup-box');
    });
    //LOCATION END

    //NOTIFICATION SART
    $('.app-install .close-btn').on('click', function () {
        $('.site-notification').slideToggle();
        $('.site-notification').parent('.main-container').css('margin-top', '0');
    });
    //NOTIFICATION END

    //product popup
    // $('.img-icon .popup-product').on('click', function() {
    //     $('.product-menu').slideToggle();

    // });

    $(".more-dropdown-btn").on('click', function () {
        $(".more-btn-menu").removeClass("hide-popup-box");
    })
    $(".close-more-dropdown").on('click', function () {
        $(".more-btn-menu").addClass("hide-popup-box");
    })

    $(".upload-dropdown-btn").on('click', function () {
        $(".upload-btn-menu").removeClass("hide-popup-box");
    })
    $(".close-upload-dropdown").on('click', function () {
        $(".more-btn-menu").addClass("hide-popup-box");
        $(".upload-btn-menu").addClass("hide-popup-box");
    })

    //product popup

    //BUMB
    $('.bump-item .close-btn').on('click', function () {
        $('.bump').slideToggle();
    });
    //BUMB

    $('.bump-btn').on('click', function () {
        $('.sell-item').removeClass('hide-popup-box');
    });

    $('.sell-item .popup-header img').on('click', function () {
        $('.sell-item').addClass('hide-popup-box');
    });

    $('.package-btn').on('click', function () {
        if ($('.sell-item .slider .slider-box.selected').length === 1) {
            $('.checkout-popup').removeClass('hide-popup-box');
        } else {
            alert('Please select a package');
        }
    });

    $('.promot-btn').on('click', function () {
        $('.promoting-popup').removeClass('hide-popup-box');
    });

    $('.promoting-popup .promot-work img').on('click', function () {
        $('.promoting-popup').addClass('hide-popup-box');
    });

    $('.payment-mode').on('click', function () {
        $('.payment-popup').removeClass('hide-popup-box');
    });

    $('.payment-popup .promot-work img').on('click', function () {
        $('.payment-popup').addClass('hide-popup-box');
    });

    $('.purchase-btn').on('click', function () {
        switch ($('.current-payment').attr('data-payment')) {
            case 'stripe':
                $('.credit-card-popup').removeClass('hide-popup-box');
                break;
            case 'paypal':
                window.location.href = '/payments/paypal/' + $('#paid-amount').val() + '/' + $('#product-id').val()
                break;
            case 'apple':
                appleCheckout();
                break;
            case 'gpay':
                gpayCheckout();
                break;
        }
    });

    $('.credit-card-popup .promot-work img').on('click', function () {
        $('.credit-card-popup').addClass('hide-popup-box');
    });

    $('.sell-item .slider .slider-box').on('click', function () {
        $('.sell-item .slider .slider-box').removeClass('selected');
        $(this).addClass('selected');
        $('.left-section h5').html($(this).find('.price-days').text());
        $('.left-section p').html($(this).find('.save-price').text());
        $('.menu-pay h1').html($(this).find('[data-price]').text());
        $('.menu-pay h1').attr('data-price', $(this).find('[data-price]').attr('data-price'));
        $('.single-item-pay .price-total-pay').text($(this).find('[data-price]').text());
        $('.single-item-pay .price-total-pay-amount').text('₪' + (parseInt($(this).find('[data-price]').text().replace(/^\D+/g, '')) + 10));
        $('#paid-amount').val($(this).find('[data-price]').attr('data-price'));
    });

    $('.checkout-popup .promot-work img').on('click', function () {
        $('.checkout-popup').addClass('hide-popup-box');
    });
    // $('.img-icon .second-product').on('click', function() {
    //     $('.second-product-menu').slideToggle(); 

    //lANGUAGE SELECTION
    $('.language-selector li').on('click', function () {
        if ($(this).closest('.language-selector').hasClass('active')) {
            $(this).parent().removeClass('active');

            $(this).closest('.language-selector').find("li").css("gridRow", "initial");
            $(this).closest('.language-selector').find("li").css("display", "none");
        } else {
            $(this).parent().addClass('active')
        }
        $(this).css("gridRow", "1");
        $(this).css("display", "grid");
    });

    $('.filter-sort').on('click', function () {
        $('.filter-sort').removeClass('grid');
        $(this).addClass('grid');
    });

    $('[login]').on('click', function (ev) {
        $('.login-popup').removeClass('hide-popup-box');
    });


    $('.back-to-login-button').on('click', function () {
        $('.account-popup').addClass('hide-popup-box');
        $('.login-popup').removeClass('hide-popup-box');
    });

    $('#form-login input').on('keyup', function () {
        var needActive = true;
        $('#form-login input').each(function () {
            if ($(this).val() == '') {
                needActive = false;
            }
        });
        if (needActive) {
            $('#form-login button').css('background-color', '#0872bb');
            $('#form-login button').attr('type', 'submit');
        } else {
            $('#form-login button').css('background-color', '#EBEBEB');
            $('#form-login button').attr('type', 'button');
        }
    });
    $('#form-signup input').on('keyup', function () {
        var needActive = true;
        $('#singup-details-form input').each(function () {
            if ($(this).val() == '') {
                needActive = false;
            }
        });
        if (needActive) {
            $('#first-email-form-submit').css('background-color', '#0872bb');
            $('#first-email-form-submit').attr('onclick', "submitOtpEmail()");
        } else {
            $('#first-email-form-submit').css('background-color', '#EBEBEB');
            $('#first-email-form-submit').attr('onclick', "");
        }
    });

    $('#form-mobile-input-signup').on('keyup', function () {
        var needActive = false;
        if ($('#form-mobile-input-signup').val().length === 10) {
            needActive = true;
        }
        if (needActive) {
            $('#first-phone-form-submit').css('background-color', '#0872bb');
            $('#first-phone-form-submit').attr('onclick', "sendOtpSingup()");
        } else {
            $('#first-phone-form-submit').css('background-color', '#EBEBEB');
            $('#first-phone-form-submit').attr('onclick', "");
        }
    });


    $('.account-popup .form-container .single-field label.checkbox input').on('click', function () {
        var d = $(this).parents('label.checkbox');
        if (d.hasClass('checked')) {
            d.removeClass('checked');
        } else {
            d.addClass('checked');
        }
    });
    $('.show-password').on('click', function () {
        var current = $(this).attr('current');
        var lang = $(this).attr('lang');
        if (current == 'show') {
            if (lang == 'en') {
                $(this).html('<img src="/img/mobile/invisible.png" style="height:12px"> Hide Password');
            } else {
                $(this).html('<img src="/img/mobile/invisible.png" style="height:12px"> הסתר סיסמא');
            }
            $(this).attr('current', 'hide');
            $(this).parents('form').find('input[name="password"]').attr('type', 'text');
        } else {
            if (lang == 'en') {
                $(this).html('<img src="/img/mobile/visible.png" style="height:12px"> Show Password');
            } else {
                $(this).html('<img src="/img/mobile/visible.png" style="height:12px"> הראה סיסמה');
            }
            $(this).attr('current', 'show');
            $(this).parents('form').find('input[name="password"]').attr('type', 'password');
        }
    });
    $('#form-login').on('submit', function (evt) {
        var response = grecaptcha.getResponse(0);
        if (response.length == 0) {
            //reCaptcha not verified
            Msg['error']("Please verify captcha to complete login.");
            evt.preventDefault();
            return false;
        }
    });

    $('#contine-with-email').on('click', function () {
        $('.email-signup-container').hide();
        $('#form-signup-container').show();
    });

    $('.reset-cat-btn').on('click', function () {
        $(this).hide();
        localStorage.removeItem("cat_ids");
        $('.menu-content').addClass('hide-menu');
        $('.cat-img').attr('src', '/img/mobile/arrow-right.svg');
        $('#cat-ids').val('');
        $('.sub-cat img').hide();
        $('#tag-search [id^=sub-cat-]').remove();
        loadProduct(true);
    })
    var lastScrollTop = 0;
    $(window).scroll(function (event) {
        var st = $(this).scrollTop();
        if (st > lastScrollTop && lastScrollTop >= 50) {
            $('.site-notification').css('opacity', 0);
        } else {
            $('.site-notification').css('opacity', 1);
        }
        lastScrollTop = st;
        var position = $(window).scrollTop() + window.innerHeight;
        var bottom = $(document).height() - $('footer').height();

        if (position == bottom) {
            loadProduct();
        }
    });

    $('.open-dropdown').on('click', function () {
        var id = $(this).attr('target');
        if ($(id).css('display') == 'none') {
            $(id).css('display', 'block');
        } else {
            $(id).css('display', 'none');
        }
    });

    $('.available-on-mobile').on('click', function (ev) {
        ev.preventDefault();
        console.log(12);
        $('#available-on-app').css('display', 'block');
        $('.popup-box').addClass('hide-popup-box');
        setTimeout(function () {
            $('body').css('overflow', 'hidden');
            $('body').css('height', '100%');
        }, 33);
    });

    $('#available-on-app .available-close').on('click', function (ev) {
        $('#available-on-app').css('display', 'none');
        $('body').css('overflow', 'inherit');
        $('body').css('height', 'inherit');
    });

    window.onclick = function (event) {
        var modal = document.getElementById("available-on-app");
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    var $inputAutocomplete = $("#searh-bar-input").autocomplete({
        source: '/search-keyword?distance=' + ($('[name="debt-amount"]:checked').attr('data-distance') || 50) + '&latitude=' + $('#product-lat').val() + '&longitude=' + $('#product-lng').val(),
        minLength: 0,
        response: function (event, ui) {
            // if (RECENT_ITEMS.length > 0) {
            //     ui.content.push({
            //         'label': 'RECENT SEARCHES',
            //         'value': ''
            //     });
            //     $.each(RECENT_ITEMS, function(k, recent_item) {
            //         ui.content.push({
            //             'label': recent_item,
            //             'value': recent_item
            //         });
            //     });
            // }
            // if (TRENDING_ITEMS.length > 0) {
            //     ui.content.push({
            //         'label': 'TRENDING SEARCHES',
            //         'value': ''
            //     });
            //     $.each(TRENDING_ITEMS, function(k, trending_item) {
            //         ui.content.push({
            //             'label': trending_item,
            //             'value': trending_item
            //         });
            //     });
            // }
        },
        select: function (event, ui) {
            setTimeout(function () {
                loadProduct(true)
            }, 30);
        }
    });

    $('#product-lat, #product-lng, [name="debt-amount"]:checked').change(function () {
        $("#searh-bar-input").autocomplete('option', 'source', '/search-keyword?distance=' + ($('[name="debt-amount"]:checked').attr('data-distance') || 50) + '&latitude=' + $('#product-lat').val() + '&longitude=' + $('#product-lng').val());
    })

    if ($inputAutocomplete.data("ui-autocomplete")) {
        $inputAutocomplete.data("ui-autocomplete")._renderItem = function (ul, item) {
            if (item.value == '') {
                return $('<li class="ui-state-disabled"><div>' + item.label + '</div></li>').appendTo(ul);
            } else {
                return $("<li>")
                    .append(`<div>${item.label}</div>`)
                    .appendTo(ul);
            }
        };

        $inputAutocomplete.on('focus', function () {
            $(this).keydown();
        });
    }
});

$(document).ready(function () {
    $('.slider').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });

    $('body').on('click', function () {
        setTimeout(function () {
            if ($('.account-popup.popup-box:not(.hide-popup-box)').length) {
                $('body').css('height', '100%');
                $('body').css('overflow', 'hidden');
            } else {
                $('body').css('height', 'inherit');
                $('body').css('overflow', 'inherit');
            }
        }, 30);
    });
});

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function translateLanguage(val) {
    setCookie('lang', val);
    $.get('/set-language/' + val).then(() => {
        window.location.reload();
    });
}

if ("geolocation" in navigator) {
    //check geolocation available 

    //try to get user current location using getCurrentPosition() method
    navigator.geolocation.getCurrentPosition(function (position) {
        var cokie = getCookie('localInformation');
        if (cokie !== '') {
            var cokie = JSON.parse(getCookie('localInformation'));
        }
        if ((cokie == '') || (cokie.latitude != position.coords.latitude) || (cokie.longitude != position.coords.longitude)) {
            $.get('https://maps.google.com/maps/api/geocode/json', {
                'latlng': position.coords.latitude + "," + position.coords.longitude,
                'key': 'AIzaSyCehnlNlZBui59vgOGNYN8XoN5rjYE9HJE'
            }, function (res) {
                var address = {};
                if (res['results']) {
                    res['results'].forEach(function (value, index) {
                        value['address_components'].forEach(function (value, index) {
                            address[value['types'][0]] = {
                                'short_name': value['short_name'],
                                'long_name': value['long_name']
                            };
                        });
                    });
                }
                if (address !== {}) {
                    var localInformation = {
                        'latitude': position.coords.latitude,
                        'longitude': position.coords.longitude,
                        'city': address.locality ? address.locality.long_name : 'Israel',
                        'zip': address.locality ? address.postal_code.long_name : ''
                    }
                    setCookie('localInformation', JSON.stringify(localInformation), 365);
                    $.get('/location');
                    if (cokie == '') {
                        window.location.reload();
                    }
                    loadProduct(true);
                }
            });
        }
    },
        function (err) {
            console.warn(`ERROR(${err.code}): ${err.message}`);
        }, {
        enableHighAccuracy: true,
        maximumAge: 0
    });
} else {
    console.log("Browser doesn't support geolocation!");
}

function trimChar(string, charToRemove) {
    while (string.charAt(0) == charToRemove) {
        string = string.substring(1);
    }

    while (string.charAt(string.length - 1) == charToRemove) {
        string = string.substring(0, string.length - 1);
    }

    return string;
}

var needExecute = true;

function loadProduct(force = false) {
    var cokie = getCookie('localInformation');
    var latitude = 31.7964452;
    var longitude = 35.1051475;
    if ($('#product-lat').val() !== '' && $('#product-lng').val() !== '') {
        var latitude = $('#product-lat').val();
        var longitude = $('#product-lng').val();
    } else if (cokie !== '') {
        var cokie = JSON.parse(getCookie('localInformation'));
        latitude = cokie.latitude;
        longitude = cokie.longitude;
    }
    if (force) {
        pageContent = 0;
    }
    if (force || needExecute) {
        $.ajax({
            url: "/get-toplitsning/",
            data: {
                latitude: latitude,
                longitude: longitude,
            }
        })
            .done(function (datas) {
                $('#top-litning-products').removeClass('slick-initialized');
                $('#top-litning-products').removeClass('slick-slider');
                if (datas.length) {
                    $('#top-litning-products').html('');
                    datas.forEach(function (data) {
                        if (data['product'].favourite !== undefined) {
                            var loginRequired = `javascript:markFavourite(${data['product'].id});`;
                            if (data['product'].favourite == null) {
                                var favImg = 'heart.svg';
                                var extraCss = '';
                            } else {
                                var favImg = 'heart_fill.png';
                                var extraCss = 'width: 25px;height: 25px;';
                            }
                        } else {
                            var favImg = 'heart.svg';
                            var loginRequired = `javascript:void(0);`;
                            var extraCss = '';
                        }
                        var product = `
                        <div class="slide single-item" onclick="window.location.href='/product/${data['product'].id}'">
                            <div class="product-image">
                                <span class="fav-btn">
                                    <a href="${loginRequired}">
                                        <img src="${BASE_URL_IMAGE}mobile/${favImg}" class="img-favourite-${data['product'].id}" login="1" style="${extraCss}">
                                    </a>
                                </span>
                                <img src="${data['product']['post_image'].image_url}" class="top-listing-image">
                            </div>
                            <div class="single-item-content">
                                <h3 style="overflow:hidden;white-space: nowrap;">${data['product'].product_title}</h3>
                                <span>${data['product'].product_price}₪</span>
                            </div>
                        </div>
                `;
                        $('#top-litning-products').append(product);
                    });
                    $('.slider').slick({
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 2000,
                    })
                } else {
                    $('#top-litning-products').html('<h3>No product found.</h3>');
                }
            });
    }
    if (needExecute || force) {
        needExecute = false;
    } else {
        return;
    }
    $.ajax({
        url: "/get-products/",
        data: {
            latitude: latitude,
            longitude: longitude,
            page: pageContent,
            distance: $('[name="debt-amount"]:checked').attr('data-distance'),
            search: $('#searh-bar-input').val(),
            min_price: $('#min-price').val(),
            max_price: $('#max-price').val(),
            order: $('.filter-sort.grid').attr('data-order'),
            category: trimChar($('#cat-ids').val(), ',')
        }
    })
        .done(function (datas) {
            if (force) {
                $('#product-list').html('');
            }
            if (datas.length) {
                $('#no-product-image').hide();
                datas.forEach(function (data) {
                    if (data.favourite !== undefined) {
                        var loginRequired = `javascript:markFavourite(${data.id});`;
                        if (data.favourite == null) {
                            var favImg = 'heart.svg';
                            var extraCss = '';
                        } else {
                            var favImg = 'heart_fill.png';
                            var extraCss = 'width: 25px;height: 25px;';
                        }
                    } else {
                        var favImg = 'heart.svg';
                        var loginRequired = `javascript:void(0);`;
                        var extraCss = '';
                    }

                    var today = new Date();
                    otherDate = new Date(data.created_at);
                    if (today.toDateString() == otherDate.toDateString()) {
                        var new_tag = '<p>New</p>';
                    } else {
                        new_tag = '<p style="background: transparent;">&nbsp;</p>';
                    }
                    var product = `
                <div class="new-single-items" data-lat="${data.product_lat}" data-lng="${data.product_lng}" onclick="window.location.href='/product/${data.id}'">
                    <div class="new-item-image">
                        <div class="text-heart">
                            ${new_tag}
                            <span class="fav-btn">
                                <a href="${loginRequired}">
                                    <img src="${BASE_URL_IMAGE}mobile/${favImg}" class="img-favourite-${data.id}" login="1" style="${extraCss}">
                                </a>
                            </span>
            
                        </div>
                        <div class="laptop-pic">
                            <img src="${data['post_image'].image_url}" class="top-listing-image">
                        </div>
            
                    </div>
                    <div class="new-item-content">
                        <h3>
                            ${data.product_title}
                        </h3>
                        <span>
                            ${data.product_price}₪
                        </span>
                    </div>
                </div>
                `;
                    $('#product-list').append(product);
                });
                needExecute = true;
            } else {
                if (force) {
                    $('#no-product-image').show();
                }
                $('#load-more-product-btn').hide();
            }
            $('[login]').on('click', function (ev) {
                $('.login-popup').removeClass('hide-popup-box');
            });
        });
}


function markFavourite(id) {
    $.get(FAVOURITE_URL + id, function () {
        var img = $('.img-favourite-' + id);
        if (img.attr('src').split('?')[0] == BASE_URL_IMAGE + 'mobile/heart.svg') {
            img.attr('style', 'width: 25px;height: 25px;');
            img.attr('src', BASE_URL_IMAGE + 'mobile/heart_fill.png');
        } else {
            img.attr('src', BASE_URL_IMAGE + 'mobile/heart.svg');
            img.attr('style', '');
        }
    });
}

function applyFilter() {
    var distance = $('[name="debt-amount"]:checked').attr('data-distance');
    var min_price = $('#min-price').val();
    var max_price = $('#max-price').val();
    var order = $('.filter-sort.grid a').text();
    localStorage.setItem("distance", distance);
    localStorage.setItem("min_price", min_price);
    localStorage.setItem("max_price", max_price);
    localStorage.setItem("order", order);
    $('#tag-search').html('');
    if (lang == 'en') {
        var distance_text = 'Distance';
        var min_text = 'Min';
        var max_text = 'Max';
    } else {
        var distance_text = 'מֶרְחָק';
        var min_text = 'מינימום';
        var max_text = 'מקסימום';
    }
    if (distance && distance.length != 0) {
        $('#tag-search').append(`
            <li id="filter-distance-input">
                ${distance_text}: ${distance}Km
                <img src="${BASE_URL_IMAGE}mobile/cancel.svg" alt="x" onclick="closeDistance()">
            </li>
        `);
    } else {
        $('#filter-distance-input').remove();
    }
    if (min_price && min_price.length != 0) {
        $('#tag-search').append(`
            <li id="filter-min-price-input">
                ${min_text}: ₪ ${min_price}
                <img src="${BASE_URL_IMAGE}mobile/cancel.svg" onclick="closeMinPrice()" alt="x">
            </li>
        `);
    } else {
        $('#filter-min-price-input').remove();
    }
    if (max_price && max_price.length != 0) {
        $('#tag-search').append(`
            <li id="filter-max-price-input">
                ${max_text}: ₪ ${max_price}
                <img src="${BASE_URL_IMAGE}mobile/cancel.svg" onclick="closeMaxPrice()" alt="x">
            </li>
        `);
    } else {
        $('#filter-max-price-input').remove();
    }
    if (order && order.length != 0) {
        $('#tag-search').append(`
            <li id="filter-order-input">
                ${order}
                <img src="${BASE_URL_IMAGE}mobile/cancel.svg" onclick="closeOrder()" alt="x">
            </li>
        `);
    } else {
        $('#filter-order-input').remove();
    }
    if (getCookie('alternateLocalInformation') != "") {
        setCookie('localInformation', getCookie('alternateLocalInformation'));
        $.get('/location');
    }
    loadProduct(true);
}

function closeDistance() {
    $('#filter-distance-input').remove();
    $('[name="debt-amount"]:checked').prop('checked', false);
    $('[name="debt-amount"]').change();
    localStorage.removeItem("distance");
    loadProduct(true);
}

function closeMinPrice() {
    $('#filter-min-price-input').remove();
    $('#min-price').val('');
    localStorage.removeItem("min_price");
    loadProduct(true);
}

function closeMaxPrice() {
    $('#filter-max-price-input').remove();
    $('#max-price').val('');
    localStorage.removeItem("max_price");
    loadProduct(true);
}

function closeOrder() {
    $('#filter-order-input').remove();
    $('.filter-sort').removeClass('grid');
    localStorage.removeItem("order");
    loadProduct(true);
}

function clearFilter() {
    var con = confirm('This will reset all your filters. Do you want to proceed?');
    if (con) {
        localStorage.removeItem("distance");
        localStorage.removeItem("min_price");
        localStorage.removeItem("max_price");
        localStorage.removeItem("order");
        $('#filter-order-input').remove();
        $('.filter-sort').removeClass('grid');
        $('#filter-max-price-input').remove();
        $('#max-price').val('');
        $('#filter-min-price-input').remove();
        $('#min-price').val('');
        $('#filter-distance-input').remove();
        $('[name="debt-amount"]:checked').prop('checked', false);
        $('[name="debt-amount"]').change();
        $('#product-lat').val('');
        $('#product-lng').val('');
        var cokie = getCookie('localInformation');
        if (cokie !== '') {
            var cokie = JSON.parse(getCookie('localInformation'));
            $('#current-location').html(cokie.city + " " + cokie.zip);
            $('#product-lat').val(cokie.latitude);
            $('#product-lng').val(cokie.longitude);
        } else {
            $('#current-location').html('Near Location');
        }
        $('#product-lat').change();
        loadProduct(true);
    }
}

function initialiseAutocompleteInput() {
    geocoder = new google.maps.Geocoder();
    var input = document.getElementById('autocomplete-filter-address');
    var places = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(places, 'place_changed', function () {
        var place = places.getPlace();
        $('#current-location').html(place.formatted_address);
        $('#product-lat').val(place.geometry.location.lat());
        $('#product-lng').val(place.geometry.location.lng());
        localStorage.setItem("product_lat", place.geometry.location.lat());
        localStorage.setItem("product_lng", place.geometry.location.lng());
        localStorage.setItem("location", place.formatted_address);
        $('#product-lat').change();
        $('.location-content').addClass('hide-popup-box');
        address = {};
        place['address_components'].forEach(function (value, index) {
            address[value['types'][0]] = {
                'short_name': value['short_name'],
                'long_name': value['long_name']
            };
        });
        var localInformation = {
            'latitude': place.geometry.location.lat(),
            'longitude': place.geometry.location.lng(),
            'city': address.locality.long_name,
            'zip': ''
        }
        setCookie('alternateLocalInformation', JSON.stringify(localInformation), 365);
    });
}

addEventListener('load', initialiseAutocompleteInput);

function loadSubCat(id) {
    if ($('#sub-cat-' + id).is(":hidden")) {
        $('.sub-cat').hide();
        $('#sub-cat-' + id).show();
    } else {
        $('.sub-cat').hide();
    }
}



function loadCat(catId, subCatId, Text) {
    $('.reset-cat-btn').show();
    var catIds = $('#cat-ids').val().split(',');
    var img = $('#sub-cat-item-' + subCatId + ' img');
    if (img.is(':hidden')) {
        img.show();
        $('#tag-search').append(`
        <li id="sub-cat-${subCatId}">
            ${Text}
            <img src="${BASE_URL_IMAGE}mobile/cancel.svg" onclick="closeLoadCat(${catId}, ${subCatId})" alt="x">
        </li>
    `);
        catIds.push(subCatId);
    } else {
        img.hide();
        $('li#sub-cat-' + subCatId).remove();
        catIds.splice(catIds.indexOf(subCatId.toString()), 1);
    }
    $('#cat-ids').val(catIds.join(','));
    localStorage.setItem('cat_ids', catIds.join(','));
    var need_to_show = false;
    $('#cat-' + catId + ' .sub-cat li').each(function () {
        if ($(this).find('img').is(':hidden') === false) {
            need_to_show = true;
        }
    });
    if (need_to_show) {
        $('#cat-' + catId + ' .cat-img').attr('src', BASE_URL_IMAGE + 'checked (3).svg');
    } else {
        $('#cat-' + catId + ' .cat-img').attr('src', BASE_URL_IMAGE + 'mobile/arrow-right.svg');
    }
    // loadProduct(true);
}

function closeLoadCat(catId, subCatId) {
    var catIds = $('#cat-ids').val().split(',');
    updatedCat = $.grep(catIds, function (value) {
        return value != subCatId;
    });
    $('#cat-ids').val(updatedCat.join(','));
    localStorage.setItem('cat_ids', updatedCat.join(','));

    $('li#sub-cat-' + subCatId).remove();
    var img = $('#sub-cat-item-' + subCatId + ' img');
    img.hide();
    var need_to_show = false;
    $('#cat-' + catId + ' .sub-cat li').each(function () {
        if ($(this).find('img').is(':hidden') === false) {
            need_to_show = true;
        }
    });
    if (need_to_show) {
        $('#cat-' + catId + ' .cat-img').attr('src', BASE_URL_IMAGE + 'checked (3).svg');
    } else {
        $('#cat-' + catId + ' .cat-img').attr('src', BASE_URL_IMAGE + 'mobile/arrow-right.svg');
    }
    // loadProduct(true);
}

function currentDiv(n) {
    showDivs(slideIndex = n);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    if (n > x.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = x.length }
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
    }
    x[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " w3-opacity-off";
}

function changePayment(type) {
    switch (type) {
        case 'stripe':
            $('.current-payment').attr('data-payment', 'stripe');
            $('.current-payment .pay img').attr('src', '/img/mobile/credit-card.svg');
            $('.current-payment h3').text('Credit or debit card');
            $('.current-payment .pay').css('background', 'transparent');
            break;
        case 'paypal':
            $('.current-payment').attr('data-payment', 'paypal');
            $('.current-payment .pay img').attr('src', '/img/mobile/paypal.png');
            $('.current-payment h3').text('Paypal');
            $('.current-payment .pay').css('background', 'white');
            break;
        case 'apple':
            $('.current-payment').attr('data-payment', 'apple');
            $('.current-payment .pay img').attr('src', '/img/mobile/apple-logo (2).png');
            $('.current-payment h3').text('Apple');
            $('.current-payment .pay').css('background', 'black');
            break;
        case 'gpay':
            $('.current-payment').attr('data-payment', 'gpay');
            $('.current-payment .pay img').attr('src', '/img/mobile/gpay.png');
            $('.current-payment h3').text('Google Pay');
            $('.current-payment .pay').css('background', 'white');
            break;
    }
    $('.payment-popup').addClass('hide-popup-box');
}

function sendOtpEmail(email) {
    $.ajax({
        url: '/send-email-otp',
        data: {
            email: email
        },
        type: 'GET',
        success: function (res) {
            console.log(res);
            if (res == true) {
                $('#sent-otp-number-singup').html(email);
                $('#verify-email-model').show();
                $('#singup-details-form').hide();
            } else {
                Msg['error']("This email is already registered. Please try a different email");
            }
        }
    })
}

function submitOtpEmail() {
    var response = grecaptcha.getResponse(1);
    if (response.length == 0) {
        //reCaptcha not verified
        Msg['error']("Please verify captcha to complete Signup");
        return false;
    }
    sendOtpEmail($('#form-signup input[name="email"]').val());
};