$(document).ready(function () {
    $('#image').on('change', function () {
        if (this.files && this.files[0]) {
            $('#thumbnail-image').attr('src', URL.createObjectURL(this.files[0]));
        } else {
            $('#thumbnail-image').attr('src', $('#thumbnail-image').attr('default-image'));
        }
    });
    $('#start-date').on('change', function () {
        $('#end-date').datepicker('setStartDate', $(this).val());
    });
    $('#end-date').on('change', function () {
        $('#start-date').datepicker('setEndDate', $(this).val());
    });
});