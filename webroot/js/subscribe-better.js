/*
  @author: Ilyas karim <ilyas.datoo@gmail.com>
  @date: 5/may/2016

*/
(function ($) {
  setInterval(function () {
    $(".navbar-brand").html($(document).width());
  }, 10)
  $.fn.jPopup = function (options) {
    var settings = $.extend({
      popupParent: "gee-popup-newsletter",
      scrollTopPx: 100,
      popupCloseButton: ".popup-newsletter-close-button",
      heading: "heading - You can change",
      paragraph: "You can change paragraph from options. You can also change the input into your own template",
      userContent: '<div class="input"> <input class="form-control" type="text" placeholder="Your Email" > </div>',
      buttonText: 'Click me',
      buttonClass: "btn btn-info btn-block btn-lg",
      // openPopup : "asd",
      initThrough: function () {
        $(window).on('scroll', function (event) {
          var scrollValue = $(window).scrollTop();
          if (scrollValue == settings.scrollTopPx || scrollValue > settings.scrollTopPx) {
            // call the popup
            if (hasPopuped == false) {
              $.fn.jPopup.openPopup();
            }
          }
        });
      },
      openPopup: function () {
        if (localStorage.getItem('newletter') != '1') {
          $("html").addClass('active-popup-newsletter');
        }
      }
    }, options);
    var hasPopuped = false;
    var scrollValue = $(window).scrollTop();
    settings.initThrough();
    $(".gee-popup-newsletter .popup-newsletter-title").html(settings.heading);
    $(".gee-popup-newsletter .paragraph").html(settings.paragraph);
    $(".gee-popup-newsletter .user-content").html(settings.userContent);
    $(".gee-popup-newsletter .btn").html(settings.buttonText);
    $(".gee-popup-newsletter .btn").addClass(settings.buttonClass);
    $(".popup-newsletter-close-button").click(function () {
      $('html').toggleClass('active-popup-newsletter');
      hasPopuped = true;
    });
    $(".opup-newsletter-button").click(function () {
      $('html').toggleClass('active-popup-newsletter');
      hasPopuped = true;
      alertify.success('Thank you for subsribing us');
      localStorage.setItem('newletter', '1');
    });
  }
  $.fn.jPopup.openPopup = function () {
    if (localStorage.getItem('newletter') != '1') {
      $("html").addClass('active-popup-newsletter');
    }
  }
}(jQuery))
