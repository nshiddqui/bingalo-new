<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Http\Client;
use Cake\Cache\Cache;
use Cake\I18n\I18n;
use Cake\Routing\Router;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $publicCredential = [
        "apiKey" => "AIzaSyDoyjxMTlmfCnQvfEU1mcIbPjiZDxdYLRo",
        "authDomain" => "bingalo.firebaseapp.com",
        "databaseURL" => "https://bingalo.firebaseio.com",
        "projectId" => "bingalo",
        "storageBucket" => "bingalo.appspot.com",
        "messagingSenderId" => "869511632481",
        "appId" => "1:869511632481:web:372df613d3b6a0c9427c20"
    ];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'loginAction' => '/',
            'authError' => 'Did you really think you are allowed to see that?',
            'authenticate' => [
                'Form' => ['userModel' => 'Admins', 'fields' => ['username' => 'email']],
                'HybridAuth'
            ],
            'storage' => 'Session'
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    public function beforeFilter(Event $event)
    {
        $this->loadModel('IpLanguages');
        $ip = $this->IpLanguages->find('all', [
            'conditions' => [
                'ip' => $this->request->clientIp()
            ]
        ]);
        if ($ip->count()) {
            $lang = $ip->first();
            $_COOKIE['lang'] = $lang->language;
            $this->set('exists_language', $lang);
        } else {
            $this->set('exists_language', []);
        }
        $this->request->addDetector('api', ['param' => 'api', 'value' => 'api']);
        if ($this->request->is('api')) {
            $this->loadComponent('Api');
        }
        if (!$this->Auth->user()) {
            $this->loadComponent('Cookie');
            $details =  $this->Cookie->read('RememberMe');
            if (!empty($details)) {
                $this->initialLogin($details);
            }
        }
        if ($this->request->getParam('prefix') === 'admin') {
            return $this->redirect('https://bingalo.com/admin');
        }
        if ($this->request->is('mobile') || isset($_COOKIE['mobile_user'])) {
            $this->loadMobileComponent();
        } else {
            return $this->redirect('https://bingalo.com' . Router::url(null));
        }
        $this->loadModel('AppMaintaince');
        $app_maintaince = $this->AppMaintaince->get(1, [
            'contain' => [],
        ]);
        if ($app_maintaince->is_maintaince == 1) {
            if ($this->request->getAttribute('here') !== '/maintance') {
                return $this->redirect('/maintance');
            }
            $this->set(compact('app_maintaince'));
        }
        if ($this->Auth->user()) {
            $this->set('authUser', $this->Auth->user());
        } else {
            $this->set('authUser', false);
        }
        $this->loadModel('AppInfos');
        $this->set('app_info_data', $this->AppInfos->get(1));
        $this->set('googleApiKey', 'AIzaSyCdx1gFKWhFRwNC8-jVRKxpS1DJavfUh5g');
        $this->set('firebaseSetup', $this->publicCredential);
        $this->set('is_mobile', $this->request->is('mobile'));
        parent::beforeFilter($event);
    }

    protected function loadMobileComponent()
    {
        $this->loadModel('Categories');
        $categories = $this->Categories->find('all', [
            'fields' => [
                'Categories.category_name',
                'Categories.herbew_category_name',
                'Categories.category_image',
                'Categories.id'
            ],
            'contain' => [
                'SubCategories' => function ($q) {
                    return $q->order('SubCategories.position')->select(['SubCategories.name', 'SubCategories.herbew_name', 'SubCategories.id', 'SubCategories.category_id']);
                }
            ],
            'order' => [
                'Categories.position'
            ]
        ]);
        $categoreies = [];
        foreach ($categories as $data) {
            if (!empty($data['sub_categories'])) {
                $categoreies[] = $data;
            }
        }
        # We check if we have a language set
        if (isset($_COOKIE['lang']) && !empty($_COOKIE['lang'])) {
            I18n::setLocale($_COOKIE['lang']);
        } else {
            # If we don't have one, we will set the default one (in my case it's English)
            I18n::setLocale('en');
        }
        $this->set('ipAddress', $this->request->clientIp());
        $this->set('isAndroid', (stripos($_SERVER['HTTP_USER_AGENT'], "Android")));
        $this->set('categories', $categoreies);
    }

    protected function initialLogin($details)
    {
        $users = $this->getTableLocator()->get('Users');
        $user = $users->find('all', [
            'conditions' => [
                'email' => $details['email'],
                'password' => md5($details['password'])
            ]
        ]);
        if ($user->count()) {
            $user = $user->first()->toArray();
            $this->Auth->setUser($user);
            return true;
        }
        return false;
    }

    protected function getLatLng()
    {
        $ip = $this->request->clientIp();
        $http = new Client();
        if ($ip === '127.0.0.1') {
            $ip = '103.232.113.107';
        }
        $session = $this->getRequest()->getSession();
        $address_details = $session->read('localInformation');

        if (empty($address_details)) {
            if (isset($_COOKIE['localInformation']) && !empty($_COOKIE['localInformation'])) {
                $address_details = json_decode($_COOKIE['localInformation'], true);
            }
        }

        if (empty($address_details)) {
            $address_details = [
                'city' => '',
                'zip' => '',
                'latitude' => 31.7277287,
                'longitude' =>  34.9545428,
            ];
        }
        return $address_details;
    }
}
