<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

/**
 * TopCities Controller
 *
 * @property \App\Model\Table\TopCitiesTable $TopCities
 *
 * @method \App\Model\Entity\TopCity[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TopCitiesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('TopCities')
                ->queryOptions([
                    'order' => ['TopCities.created DESC']
                ])
                ->column('TopCities.id', ['label' => '#', 'width' => '30px'])
                ->column('TopCities.name', ['label' => 'Name'])
                ->column('TopCities.created', ['label' => 'Created Date', 'width' => '150px'])
                ->column('actions', ['label' => 'Actions', 'database' => false, 'width' => '150px']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->TopCities);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('TopCities');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Top City id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $topCity = $this->TopCities->get($id, [
            'contain' => [],
        ]);

        $this->set('topCity', $topCity);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $topCity = $this->TopCities->newEntity();
        if ($this->request->is('post')) {
            $topCity = $this->TopCities->patchEntity($topCity, $this->request->getData());
            if ($this->TopCities->save($topCity)) {
                $this->Flash->success(__('The top city has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The top city could not be saved. Please, try again.'));
        }
        $this->set(compact('topCity'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Top City id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $topCity = $this->TopCities->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $topCity = $this->TopCities->patchEntity($topCity, $this->request->getData());
            if ($this->TopCities->save($topCity)) {
                $this->Flash->success(__('The top city has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The top city could not be saved. Please, try again.'));
        }
        $this->set(compact('topCity'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Top City id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $topCity = $this->TopCities->get($id);
        if ($this->TopCities->delete($topCity)) {
            $this->Flash->success(__('The top city has been deleted.'));
        } else {
            $this->Flash->error(__('The top city could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
