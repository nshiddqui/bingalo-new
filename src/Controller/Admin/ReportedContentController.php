<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Mailer\Email;

/**
 * ReportedContent Controller
 *
 * @property \App\Model\Table\ReportedContentTable $ReportedContent
 *
 * @method \App\Model\Entity\ReportedContent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReportedContentController extends AppController {

    public function initialize() {
        $type = $this->request->getQuery('content');
        if (is_null($type)) {
            $type = $this->getRequest()->getSession()->read('reported-content');
        } else {
            $this->getRequest()->getSession()->write('reported-content', $type);
        }
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('ReportedContent')
                ->databaseColumn('ReportedContent.id')
                ->queryOptions([
                    'contain' => [
                        'Users',
                        'Product',
                        'ReportedUsers'
                    ],
                    'conditions' => [
                        'ReportedContent.' . ( $type ? 'post_id' : 'reported_user_id' ) . ' !=' => ''
                    ],
                    'order' => ['ReportedContent.created_at DESC']
                ])
                ->databaseColumn('Users.full_name')
                ->databaseColumn('Users.id')
                ->databaseColumn('Product.product_title')
                ->databaseColumn('Product.uploaded_by_user_id')
                ->column('ReportedUsers.full_name', ['label' => 'Reported By', 'searchable' => false])
                ->column('ReportedContent.reported_reason', ['label' => 'Reason'])
                ->column('ReportedContent.reported_desc', ['label' => 'Description'])
                ->column('reported_to', ['label' => 'Report To ' . ($type ? 'Product' : 'User'), 'database' => false])
                ->column('ReportedContent.created_at', ['label' => 'Reported Date'])
                ->column('action', ['label' => 'Action', 'database' => false]);
        $this->set(compact('type'));
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $to = $this->ReportedContent->Users->get($data['email']);
            $email = new Email('default');
            $email->setFrom(['admin@bingalo.com' => 'Bingalo Admin'])
                    ->setTo($to->email)
                    ->setSubject($data['subject'])
                    ->send($data['message']);
            $this->Flash->success(__('The mail has been sent to user.'));
            return $this->redirect($this->referer());
        } else {
            $this->DataTables->setViewVars('ReportedContent');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Reported Content id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $reportedContent = $this->ReportedContent->get($id, [
            'contain' => [
                'Users',
                'Product',
            ]
        ]);
        if ($this->ReportedContent->delete($reportedContent)) {
            if (!empty($reportedContent['user'])) {
                $this->ReportedContent->Users->delete($reportedContent['user']);
            }
            if (!empty($reportedContent['product'])) {
                $this->ReportedContent->Product->delete($reportedContent['product']);
            }
            $this->Flash->success(__('The reported content has been deleted.'));
        } else {
            $this->Flash->error(__('The reported content could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }

}
