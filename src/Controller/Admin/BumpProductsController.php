<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

/**
 * BumpProducts Controller
 *
 * @property \App\Model\Table\BumpProductsTable $BumpProducts
 *
 * @method \App\Model\Entity\BumpProduct[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BumpProductsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('BumpProducts')
                ->queryOptions([
                    'contain' => [
                        'Product', 'Users'
                    ]
                ])
                ->databaseColumn('Product.id')
                ->databaseColumn('Users.id')
                ->column('BumpProducts.id', ['label' => '#', 'width' => '30px'])
                ->column('Product.product_title', ['label' => 'Product Name'])
                ->column('Users.full_name', ['label' => 'Who Bumped an Item '])
                ->column('BumpProducts.payment_mode', ['label' => 'Payment Method'])
                ->column('BumpProducts.bump_price', ['label' => 'Paid'])
                ->column('BumpProducts.bump_days', ['label' => 'Total Days'])
                ->column('BumpProducts.created_at', ['label' => 'Created Date'])
                ->column('actions', ['label' => 'Actions', 'database' => false, 'width' => '30px']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->BumpProducts);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('BumpProducts');
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $bump_product = $this->BumpProducts->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bump_product = $this->BumpProducts->patchEntity($bump_product, $this->request->getData());
            if ($this->BumpProducts->save($bump_product)) {
                $this->Flash->success(__('The bump product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bump product could not be saved. Please, try again.'));
        }
        $this->set(compact('bump_product'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $bump_product = $this->BumpProducts->get($id);
        if ($this->BumpProducts->delete($bump_product)) {
            $this->Flash->success(__('The bump product has been deleted.'));
        } else {
            $this->Flash->error(__('The bump product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
