<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Http\Client;
use Cake\Routing\Router;

/**
 * Notfications Controller
 *
 * @property \App\Model\Table\NotficationsTable $Notfications
 *
 * @method \App\Model\Entity\Notfication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NotficationsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('Notfications')
            ->databaseColumn('Notfications.id')
            ->queryOptions([
                'contain' => [
                    'Users'
                ],
                'conditions' => [
                    'Notfications.notification_by' => '-1'
                ],
                'order' => ['Notfications.notification_date DESC']
            ])
            ->column('Users.full_name', ['label' => 'Send To'])
            ->column('Notfications.notification_title', ['label' => 'Title'])
            ->column('Notfications.notification_description', ['label' => 'Message'])
            ->column('Notfications.notification_link', ['label' => 'Link'])
            ->column('Notfications.notification_date', ['label' => 'Send Date'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Notfications);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Notfications');
        }
    }

    public function send() {
        $notification = $this->Notfications->newEntity();
        if ($this->request->is('post')) {
            $tokens = [];
            $data = $this->request->getData();
            $this->loadComponent('Fileupload');
            if (!empty($data['image']['tmp_name'])) {
                $this->Fileupload->init(['upload_path' => WWW_ROOT . 'images/banner/', 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
                $this->Fileupload->upload('image');
                $data['image'] = Router::url(['controller' => 'images', 'action' => 'banner', $this->Fileupload->output('file_name'), 'prefix' => false], true);
            } else {
                unset($data['image']);
            }
            $this->Notfications->Users->setDisplayField('push_token');
            if ($data['user_type'] === 'custom') {
                $user_ids = array_filter($data['to_user_id']);
                foreach ($user_ids as $user_id) {
                    $data['notification_to'] = $user_id;
                    $data['notification_by'] = '-1';
                    $data['notification_type'] = '-1';
                    $notification = $this->Notfications->newEntity();
                    $notification = $this->Notfications->patchEntity($notification, $data);
                    $this->Notfications->save($notification);
                }
                $tokens = $this->Notfications->Users->find('list', [
                    'conditions' => [
                        'id in ' => $user_ids,
                        'push_token !=' => null
                    ]
                ])->toArray();
            } else {
                $data['notification_to'] = '-1';
                $data['notification_by'] = '-1';
                $data['notification_type'] = '-1';
                $notification = $this->Notfications->newEntity();
                $notification = $this->Notfications->patchEntity($notification, $data);
                $this->Notfications->save($notification);
                $tokens = $this->Notfications->Users->find('list', [
                    'conditions' => [
                        'push_token !=' => null
                    ]
                ])->toArray();
            }
            if (!empty($tokens)) {
                try {
                    $client = new Client();
                    $client->post('https://ijyaweb.com/bingalo_api/api/sendNotificationFromWeb', [
                        'push_token' => array_values(array_filter($tokens)),
                        'title' => $data['notification_title'],
                        'body' => $data['notification_description'],
                        'type' => '-1',
                        'id' => '-1',
                    ]);
                } catch (\Exception $e) {
                    echo $e->getMessage();
                    die;
                }
            }
            $this->Flash->success('Notification Sent Successfully');
            $this->redirect(['action' => 'index']);
        }
        $this->set(compact('notification'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Message id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $message = $this->Notfications->get($id);
        if ($this->Notfications->delete($message)) {
            $this->Flash->success(__('The notification has been deleted.'));
        } else {
            $this->Flash->error(__('The notification could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
