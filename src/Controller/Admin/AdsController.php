<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Routing\Router;

class AdsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->set('position', $this->Ads->getPosition());
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('Ads')
            ->databaseColumn('Ads.id')
            ->column('Ads.image', ['label' => 'Picture', 'width' => '130px', 'class' => 'text-center'])
            ->column('Ads.title', ['label' => 'Title'])
            ->column('Ads.link', ['label' => 'Link'])
            ->column('Ads.start_date', ['label' => 'Start Date'])
            ->column('Ads.end_date', ['label' => 'End Date'])
            ->column('Ads.position', ['label' => 'Position'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        if ($this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Ads);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Ads');
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        $ads = $this->Ads->newEntity();
        if ($this->request->is('post')) {
            $ads = $this->Ads->patchEntity($ads, $this->getData());
            if ($this->Ads->save($ads)) {
                $this->Flash->success(__('The ads has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ads could not be saved. Please, try again.'));
        }
        $this->set(compact('ads'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        $ads = $this->Ads->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ads = $this->Ads->patchEntity($ads, $this->getData());
            if ($this->Ads->save($ads)) {
                $this->Flash->success(__('The ads has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ads could not be saved. Please, try again.'));
        }
        $this->set(compact('ads'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if ($this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        $this->request->allowMethod(['post', 'delete']);
        $ads = $this->Ads->get($id);
        if ($this->Ads->delete($ads)) {
            $this->Flash->success(__('The ads has been deleted.'));
        } else {
            $this->Flash->error(__('The ads could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getData()
    {
        $data = $this->request->getData();
        $this->loadComponent('Amazon');
        if (!empty($data['image']['tmp_name'])) {
            $this->Amazon->upload($data['image']['tmp_name'], 'ads');
            $data['image'] = $this->Amazon->upload($data['image']['tmp_name'], 'ads');
        } else {
            unset($data['image']);
        }
        return $data;
    }
}
