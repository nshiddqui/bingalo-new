<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Routing\Router;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    public function home()
    {
        $this->loadModel('Ads');
        $this->loadModel('BumpProducts');
        $topBanner = $this->Ads->find('all', [
            'conditions' => [
                'NOW() BETWEEN start_Date AND end_date',
                'position' => '4'
            ]
        ]);
        $this->set('topBanner', $topBanner);
    }

    public function forgotPassword()
    {
        $this->request->allowMethod('post');
        $email = $this->request->getData('email');
        if (!empty($email)) {
            $this->loadModel('Users');
            $user = $this->Users->find('all', [
                'conditions' => [
                    'email' => $this->request->getData('email')
                ]
            ]);
            if ($user->count()) {
                $userData = $user->first();
                $userData->enc_id = base64_encode(base64_encode(base64_encode($userData->id)));
                $this->loadComponent('Notify');
                $this->Notify->sendResetPasswordEmail($userData);
                $this->Flash->success('Please Check email for further instructions');
            } else {
                $this->Flash->error('Provided email does not exist.');
            }
        }
        $this->redirect($this->referer());
    }

    public function login()
    {
        $this->request->allowMethod(['post']);
        $this->loadModel('Users');
        $response = $this->initialLogin([
            'email' => $this->request->getData('email'),
            'password' => $this->request->getData('password')
        ]);
        if ($response) {
            $this->_setCookie();
            $this->Flash->success(__('Login successful'));
        } else {
            $this->Flash->error('Invalid email & password');
        }
        $this->redirect($this->referer());
    }

    public function signup()
    {
        $this->request->allowMethod(['post']);
        $this->loadModel('Users');
        $this->loadModel('UserOtp');
        $data = $this->UserOtp->find('all', ['conditions' => [
            'send_to' => $this->request->getData('email'),
            'otp' => implode('', $this->request->getData('otp'))
        ]]);
        if ($data->count()) {
            $latlng = $this->getLatLng();
            $user = $this->Users->find('all', [
                'conditions' => [
                    'email' => $this->request->getData('email')
                ]
            ]);
            if ($user->count() === 0) {
                $user = $this->Users->newEntity();
                $user->full_name = $this->request->getData('full_name');
                $user->email = $this->request->getData('email');
                $user->password = md5($this->request->getData('password'));
                $user->lat = $latlng['latitude'];
                $user->lng = $latlng['longitude'];
                $user->singup_from = 'mobile website';
                $this->Users->save($user);
                $this->loadComponent('Notify');
                $this->Notify->sendWelcomeEmail($user);
                $this->Flash->success(__('Sign up successfull, Please Login to continue'));
            } else {
                $this->Flash->error('This email is already registered. Please try a different email');
            }
        } else {
            $this->Flash->error(__('Invalid OTP.'));
        }
        $this->redirect($this->referer());
    }

    public function authenticated()
    {
        $user = (array) $this->Auth->identify();
        if ($user) {
            $loginType = ['Facebook' => 'facebook_id', 'Google' => 'google_id', 'Twitter' => 'twitter_id', 'Apple' => 'apple_id'];
            $this->loadModel('Users');
            $latlng = $this->getLatLng();
            $existsUser = $this->Users->find('all', [
                'conditions' => [
                    'email' => $user['email']
                ]
            ]);
            if ($existsUser->count()) {
                $userData = $existsUser->first();
            } else {
                $userData = $this->Users->newEntity();
                $userData->email = $user['email'];
            };
            $userData->image = $user['photoURL'];
            $userData->full_name = $user['displayName'];
            $userData->{$loginType[Configure::read('provider')]} = $user['identifier'];
            $userData->lat = $latlng['latitude'];
            $userData->lng = $latlng['longitude'];
            $userData->singup_from = 'mobile website';
            $this->Users->save($userData);
            $this->Auth->setUser($userData->toArray());
            $this->Flash->success(__('Login successful'));

            return $this->redirect($this->Auth->redirectUrl());
        } else {
            $this->Flash->error('Invalid failed');
        }

        return $this->redirect($this->referer());
    }

    public function logout()
    {
        $this->loadComponent('Cookie');
        $this->Cookie->delete('RememberMe');
        $this->Flash->success(__('Logout successful'));
        $this->redirect($this->Auth->logout());
    }

    protected function _setCookie()
    {
        if (!$this->request->getData('remember_me')) {
            return false;
        }
        $this->loadComponent('Cookie');
        $data = [
            'email' => $this->request->getData('email'),
            'password' => $this->request->getData('password')
        ];
        $this->Cookie->write('RememberMe', $data, true, '+1 year');
        return true;
    }

    public function updateAddress()
    {
        $this->disableAutoRender();
        $this->viewBuilder()->setLayout(false);
        if (!empty($_COOKIE['localInformation'])) {
            $address_details = json_decode($_COOKIE['localInformation'], true);
            $session = $this->getRequest()->getSession();
            $address_details = $session->write('localInformation', $address_details);
        }
    }

    public function reset($id)
    {
        $this->viewBuilder()->setLayout(false);
        $id = base64_decode(base64_decode(base64_decode($id)));
        if ($id === false) {
            $this->redirect('/');
        }
        $this->loadModel('Users');
        $user = $this->Users->get($id);
        if ($this->request->is('post')) {
            $user->password = md5($this->request->getData('password'));
            $this->Users->save($user);
            $this->Flash->success('Your password has been changes, please login with new password.');
            $this->redirect('/');
        }
    }

    public function about()
    {
        $this->viewBuilder()->setLayout('public');
    }

    public function faq()
    {
        $this->viewBuilder()->setLayout('public');
    }

    public function safetyTips()
    {
        $this->viewBuilder()->setLayout('public');
    }

    public function prohibitedItems()
    {
        $this->viewBuilder()->setLayout('public');
    }

    public function termsConditions()
    {
        $this->viewBuilder()->setLayout('public');
    }

    public function privacyPolicy()
    {
        $this->viewBuilder()->setLayout('public');
    }

    public function marketing()
    {
        $this->viewBuilder()->setLayout('public');
    }

    public function sitemap()
    {
        $sitemap = [
            ['loc' => 'https://bingalo.com'],
            ['loc' => 'https://bingalo.com/about-us'],
            ['loc' => 'https://bingalo.com/faq'],
            ['loc' => 'https://bingalo.com/safety-tips'],
            ['loc' => 'https://bingalo.com/prohibited-items'],
            ['loc' => 'https://bingalo.com/terms-conditions'],
            ['loc' => 'https://bingalo.com/privacy-policy']
        ];
        $this->set([
            // Define an attribute on the root node.
            '@xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9',
            'sitemap' => $sitemap
        ]);
        $this->set('_serialize', ['@xmlns', 'sitemap']);
        $this->set('_rootNode', 'sitemapindex');
        $this->RequestHandler->renderAs($this, 'xml');
    }

    public function getProducts()
    {
        $latitude = $this->request->getQuery('latitude', $this->getLatLng()['latitude']);
        $longitude = $this->request->getQuery('longitude', $this->getLatLng()['longitude']);
        $condition = [];
        if ($this->request->getQuery('min_price')) {
            $condition['product_price <='] = $this->request->getQuery('min_price');
        }
        if ($this->request->getQuery('max_price')) {
            $condition['product_price <='] = $this->request->getQuery('max_price');
        }
        if ($this->request->getQuery('search')) {
            $condition['LOWER(Product.product_title) LIKE'] = '%' . strtolower($this->request->getQuery('search')) . '%';
        }
        if ($this->request->getQuery('condition')) {
            $condition['product_condition'] = $this->request->getQuery('condition');
        }
        if ($this->request->getQuery('category')) {
            $this->loadModel('SubCategories');
            $categories = $this->SubCategories->find('list', [
                'conditions' => [
                    'SubCategories.id IN' => explode(',', $this->request->getQuery('category'))
                ]
            ]);
            $condition['product_category IN'] = $categories->toArray();
        }
        $this->loadModel('Product');
        if (!empty($latitude) && !empty($longitude)) {
            $contain = [
                'PostImage'
            ];
            $fields = [
                'Product.product_price',
                'Product.id',
                'Product.created_at',
                'Product.product_title',
                'PostImage.image_url',
                'Product.product_lat',
                'Product.product_lng',
                'distance' => "(
                3959 * ACOS(
                    COS(RADIANS({$latitude})) * COS(
                        RADIANS(Product.product_lat)
                    ) * COS(
                        RADIANS(Product.product_lng) - RADIANS({$longitude})
                    ) + SIN(RADIANS({$latitude})) * SIN(
                        RADIANS(Product.product_lat)
                    )
                )
            )"
            ];
            if ($this->Auth->user()) {
                $contain['Favourite'] = function ($q) {
                    return $q->where(['Favourite.user_id' => $this->Auth->user('id')]);
                };
                $fields[] = 'Favourite.id';
            }
            $datas = $this->Product->find('all', [
                'fields' => $fields,
                'contain' => $contain,
                'conditions' => array_merge($condition, [
                    'Product.is_deleted' => 0,
                    'Product.product_is_sold' => 0,
                ]),
                'group' => 'Product.id HAVING distance <= ' . (int)$this->request->getQuery('distance', 50),
                'order' => $this->request->getQuery('order', 'Product.created_at DESC'),
                'limit' => 20,
                'offset' => $this->request->getQuery('page', 0) * 20
            ]);
        } else {
            $datas = [];
        }

        $this->set([
            'datas' => $datas,
            '_serialize' => 'datas',
        ]);
        return $this->RequestHandler->renderAs($this, 'json');
    }

    public function getTopLitning()
    {
        $this->loadModel('BumpProducts');
        $latitude = $this->request->getQuery('latitude', $this->getLatLng()['latitude']);
        $longitude = $this->request->getQuery('longitude', $this->getLatLng()['longitude']);
        $contain = [
            'Product' => [
                'PostImage',
                'conditions' => [
                    "(
                        3959 * ACOS(
                            COS(RADIANS({$latitude})) * COS(
                                RADIANS(Product.product_lat)
                            ) * COS(
                                RADIANS(Product.product_lng) - RADIANS({$longitude})
                            ) + SIN(RADIANS({$latitude})) * SIN(
                                RADIANS(Product.product_lat)
                            )
                        )
                    ) <= 50"
                ]
            ]
        ];
        if ($this->Auth->user()) {
            $contain['Product']['Favourite'] = function ($q) {
                return $q->where(['Favourite.user_id' => $this->Auth->user('id')]);
            };
        }
        $datas = $this->BumpProducts->find('all', [
            'conditions' => [
                'Product.product_is_sold' => 0,
                'DATE_ADD(BumpProducts.created_at, INTERVAL BumpProducts.bump_days DAY) >= DATE(NOW())'
            ],
            'contain' => $contain,
            'order' => ['BumpProducts.created_at DESC'],
            'group' => 'Product.id'
        ]);
        $this->set([
            'datas' => $datas,
            '_serialize' => 'datas',
        ]);
        return $this->RequestHandler->renderAs($this, 'json');
    }

    public function searchKeyword()
    {
        $datas = [];
        if (!empty($this->request->getQuery('term'))) {

            $latitude = $this->request->getQuery('latitude', $this->getLatLng()['latitude']);
            $longitude = $this->request->getQuery('longitude', $this->getLatLng()['longitude']);
            $this->loadModel('Product');
            $keywords = $this->Product->find('all', [
                'fields' => [
                    'Product.product_title',
                    'distance' => "(
                    3959 * ACOS(
                        COS(RADIANS({$latitude})) * COS(
                            RADIANS(Product.product_lat)
                        ) * COS(
                            RADIANS(Product.product_lng) - RADIANS({$longitude})
                        ) + SIN(RADIANS({$latitude})) * SIN(
                            RADIANS(Product.product_lat)
                        )
                    )
                )"
                ],
                'conditions' => [
                    'LOWER(Product.product_title) LIKE' => '%' . strtolower($this->request->getQuery('term')) . '%'
                ],
                'limit' => 5,
                'group' => 'Product.product_title HAVING distance <= ' . (int)$this->request->getQuery('distance', 50),
            ]);
            foreach ($keywords as $keyword) {
                array_push($datas, [
                    'label' => $keyword->product_title,
                    'value' => $keyword->product_title
                ]);
            }
        }

        $this->set([
            'datas' => $datas,
            '_serialize' => 'datas',
        ]);
        return $this->RequestHandler->renderAs($this, 'json');
    }

    public function product($id)
    {
        $contain = [
            'Users',
            'PostImages',
            'BumpProducts'
        ];
        if ($this->Auth->user()) {
            $contain['Favourite'] = function ($q) {
                return $q->where(['Favourite.user_id' => $this->Auth->user('id')]);
            };
            $fields[] = 'Favourite.id';
        }
        $this->loadModel('Product');
        $product = $this->Product->get($id, [
            'contain' => $contain
        ]);
        $topListings = $this->Product->find('all', [
            'conditions' => [
                'Product.product_is_sold' => 0,
                'Product.uploaded_by_user_id' => $product->uploaded_by_user_id
            ],
            'contain' => [
                'PostImage'
            ],
            'order' => ['Product.created_at DESC'],
            'group' => 'Product.id'
        ]);
        $this->loadModel('Ads');
        $bottomBanner = $this->Ads->find('all', [
            'conditions' => [
                'NOW() BETWEEN start_Date AND end_date',
                'position' => '9'
            ]
        ]);
        $this->set('bottomBanner', $bottomBanner);
        $this->set(compact('product', 'topListings'));
    }

    public function paypal($amount, $product_id)
    {
        if (!in_array($amount, ['20', '35', '50'])) {
            $this->Flash->error('Invalid amount.');
            $this->redirect($this->referer());
        }
        $this->loadComponent('Paypal');
        try {
            $order = [
                'product_id' => $product_id,
                'description' => 'Bingalo Bump Product Subscription',
                'currency' => 'ILS',
                'return' => Router::url('/payment-confirmation/paypal/', true),
                'cancel' => Router::url('/payment-canceled', true),
                'shipping' => $amount + 10,
                'items' => [
                    [
                        'name' => 'Bingalo Package',
                        'description' => 'Bingalo Bump Product Subscription',
                        'tax' => 0.00,
                        'subtotal' => 0.00,
                        'qty' => 1,
                    ]
                ]
            ];
            $response = $this->Paypal->setExpressCheckout($order);
            $this->request->getSession()->write('checkout_details', $order);
            $this->redirect($response);
        } catch (\Exception $e) {
            $this->Flash->error($e->getMessage());
        }
        $this->redirect($this->referer());
    }

    public function cancleTransaction()
    {
        $this->Flash->success(__('Payment Cancelled'));
        $this->redirect($this->referer());
    }

    public function paypalConfirmation()
    {
        $days = [
            '30.00' => '3',
            '45.00' => '7',
            '60.00' => '15'
        ];
        $this->loadComponent('Paypal');
        $this->loadModel('Payments');
        $this->loadModel('Product');
        $this->loadModel('BumpProducts');
        try {
            $order = $this->request->getSession()->read('checkout_details');
            $response = $this->Paypal->doExpressCheckoutPayment($order, $this->request->getQuery('token'), $this->request->getQuery('PayerID'));
            $payment = $this->Payments->newEntity([
                'txnid' => $response['PAYMENTINFO_0_TRANSACTIONID'],
                'payment_amount' => $response['PAYMENTINFO_0_AMT'],
                'payment_status' => $response['PAYMENTINFO_0_PAYMENTSTATUS'],
                'itemid' => $order['product_id']
            ]);
            $this->Payments->save($payment);
            $product = $this->Product->get($order['product_id']);
            $product->bump_data = 1;
            $this->Product->save($product);
            $bump_data = $this->BumpProducts->newEntity([
                'product_id' => $order['product_id'],
                'bump_date' => date('Y-m-d h:i:s'),
                'bump_days' => $days[$response['PAYMENTINFO_0_AMT']],
                'bump_user_id' => $this->Auth->user('id'),
                'bump_price' => $response['PAYMENTINFO_0_AMT'],
                'payment_mode' => 'Paypal'
            ]);
            $this->BumpProducts->save($bump_data);
            $this->Flash->success(__("Congrats! You’re item was successfully Bumped!\nA confirmation email regarding your Top Listing will be sent shortly."));
            $this->request->getSession()->delete('checkout_details');
        } catch (\App\Controller\Component\PaypalRedirectException $e) {
            $this->redirect($e->getMessage());
        } catch (\Exception $e) {
            $this->Flash->error($e->getMessage());
        }
        return $this->redirect('/');
    }

    public function stripe()
    {
        $this->loadModel('Payments');
        $this->loadModel('Product');
        $this->loadModel('BumpProducts');
        $amount = $this->request->getData('amount');
        $product_id = $this->request->getData('product_id');
        $days = [
            '20' => '3',
            '35' => '7',
            '50' => '15'
        ];
        if (!in_array($amount, ['20', '35', '50'])) {
            $this->Flash->error('Invalid amount.');
            $this->redirect($this->referer());
        }
        try {
            \Stripe\Stripe::setApiKey('sk_test_51J8PosLTp72IeRovFlrfF6X2BckfIPhO0atrTy5oSnBDzgE6L5aXU2hOgU9AApkDSDFZC016d1M9yGZAh6samY9O00momho1Hf');
            $response = \Stripe\Charge::create([
                "amount" => $amount * 100,
                "currency" => "usd",
                "source" => $this->request->getData('stripeToken'),
                "description" => "Bingalo Bump Product Subscription"
            ]);
        } catch (\Exception $e) {
            $this->Flash->error($e->getMessage());
            return $this->redirect($this->referer());
        }
        if ($response->paid != true) {
            $this->Flash->error('Card not accepted.');
            return $this->redirect($this->referer());
        }
        $payment = $this->Payments->newEntity([
            'txnid' => $response->balance_transaction,
            'payment_amount' => $amount,
            'payment_status' => $response->status,
            'itemid' => $product_id
        ]);
        $this->Payments->save($payment);
        $product = $this->Product->get($product_id);
        $product->bump_data = 1;
        $this->Product->save($product);
        $bump_data = $this->BumpProducts->newEntity([
            'product_id' => $product_id,
            'bump_date' => date('Y-m-d h:i:s'),
            'bump_days' => $days[$amount],
            'bump_user_id' => $this->Auth->user('id'),
            'bump_price' => $amount,
            'payment_mode' => 'Paypal'
        ]);
        $this->BumpProducts->save($bump_data);
        $this->Flash->success(__('Congratulations! Your Product has been Bumped!'));
        return $this->redirect('/');
    }

    public function gpay()
    {
        $this->loadModel('Payments');
        $this->loadModel('Product');
        $this->loadModel('BumpProducts');
        $days = [
            '20' => '3',
            '35' => '7',
            '50' => '15'
        ];
        $gateway = new \Braintree\Gateway([
            'environment' => 'sandbox',
            'merchantId' => '8qvhbp66n65srzv5',
            'publicKey' => '6s33t5mnp54hbfz3',
            'privateKey' => '51899b14e549990066390248be3d61bc'
        ]);

        $result = $gateway->transaction()->sale([
            'amount' => $this->request->getQuery('amount'),
            'paymentMethodNonce' => $this->request->getQuery('payerID'),
            'options' => [
                'submitForSettlement' => true
            ],
            'deviceData' => []
        ]);

        if ($result->success) {
            $payment = $this->Payments->newEntity([
                'txnid' => $result->transaction->id,
                'payment_amount' => $this->request->getQuery('amount'),
                'payment_status' => 'paid',
                'itemid' => $this->request->getQuery('productId')
            ]);
            $this->Payments->save($payment);
            $product = $this->Product->get($this->request->getQuery('productId'));
            $product->bump_data = 1;
            $this->Product->save($product);
            $bump_data = $this->BumpProducts->newEntity([
                'product_id' => $this->request->getQuery('productId'),
                'bump_date' => date('Y-m-d h:i:s'),
                'bump_days' => $days[$this->request->getQuery('amount')],
                'bump_user_id' => $this->Auth->user('id'),
                'bump_price' => $this->request->getQuery('amount'),
                'payment_mode' => 'GPay'
            ]);
            $this->BumpProducts->save($bump_data);
            $this->Flash->success(__('Congratulations! Your Product has been Bumped!'));
        } else if ($result->transaction) {
            $this->Flash->success(__($result->transaction->processorResponseText));
        } else {
            foreach ($result->errors->deepAll() as $error) {
                $this->Flash->success(__($error->message));
            }
        }
        $this->redirect('/');
    }

    public function apple()
    {
        $this->loadModel('Payments');
        $this->loadModel('Product');
        $this->loadModel('BumpProducts');
        $days = [
            '20' => '3',
            '35' => '7',
            '50' => '15'
        ];
        $gateway = new \Braintree\Gateway([
            'environment' => 'sandbox',
            'merchantId' => '8qvhbp66n65srzv5',
            'publicKey' => '6s33t5mnp54hbfz3',
            'privateKey' => '51899b14e549990066390248be3d61bc'
        ]);

        $result = $gateway->transaction()->sale([
            'amount' => $this->request->getQuery('amount'),
            'paymentMethodNonce' => $this->request->getQuery('payerID'),
            'options' => [
                'submitForSettlement' => true
            ],
            'deviceData' => []
        ]);

        if ($result->success) {
            $payment = $this->Payments->newEntity([
                'txnid' => $result->transaction->id,
                'payment_amount' => $this->request->getQuery('amount'),
                'payment_status' => 'paid',
                'itemid' => $this->request->getQuery('productId')
            ]);
            $this->Payments->save($payment);
            $product = $this->Product->get($this->request->getQuery('productId'));
            $product->bump_data = 1;
            $this->Product->save($product);
            $bump_data = $this->BumpProducts->newEntity([
                'product_id' => $this->request->getQuery('productId'),
                'bump_date' => date('Y-m-d h:i:s'),
                'bump_days' => $days[$this->request->getQuery('amount')],
                'bump_user_id' => $this->Auth->user('id'),
                'bump_price' => $this->request->getQuery('amount'),
                'payment_mode' => 'Apple'
            ]);
            $this->BumpProducts->save($bump_data);
            $this->Flash->success(__('Congratulations! Your Product has been Bumped!'));
        } else if ($result->transaction) {
            $this->Flash->success(__($result->transaction->processorResponseText));
        } else {
            foreach ($result->errors->deepAll() as $error) {
                $this->Flash->success(__($error->message));
            }
        }
        $this->redirect('/');
    }

    public function profile($id = null)
    {
        $favourite_required = false;
        $this->loadModel('Users');
        $this->loadModel('Product');
        $user = $this->Users->get(is_null($id) ? $this->Auth->user('id') : $id);
        $contain = [
            'PostImage'
        ];
        if ($this->Auth->user()) {
            $contain['Favourite'] = function ($q) {
                return $q->where(['Favourite.user_id' => $this->Auth->user('id')]);
            };
        }
        $selling = $this->Product->find('all', [
            'contain' => $contain,
            'conditions' => [
                'Product.product_is_sold' => 0,
                'Product.uploaded_by_user_id' => $user->id
            ],
            'group' => 'Product.id',
            'order' => ['Product.created_at DESC']
        ]);
        $sold = $this->Product->find('all', [
            'contain' => $contain,
            'conditions' => [
                'Product.product_is_sold' => 1,
                'Product.uploaded_by_user_id' => $user->id
            ],
            'group' => 'Product.id',
            'order' => ['Product.created_at DESC']
        ]);
        if ($id == $this->Auth->user('id') || $id == null) {
            $favourite = $this->Product->find('all', [
                'contain' => $contain,
                'conditions' => [
                    'Favourite.id IS NOT' => null,
                    'Product.uploaded_by_user_id' => $user->id
                ],
                'group' => 'Product.id',
                'order' => ['Product.created_at DESC']
            ]);
            $favourite_required = true;
        } else {
            $favourite = [];
        }
        $rating = $this->Users->Reviews->find('all', ['fields' => ['averageRating' => 'AVG(Reviews.rating)'], 'conditions' => ['review_to' => $user->id]])->first();
        if (!empty($rating)) {
            $user->rating = $rating->averageRating;
        } else {
            $user->rating = 0;
        }
        $this->set(compact('user', 'selling', 'sold', 'favourite', 'favourite_required'));
    }

    public function sendEmailOTP()
    {
        $this->loadModel('Users');
        $user = $this->Users->find('all', [
            'conditions' => [
                'email' => $this->request->getQuery('email')
            ]
        ]);
        if ($user->count()) {
            $this->set([
                'status' => false,
                '_serialize' => 'status',
            ]);
            return $this->RequestHandler->renderAs($this, 'json');
        }
        $this->loadModel('UserOtp');
        $this->loadComponent('Notify');
        $this->Notify->sendEmailOtp($this->request->getQuery('email'));
        $this->set([
            'status' => true,
            '_serialize' => 'status',
        ]);
        return $this->RequestHandler->renderAs($this, 'json');
    }

    public function maintance()
    {
        $this->loadModel('AppMaintaince');
        $app_maintaince = $this->AppMaintaince->get(1, [
            'contain' => [],
        ]);
        if ($app_maintaince->is_maintaince != 1) {
            return $this->redirect('/');
        }
    }

    public function setLanguage($lang)
    {
        $this->loadModel('IpLanguages');
        $ip = $this->IpLanguages->find('all', [
            'conditions' => [
                'ip' => $this->request->clientIp()
            ]
        ]);
        if ($ip->count()) {
            $ip = $ip->first();
            $ip->language = $lang;
        } else {
            $ip = $this->IpLanguages->newEntity([
                'ip' => $this->request->clientIp(),
                'language' => $lang
            ]);
        }
        $this->IpLanguages->save($ip);
        die;
    }
}
