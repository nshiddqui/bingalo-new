<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Aws\S3\S3Client;

class AmazonComponent extends Component
{
    public $config = null;
    public $s3 = null;

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->config = [
            's3' => [
                'key' => 'AKIASVCWKAXRVNZBY5GU',
                'secret' => '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',
                'bucket' => 'bingalo',
            ]
        ];
        $this->s3 = S3Client::factory([
            'credentials' => [
                'key' => $this->config['s3']['key'],
                'secret' => $this->config['s3']['secret']
            ],
            'region' => 'us-west-1',
            'version' => 'latest'
        ]);
    }
    public function upload($filepath, $type, $awsFilePath = null)
    {
        if (is_null($awsFilePath)) {
            $awsFilePath = time() . '_' . basename($filepath);
        }
        try {
            /*** uploading file ***/
            $result = $this->s3->putObject([
                'Bucket'     => $this->config['s3']['bucket'],
                'Key'        => $type . '/' . $awsFilePath,
                'SourceFile' => $filepath
            ]);

            gc_collect_cycles();
            //echo $result['ObjectURL'];die;
            return $result['ObjectURL'];
        } catch (S3Exception $e) {

            $this->log('AWS S3 Upload Error Source: ' . $filepath . ' Dest: ' . $this->config['s3']['bucket'] . '/' . $awsFilePath);
            return false;
        }
    }
}
