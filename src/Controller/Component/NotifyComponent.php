<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Mailer\Email;
use Mailgun\Mailgun;

class NotifyComponent extends Component
{

    private $mailgun_key = 'key-3363e829764300f51baea4f7af12ef07';
    private $mailgun_domain = 'email.bingalo.com';

    public function initialize(array $config)
    {
        $this->Controller = $this->_registry->getController();
        parent::initialize($config);
    }

    public function sendWelcomeEmail($user)
    {
        if (is_numeric($user)) {
            $this->Controller->loadModel('Users');
            $user = $this->Controller->Users->get($user);
        }
        if (empty($user->email)) {
            return false;
        }
        $mgClient = new Mailgun($this->mailgun_key);

        # Make the call to the client.
        $mgClient->sendMessage(
            $this->mailgun_domain,
            array(
                'from'    => 'Bingalo <postmaster@email.bingalo.com>',
                'to'      => $user->email,
                'subject' => 'Welcome to Bingalo',
                'template'    => 'welcome-bingalo-2',
            )
        );
        return true;
    }

    public function sendEmailOtp($email)
    {
        if (empty($email)) {
            return false;
        }
        $otp = rand(111111, 999999);
        $this->Controller->loadModel('UserOtp');
        $user_otp = $this->Controller->UserOtp->find('all', [
            'conditions' => [
                'send_to' => $email
            ]
        ]);
        if ($user_otp->count() === 0) {
            $user_otp = $this->Controller->UserOtp->newEntity([
                'send_to' => $email
            ]);
        } else {
            $user_otp = $user_otp->first();
        }
        $user_otp->otp = $otp;
        $this->Controller->UserOtp->save($user_otp);

        $mgClient = new Mailgun($this->mailgun_key);

        # Make the call to the client.
        $mgClient->sendMessage(
            $this->mailgun_domain,
            array(
                'from'    => 'Bingalo <postmaster@email.bingalo.com>',
                'to'      => $email,
                'subject' => 'Email OTP Confirmation',
                'template'    => 'bingalaotp',
                'h:X-Mailgun-Variables'    => json_encode(['otp' => $otp])
            )
        );
        return true;
    }

    public function sendResetPasswordEmail($user)
    {
        if (is_numeric($user)) {
            $this->Controller->loadModel('Users');
            $user = $this->Controller->Users->get($user);
        }
        if (empty($user->email)) {
            return false;
        }
        $email = new Email('default');
        $email->setFrom("no-reply@mail.bingalo.com", "Bingalo");
        $email->setSubject("Bingalo Password Reset");
        $email->viewBuilder()->setTemplate('reset_password');
        $email->setEmailFormat('html');
        $email->setViewVars(compact('user'));
        $email->setTo($user->email);
        $email->send();
        return true;
    }
}
