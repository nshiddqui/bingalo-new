<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Routing\Router;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3/en/controllers/pages-controller.html
 */
class AuthorizedController extends AppController
{


    public function favourite($id)
    {
        $this->loadModel('Favourite');
        $data = $this->Favourite->find('all', ['conditions' => ['user_id' => $this->Auth->user('id'), 'product_id' => $id]]);
        if ($data->count()) {
            $this->Favourite->delete($data->first());
            $this->Flash->error('Product has been removed from favourite');
        } else {
            $favourite = $this->Favourite->newEntity();
            $favourite = $this->Favourite->patchEntity($favourite, ['user_id' => $this->Auth->user('id'), 'product_id' => $id]);
            $this->Favourite->save($favourite);
            $this->Flash->success('Product has been added on favourite');
        }
        return $this->redirect($this->referer());
    }

    public function sendEmailOTP()
    {
        $this->loadModel('UserVerification');
        $this->loadModel('Curl');
        $this->Curl->updateVerification([
            'phone' => $this->request->getQuery('phone'),
            'email' => $this->request->getQuery('email')
        ]);
        die;
    }

    public function socialVerification()
    {
        $this->loadModel('UserOtp');
        $this->loadModel('Users');
        $this->loadModel('UserVerification');
        $data = $this->UserOtp->find('all', ['conditions' => [
            'send_to' => !empty($this->request->getData('email')) ? $this->request->getData('email') : $this->request->getData('phone'),
            'otp' => $this->request->getData('otp')
        ]]);
        if ($data->count()) {
            $data = $this->UserVerification->find('all', ['conditions' => [
                'user_id' => $this->Auth->user('id'),
                'verified_by' => !empty($this->request->getData('email')) ? 'email' : 'phone'
            ]]);
            if ($data->count() == 0) {
                $verification = $this->UserVerification->newEntity();
                $verification = $this->UserVerification->patchEntity($verification, [
                    'user_id' => $this->Auth->user('id'),
                    'verified_by' =>  !empty($this->request->getData('email')) ? 'email' : 'phone',
                    'verified_status' => '1',
                    'created_at' => date('Y-m-d h:i:s')
                ]);
                $this->UserVerification->save($verification);
                $user = $this->Users->get($this->Auth->user('id'));
                $user->phone_verified = 1;
                $user->phone = $this->request->getData('phone');
                $this->Users->save($user);
                $userData = $this->Auth->user();
                $userData['phone'] = $this->request->getData('phone');
                $userData['phone_verified'] = 1;
                $this->Auth->setUser($userData);
            }
        } else {
            $this->Flash->error(__('Invalid OTP.'));
        }
        $this->redirect($this->referer());
    }
}
