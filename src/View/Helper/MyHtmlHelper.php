<?php

namespace App\View\Helper;

use Cake\View\Helper\HtmlHelper;

class MyHtmlHelper extends HtmlHelper {

    public function component($path, $type = 'css', array $options = array()) {
        $path = '/component/' . $path;
        return parent::{$type}($path, $options);
    }

    public function image($path, array $options = array()) {
        // $options['onerror'] = "this.onerror=null;this.src='/images/not-found.png';";
        if (is_string($path)) {
            $path = $this->Url->image($path, $options);
        } else {
            $path = $this->Url->build($path, $options);
        }
        if (isset($options['lazy']) && $options['lazy']) {
            $options['data-src'] = $path;
            $path = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
        }
        return parent::image($path, $options);
    }
}
