<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ads Model
 *
 * @method \App\Model\Entity\Ad get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ad newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ad[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ad|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ad saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ad patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ad[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ad findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AdsTable extends Table
{

        /**
         * Initialize method
         *
         * @param array $config The configuration for the Table.
         * @return void
         */
        public function initialize(array $config)
        {
                parent::initialize($config);

                $this->setTable('ads');
                $this->setDisplayField('id');
                $this->setPrimaryKey('id');

                $this->addBehavior('Timestamp');
        }

        /**
         * Default validation rules.
         *
         * @param \Cake\Validation\Validator $validator Validator instance.
         * @return \Cake\Validation\Validator
         */
        public function validationDefault(Validator $validator)
        {
                $validator
                        ->integer('id')
                        ->allowEmptyString('id', null, 'create');

                $validator
                        ->scalar('image')
                        ->maxLength('image', 255)
                        ->requirePresence('image', 'create')
                        ->notEmptyFile('image');

                $validator
                        ->scalar('link')
                        ->maxLength('link', 255)
                        ->requirePresence('link', 'create')
                        ->notEmptyString('link');

                $validator
                        ->dateTime('start_date')
                        ->requirePresence('start_date', 'create')
                        ->notEmptyDateTime('start_date');

                $validator
                        ->dateTime('end_date')
                        ->requirePresence('end_date', 'create')
                        ->notEmptyDateTime('end_date');

                $validator
                        ->integer('position')
                        ->requirePresence('position', 'create')
                        ->notEmptyString('position');

                $validator
                        ->scalar('title')
                        ->maxLength('title', 255)
                        ->allowEmptyString('title');

                return $validator;
        }

        public function getPosition()
        {
                return [
                        1 => 'iOS and Android',
                        2 => 'Web Left Ad (261x268)',
                        3 => 'Web Top Ad (1479x295)',
                        4 => 'Web Mobile Top Ad (350x119)',
                ];
        }
}
