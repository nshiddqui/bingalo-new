<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserVerification Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\UserVerification get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserVerification newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserVerification[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserVerification|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserVerification saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserVerification patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserVerification[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserVerification findOrCreate($search, callable $callback = null, $options = [])
 */
class UserVerificationTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_verification');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('verified_by')
            ->maxLength('verified_by', 255)
            ->requirePresence('verified_by', 'create')
            ->notEmptyString('verified_by');

        $validator
            ->integer('verified_status')
            ->requirePresence('verified_status', 'create')
            ->notEmptyString('verified_status');

        $validator
            ->scalar('otp')
            ->maxLength('otp', 20)
            ->notEmptyString('otp');

        $validator
            ->scalar('created_at')
            ->maxLength('created_at', 255)
            ->notEmptyString('created_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
