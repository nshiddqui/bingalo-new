<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReportedContent Model
 *
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\BelongsTo $Posts
 * @property \App\Model\Table\ReportedUsersTable&\Cake\ORM\Association\BelongsTo $ReportedUsers
 *
 * @method \App\Model\Entity\ReportedContent get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReportedContent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReportedContent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReportedContent|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReportedContent saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReportedContent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReportedContent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReportedContent findOrCreate($search, callable $callback = null, $options = [])
 */
class ReportedContentTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('reported_content');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Product', [
            'foreignKey' => 'post_id',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('ReportedUsers', [
            'className' => 'Users',
            'foreignKey' => 'reported_by',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'reported_user_id',
            'joinType' => 'LEFT',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('reported_reason')
                ->maxLength('reported_reason', 255)
                ->requirePresence('reported_reason', 'create')
                ->notEmptyString('reported_reason');

        $validator
                ->scalar('reported_desc')
                ->maxLength('reported_desc', 1000)
                ->requirePresence('reported_desc', 'create')
                ->notEmptyString('reported_desc');

        $validator
                ->dateTime('created_at')
                ->notEmptyDateTime('created_at');

        $validator
                ->scalar('reported_by')
                ->maxLength('reported_by', 255)
                ->requirePresence('reported_by', 'create')
                ->notEmptyString('reported_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['post_id'], 'Posts'));
        $rules->add($rules->existsIn(['reported_user_id'], 'ReportedUsers'));

        return $rules;
    }

}
