<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AppInfos Model
 *
 * @method \App\Model\Entity\AppInfo get($primaryKey, $options = [])
 * @method \App\Model\Entity\AppInfo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AppInfo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AppInfo|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AppInfo saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AppInfo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AppInfo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AppInfo findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AppInfosTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('app_infos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('logo_image')
                ->maxLength('logo_image', 255)
                ->requirePresence('logo_image', 'create')
                ->notEmptyFile('logo_image');


        $validator
                ->scalar('facebook')
                ->maxLength('facebook', 255)
                ->allowEmptyString('facebook');

        $validator
                ->scalar('twitter')
                ->maxLength('twitter', 255)
                ->allowEmptyString('twitter');

        $validator
                ->scalar('instagram')
                ->maxLength('instagram', 255)
                ->allowEmptyString('instagram');

        $validator
                ->scalar('linkedin')
                ->maxLength('linkedin', 255)
                ->allowEmptyString('linkedin');

        $validator
                ->scalar('youtube')
                ->maxLength('youtube', 255)
                ->allowEmptyString('youtube');

        $validator
                ->scalar('pinterest')
                ->maxLength('pinterest', 255)
                ->allowEmptyString('pinterest');

        $validator
                ->scalar('vimeo')
                ->maxLength('vimeo', 255)
                ->allowEmptyString('vimeo');

        $validator
                ->scalar('appstore')
                ->maxLength('appstore', 255)
                ->allowEmptyString('appstore');

        $validator
                ->scalar('playstore')
                ->maxLength('playstore', 255)
                ->allowEmptyString('playstore');
        return $validator;
    }

}
