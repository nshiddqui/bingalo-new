<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AppMaintaince Model
 *
 * @method \App\Model\Entity\AppMaintaince get($primaryKey, $options = [])
 * @method \App\Model\Entity\AppMaintaince newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AppMaintaince[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AppMaintaince|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AppMaintaince saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AppMaintaince patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AppMaintaince[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AppMaintaince findOrCreate($search, callable $callback = null, $options = [])
 */
class AppMaintainceTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('app_maintaince');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('maintaince_message')
            ->maxLength('maintaince_message', 3000)
            ->requirePresence('maintaince_message', 'create')
            ->notEmptyString('maintaince_message');

        $validator
            ->scalar('he_maintaince_message')
            ->maxLength('he_maintaince_message', 3000)
            ->requirePresence('he_maintaince_message', 'create')
            ->notEmptyString('he_maintaince_message');

        $validator
            ->scalar('maintaince_detail')
            ->maxLength('maintaince_detail', 3000)
            ->requirePresence('maintaince_detail', 'create')
            ->notEmptyString('maintaince_detail');

        $validator
            ->scalar('he_maintaince_detail')
            ->maxLength('he_maintaince_detail', 3000)
            ->requirePresence('he_maintaince_detail', 'create')
            ->notEmptyString('he_maintaince_detail');

        $validator
            ->integer('is_maintaince')
            ->requirePresence('is_maintaince', 'create')
            ->notEmptyString('is_maintaince');

        $validator
            ->dateTime('created_at')
            ->notEmptyDateTime('created_at');

        $validator
            ->dateTime('updated_at')
            ->notEmptyDateTime('updated_at');

        return $validator;
    }
}
