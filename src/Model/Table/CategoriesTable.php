<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Categories Model
 *
 * @method \App\Model\Entity\Category get($primaryKey, $options = [])
 * @method \App\Model\Entity\Category newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Category[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Category|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Category[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Category findOrCreate($search, callable $callback = null, $options = [])
 */
class CategoriesTable extends Table {
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('categories');
        $this->setDisplayField('category_name');
        $this->setPrimaryKey('id');
        $this->hasMany('SubCategories', [
            'foreignKey' => 'category_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('category_name')
            ->maxLength('category_name', 255)
            ->requirePresence('category_name', 'create')
            ->notEmptyString('category_name');

        $validator
            ->scalar('herbew_category_name')
            ->maxLength('herbew_category_name', 255)
            ->requirePresence('herbew_category_name', 'create')
            ->notEmptyString('herbew_category_name');

        $validator
            ->scalar('category_image')
            ->maxLength('category_image', 255)
            ->requirePresence('category_image', 'create')
            ->notEmptyFile('category_image');

        return $validator;
    }

    public function getCategories() {
        $datas = $this->find('all', [
            'fields' => [
                'Categories.category_name',
                'SubCategories.name'
            ],
            'join' => [
                'alias' => 'SubCategories',
                'table' => 'sub_categories',
                'type' => 'INNER',
                'conditions' => [
                    'SubCategories.category_id = Categories.id'
                ]
            ],
            'group' => ['SubCategories.name']
        ]);
        $options = [];
        foreach ($datas as $data) {
            $options[$data['SubCategories']['name'] . '#' . $data->category_name] = $data['SubCategories']['name'];
        }
        return $options;
    }
}
