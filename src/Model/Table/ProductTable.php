<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Product Model
 *
 * @property \App\Model\Table\UploadedByUsersTable&\Cake\ORM\Association\BelongsTo $UploadedByUsers
 * @property \App\Model\Table\BumpProductsTable&\Cake\ORM\Association\HasMany $BumpProducts
 * @property \App\Model\Table\FavouriteTable&\Cake\ORM\Association\HasMany $Favourite
 * @property \App\Model\Table\NotficationsTable&\Cake\ORM\Association\HasMany $Notfications
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null, $options = [])
 */
class ProductTable extends Table
{

        /**
         * Initialize method
         *
         * @param array $config The configuration for the Table.
         * @return void
         */
        public function initialize(array $config)
        {
                parent::initialize($config);

                $this->setTable('product');
                $this->setDisplayField('id');
                $this->setPrimaryKey('id');

                $this->belongsTo('Users', [
                        'foreignKey' => 'uploaded_by_user_id',
                        'joinType' => 'INNER',
                ]);
                $this->hasOne('BumpProducts', [
                        'foreignKey' => 'product_id',
                        'joinType' => 'LEFT',
                        'conditions' => [
                                '(DATEDIFF(BumpProducts.created_at, NOW()) BETWEEN 0 AND BumpProducts.bump_days)'
                        ]
                ]);
                $this->hasMany('PostImages', [
                        'foreignKey' => 'post_id',
                        'joinType' => 'LEFT',
                ]);
                $this->hasOne('PostImage', [
                        'className' => 'PostImages',
                        'foreignKey' => 'post_id',
                        'joinType' => 'INNER',
                ]);
                $this->hasOne('Favourite', [
                        'foreignKey' => 'product_id',
                        'joinType' => 'LEFT',
                ]);
        }

        /**
         * Default validation rules.
         *
         * @param \Cake\Validation\Validator $validator Validator instance.
         * @return \Cake\Validation\Validator
         */
        public function validationDefault(Validator $validator)
        {
                $validator
                        ->integer('id')
                        ->allowEmptyString('id', null, 'create');

                $validator
                        ->scalar('product_category')
                        ->maxLength('product_category', 255)
                        ->requirePresence('product_category', 'create')
                        ->notEmptyString('product_category');

                $validator
                        ->scalar('product_condition')
                        ->maxLength('product_condition', 255)
                        ->requirePresence('product_condition', 'create')
                        ->notEmptyString('product_condition');

                $validator
                        ->scalar('product_description')
                        ->requirePresence('product_description', 'create')
                        ->notEmptyString('product_description');

                $validator
                        ->integer('product_is_sold')
                        ->requirePresence('product_is_sold', 'create')
                        ->notEmptyString('product_is_sold');

                $validator
                        ->scalar('product_lat')
                        ->maxLength('product_lat', 255)
                        ->requirePresence('product_lat', 'create')
                        ->notEmptyString('product_lat');

                $validator
                        ->scalar('product_lng')
                        ->maxLength('product_lng', 255)
                        ->requirePresence('product_lng', 'create')
                        ->notEmptyString('product_lng');

                $validator
                        ->scalar('product_location')
                        ->maxLength('product_location', 255)
                        ->requirePresence('product_location', 'create')
                        ->notEmptyString('product_location');

                $validator
                        ->scalar('product_city')
                        ->maxLength('product_city', 255)
                        ->requirePresence('product_city', 'create')
                        ->notEmptyString('product_city');

                $validator
                        ->scalar('product_state')
                        ->maxLength('product_state', 255)
                        ->requirePresence('product_state', 'create')
                        ->notEmptyString('product_state');

                $validator
                        ->scalar('product_country')
                        ->maxLength('product_country', 255)
                        ->requirePresence('product_country', 'create')
                        ->notEmptyString('product_country');

                $validator
                        ->scalar('product_price')
                        ->maxLength('product_price', 255)
                        ->requirePresence('product_price', 'create')
                        ->notEmptyString('product_price');

                $validator
                        ->scalar('product_specifications')
                        ->maxLength('product_specifications', 255)
                        ->requirePresence('product_specifications', 'create')
                        ->notEmptyString('product_specifications');

                $validator
                        ->scalar('product_title')
                        ->maxLength('product_title', 255)
                        ->requirePresence('product_title', 'create')
                        ->notEmptyString('product_title');

                $validator
                        ->scalar('parent_category')
                        ->maxLength('parent_category', 255)
                        ->requirePresence('parent_category', 'create')
                        ->notEmptyString('parent_category');

                $validator
                        ->scalar('bump_data')
                        ->maxLength('bump_data', 255)
                        ->requirePresence('bump_data', 'create')
                        ->notEmptyString('bump_data');

                $validator
                        ->integer('rank')
                        ->requirePresence('rank', 'create')
                        ->notEmptyString('rank');

                $validator
                        ->dateTime('created_at')
                        ->notEmptyDateTime('created_at');

                $validator
                        ->dateTime('updated_at')
                        ->notEmptyDateTime('updated_at');

                return $validator;
        }

        /**
         * Returns a rules checker object that will be used for validating
         * application integrity.
         *
         * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
         * @return \Cake\ORM\RulesChecker
         */
        public function buildRules(RulesChecker $rules)
        {
                $rules->add($rules->existsIn(['uploaded_by_user_id'], 'Users'));

                return $rules;
        }
}
