<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Http\Client;
use Cake\Http\Session;
use Cake\Cache\Cache;
use Exception;
use Google\Cloud\Firestore\FirestoreClient;

class CurlTable extends Table
{

    public $Client;
    public $Session;
    public $CurlBaseUrl;
    public $limit;
    public $offset;

    public function initialize(array $config)
    {
        $this->CurlBaseUrl = 'https://api.bingalo.com/api/';
        $this->limit = 10;
        $this->offset = 0;
        $this->Client = new Client();
        $this->Session = new Session();
        $this->setTable(false);
    }

    public function getCategories()
    {
        // if (!$this->Session->check('getCategories')) {
        $response = $this->Client->get($this->CurlBaseUrl . 'getCategories');
        // $this->Session->write('getCategories', $response->getJson()['data']);
        // }
        return  $response->getJson()['data'];
    }

    public function searchNearbyProducts($latLng, $distance = 500000)
    {
        $response = $this->Client->get(
            $this->CurlBaseUrl . 'searchNearbyProducts',
            [
                'lat' => $latLng['latitude'],
                'lng' => $latLng['longitude'],
                'max_distance' => $distance,
                'limit' => $this->limit,
                'offset' => $this->offset
            ]
        )->getJson();
        return isset($response['data']) ? $response['data'] : [];
    }

    public function filterProduct($filterData = [])
    {
        if (isset($filterData['address'])) {
            $getLatLng = $this->getLatLngFromAddress($filterData['address']);
            $filterData['lat'] = $getLatLng['lat'];
            $filterData['lng'] = $getLatLng['lng'];
            unset($filterData['address']);
        }
        $filterData['limit'] = $this->limit;
        $filterData['offset'] = $this->offset;
        $response = $this->Client->get(
            $this->CurlBaseUrl . 'newSearchPoral',
            $filterData
        )->getJson();
        return isset($response['data']) ? $response['data'] : [];
    }

    public function getLatLngFromAddress($address)
    {
        if (($getLatLngFromAddress = Cache::read('getLatLngFromAddress.' . $address)) === false) {
            try {
                $getLatLngFromAddress = $this->Client->get(
                    'https://maps.googleapis.com/maps/api/geocode/json',
                    [
                        'key' => 'AIzaSyCdx1gFKWhFRwNC8-jVRKxpS1DJavfUh5g',
                        'address' => $address
                    ]
                )->getJson()['results'][0]['geometry']['location'];
            } catch (Exception $ex) {
                $getLatLngFromAddress = [
                    'lat' => 9999999999,
                    'lng' => 9999999999
                ];
            }
            Cache::write('getLatLngFromAddress.' . $address, $getLatLngFromAddress);
        }
        return $getLatLngFromAddress;
    }

    public function getSearchRankItem()
    {
        // if (!$this->Session->check('getTrendingSearches')) {
        $response = $this->Client->get($this->CurlBaseUrl . 'getTrendingSearches')->getJson();
        $getProductRankWise = [];
        if (isset($response['data']) && !empty($response['data'])) {
            foreach ($response['data'] as $data) {
                $getProductRankWise[] = $data['search_name'];
            }
        }
        // $this->Session->write('getTrendingSearches', $getProductRankWise);
        // }
        return $getProductRankWise;
    }

    public function getTopListings()
    {
        // $product = TableRegistry::getTableLocator()->get('Product');
        // $data = $product->find('all', [
        //     'fields' => [
        //         'Product.id',
        //         'Product.product_title',
        //         'Product.product_price',
        //         'Product.product_description',
        //         'Product.bump_data',
        //         'Product.product_is_sold',
        //         'Product.created_at',
        //         'images.image_url',
        //         'BumpProducts.id'
        //     ],
        //     'join' => [
        //         [
        //             'table' => 'bump_products',
        //             'alias' => 'BumpProducts',
        //             'conditions' => [
        //                 'BumpProducts.product_id = Product.id',
        //                 'DATE_ADD(BumpProducts.created_at, INTERVAL BumpProducts.bump_days DAY) <= DATE(NOW())'
        //             ]
        //         ],
        //         [
        //             'table' => 'post_images',
        //             'alias' => 'images',
        //             'conditions' => [
        //                 'images.post_id = Product.id',
        //             ]
        //         ]
        //     ]
        // ]);
        // if (!$this->Session->check('getProductRankWise')) {
        $response = $this->Client->get($this->CurlBaseUrl . 'getProductRankWise');
        //     $this->Session->write('getProductRankWise', $response->getJson()['data']);
        // }
        return $response->getJson()['data'];
    }

    public function updateTrendingRank($text)
    {
        $this->Client->post($this->CurlBaseUrl . 'update_trending_rank', ['search_text' => $text]);
    }

    public function searchKeyword($keyword, $latLng)
    {
        $response = $this->Client->get(
            $this->CurlBaseUrl . 'search_product',
            [
                'limit' => 10,
                'name' => $keyword,
                'lat' => $latLng['latitude'],
                'lng' => $latLng['longitude'],
                'max_distance' => 1000,
            ]
        )->getJson();
        return isset($response['data']) ? $response['data'] : [];
    }

    public function geProductDetails($id)
    {
        $response = $this->Client->get(
            $this->CurlBaseUrl . 'getSingleProductDetails',
            [
                'product_id' => $id,
            ]
        )->getJson();
        return isset($response['data']) ? $response['data'][0] : [];
    }

    public function getProductsByUserId($id, $status = 0)
    {
        $response = $this->Client->get(
            $this->CurlBaseUrl . 'getProductByUserId',
            [
                'status' => $status,
                'user_id' => $id,
            ]
        )->getJson();
        return isset($response['data']) ? $response['data'] : [];
    }

    public function login($data)
    {
        $response = $this->Client->post($this->CurlBaseUrl . 'login', $data)->getJson();
        if (isset($response['status']) && $response['status'] === 200) {
            $this->addUpdateUser($response['data']);
        }
        return $response['status'] === 200 ? $response['data'] : false;
    }

    public function signup($data)
    {
        $response = $this->Client->post($this->CurlBaseUrl . 'signup', $data)->getJson();
        if (isset($data['image']) && !empty($data['image'])) {
            $url = stream_get_meta_data($data['image'])['uri'];
            if (file_exists($url)) {
                @unlink($url);
            }
        }
        if (isset($response['status']) && $response['status'] === 200) {
            $this->addUpdateUser($response['data']);
        }
        return isset($response['message']) ? $response['message'] : 'Signup successful';
    }

    public function sellYourStuff($data)
    {
        $response = $this->Client->post($this->CurlBaseUrl . (isset($data['product_id']) ? 'edit_product' : 'postproduct'), $data)->getJson();
        foreach ($data['image'] as $existImage) {
            $url = stream_get_meta_data($existImage)['uri'];
            if (file_exists($url)) {
                @unlink($url);
            }
        }
        if (isset($response['status']) && $response['status'] == '403') {
            return false;
        }
        return isset($response['message']) ? $response['message'] : 'Post successful';
    }

    public function profile_pic($data)
    {
        $response = $this->Client->post($this->CurlBaseUrl . 'changeProfilePic', $data)->getStringBody();
        $url = stream_get_meta_data($data['image'])['uri'];
        if (file_exists($url)) {
            @unlink($url);
        }
        if (isset($response['status']) && $response['status'] == '403') {
            return false;
        }
        return isset($response['message']) ? $response['message'] : 'Profile picture successfully changed';
    }

    public function getUserProfile($id)
    {
        $response = $this->Client->get(
            $this->CurlBaseUrl . 'getProfileDetails',
            [
                'user_id' => $id,
            ]
        )->getJson();
        return isset($response['data']) ? $response['data'][0] : [];
    }

    public function chageProductIsSoldStatus($id)
    {
        $this->Client->post($this->CurlBaseUrl . 'chageProductIsSoldStatus', ['product_id' => $id, 'status' => '1']);
    }

    public function changePassword($data, $id)
    {
        $response = $this->Client->post($this->CurlBaseUrl . 'changePassword', [
            'oldPassword' => $data['old_password'],
            'newPassword' => $data['new_password'],
            'user_id' => $id
        ])->getJson();
        return isset($response['message']) ? $response['message'] : 'Your password has beed changed';
    }


    public function deleteProduct($id)
    {
        $this->Client->post($this->CurlBaseUrl . 'deleteproduct', ['product_id' => $id]);
    }

    public function addUpdatedProductBump($id, $user_id)
    {
        $data = $this->Client->post($this->CurlBaseUrl . 'addUpdatedProductBump', [
            'product_id' => $id,
            'bump_date' => date('Y-m-d'),
            'bump_days' => '1',
            'bump_user_id' => '$user_id',
            'bump_price' => '1.99'
        ]);
    }

    public function socialLogin($data)
    {
        $response = $this->Client->post($this->CurlBaseUrl . 'social_login', $data)->getJson();
        if (isset($response['status']) && $response['status'] === 200) {
            $this->addUpdateUser($response['data']);
        }
        return $response['status'] === 200 ? $response['data'] : false;
    }

    public function updateProfile($data)
    {
        $response = $this->Client->post($this->CurlBaseUrl . 'edit_profile', $data)->getJson();
        if (isset($data['image']) && !empty($data['image'])) {
            $url = stream_get_meta_data($data['image'])['uri'];
            if (file_exists($url)) {
                @unlink($url);
            }
        }
        if (isset($response['status']) && $response['status'] === 200) {
            $this->addUpdateUser($response['data']);
        }
        return $response['status'] === 200 ? $response['data'] : false;
    }

    public function addUpdateUser($data)
    {
        return;
        $db = new FirestoreClient([
            'projectId' => 'bingalo',
            'keyFile' => [
                "type" => "service_account",
                "project_id" => "bingalo",
                "private_key_id" => "04d338f9c6fda912b4a5d6ab77194e3f2dde9773",
                "private_key" => "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDCRr4bp1Uj2IYu\niZ7UytDL2sL67zlrkRgLNG5EfNY850Ip1FAEbK4LurLF3y3K9zpGhPJQpysjFtPo\nfTpzJQZ/3ZEbrl1KhnfQCPUAyb1emJAt0ziPpbmk87ktXNYn8EYajoB6HoBYCUrq\naEcNnOu73su1oSHYlmTlRSvSFqE+xAfqmNgMNmZ8EJ+6lfRVRBejIuVjpHd/+uzW\n+SS4kTQcMIgmBFfdqO1rofMFma2nZ+hl2YpmgRHHUe0AM2K8KloXYnhd2A8QoS+f\nCRTqKpyf1Q7l/OyT8sXYcrH4ev9ZpIodynVFb2d7ngjaa0C/pr1gtcqb7fyMuuev\nP36AH1eFAgMBAAECggEAAbS8U6LXCQ8MMplrxyOWzMt5fdme7/Zgtyx5MWnFOdiw\nCI0APOG1b43upvUsQkWEgf0F4bZuFwtKxhZfboZrNbE85njUAeUHZVDG0gBzhMKj\nNXvO2dgTNsMbI65WhRhDjVUmBKyC5QMD4OYUhaaUnVw5/rjRAsVjUm4Wkew380Si\newpzebZ0G/MuqxXVKUOpHYVkuFkS+NcA0fpYm8jxKRjWPFBGbWSt4N+4qu++Ax9b\nOOErLiDKCv+l4KCmtzlbApTjIIVn6bqqno1q2YZLOcRMmB4qhPvV7b06qosBsWoH\nNr9KrmWb7Qxi+P4u/bWfP8YHWyRaC7cdMZKX4jArgQKBgQD3eJaL1SXurCHzvjHj\nUxy0Z57CRsbjt8K5WZtq0Ne9eMHkRtXbCzokyy9if09JxiDJddrC2CzNUPepMtfb\nGmF4nkIQ/mKhoOXzIakceReuvUYYhycJLbYQX/2sJ/VMdY3+y4Qc258bDibURcrG\nTk4pR5+LE/O7hVAHPSYLx1KIRQKBgQDI+NKxkEjRfRBtJVCbTr52DpWZxnABsYtQ\nMI8QXc3zLHDwUNS/P9BNm4KPV5+ZGPCrnxWpZdz6DCzyf3/abQpjnTQbIA5Bj4Bd\nFmFZN9lvTcBmgTGvSZwp+5ynnJ9k28Ki/0+3987SB3XcjsZLyHuEIWpDdWy24RdK\nAoqtUHamQQKBgAICRJaWAFzToOLtpFIIFq+QN8Wz9WFb2ITQqWZQk6/CDzDfMlU4\n3hv2WkgeaHJc3V9FSaVTQ+nWwmvpMb9hk3D7N+YTapGNUNCMcWb9xKdyeeMxFawJ\nZvTzEogXOvKH8cMju8rQLZZ2ovIOLvs1H4VOQ4lJfC0FhATLBkgKMi1RAoGAbJrK\ngVpPf6XuMo1vuf4bvwCyp58s+D1R48vaBCYtHbRuc++6iH+budTIAV6AyONJa/VU\nZ268tAPQZluwZAF5E8voVKBGE/fQjn2Brg0YW6XQy5QVnXxR//4Q8iMZoA+CZ0NB\neVFZWvbdIoulbV6yIP1Nx5a1YT7SrxQgjPhmpgECgYEAlwPTKdUbZD0Uu6Pp1X1C\nykaVBEJeGZ9Q6wvPsCraDFC8fVdGe7wjMGaYXx0dCki7k6ChUCFrvl02IcpBvoD+\nvLA+CWY4DD7A/4zUiVAcDfcDppUvJm+GoxMMZ83G9636lPm5mrQPiX7pZGM08d0l\nChETU0ePKgu+3yDsnOyyjh4=\n-----END PRIVATE KEY-----\n",
                "client_email" => "firebase-adminsdk-v2blt@bingalo.iam.gserviceaccount.com",
                "client_id" => "115086025191919814477",
                "auth_uri" => "https://accounts.google.com/o/oauth2/auth",
                "token_uri" => "https://oauth2.googleapis.com/token",
                "auth_provider_x509_cert_url" => "https://www.googleapis.com/oauth2/v1/certs",
                "client_x509_cert_url" => "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-v2blt%40bingalo.iam.gserviceaccount.com"
            ]
        ]);
        $docRef = $db->collection('Users')->document($data['id']);
        $docRef->set([
            'email' => $data['email'],
            'id' => $data['id'],
            'name' => $data['full_name'],
            'profilePicLink' => $data['image']
        ]);
    }

    public function updateVerification($data)
    {
        $response = $this->Client->post($this->CurlBaseUrl . 'sendOTP', $data)->getJson();

        return $response['status'] === 200 ? $response : false;
    }

    public function forgotPassword($data)
    {
        $response = $this->Client->get($this->CurlBaseUrl . 'forgotPassword', $data)->getJson();
        return isset($response['message']) ? $response['message'] : 'Provided email does not exist.';
    }

    public  function deleteimage($id)
    {
        $this->Client->post($this->CurlBaseUrl . 'deleteimage', ['id' => $id]);
    }

    public function sendOTP($email)
    {
        $this->Client->post($this->CurlBaseUrl . 'sendOTP', ['email' => $email]);
    }
}
