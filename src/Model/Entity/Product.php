<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property string $product_category
 * @property string $product_condition
 * @property string $product_description
 * @property int $product_is_sold
 * @property string $product_lat
 * @property string $product_lng
 * @property string $product_location
 * @property string $product_city
 * @property string $product_state
 * @property string $product_country
 * @property string $product_price
 * @property string $product_specifications
 * @property string $product_title
 * @property string $uploaded_by_user_id
 * @property string $parent_category
 * @property string $bump_data
 * @property int $rank
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $updated_at
 *
 * @property \App\Model\Entity\UploadedByUser $uploaded_by_user
 * @property \App\Model\Entity\BumpProduct[] $bump_products
 * @property \App\Model\Entity\Favourite[] $favourite
 * @property \App\Model\Entity\Notfication[] $notfications
 */
class Product extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_category' => true,
        'product_condition' => true,
        'product_description' => true,
        'product_is_sold' => true,
        'product_lat' => true,
        'product_lng' => true,
        'product_location' => true,
        'product_city' => true,
        'product_state' => true,
        'product_country' => true,
        'product_price' => true,
        'product_specifications' => true,
        'product_title' => true,
        'uploaded_by_user_id' => true,
        'parent_category' => true,
        'bump_data' => true,
        'rank' => true,
        'created_at' => true,
        'updated_at' => true,
        'uploaded_by_user' => true,
        'bump_products' => true,
        'favourite' => true,
        'notfications' => true,
    ];
}
