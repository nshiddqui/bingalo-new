<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $full_name
 * @property string $email
 * @property string $phone
 * @property string $password
 * @property string $lat
 * @property string $lng
 * @property string $updated_at
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $facebook_id
 * @property string $google_id
 * @property string $image
 * @property string $image_name
 * @property int $language
 * @property string $token
 * @property string $twitter_id
 * @property string $apple_id
 * @property string $push_token
 *
 * @property \App\Model\Entity\Facebook $facebook
 * @property \App\Model\Entity\Google $google
 * @property \App\Model\Entity\Twitter $twitter
 * @property \App\Model\Entity\Apple $apple
 * @property \App\Model\Entity\Favourite[] $favourite
 * @property \App\Model\Entity\NotificationCount[] $notification_count
 * @property \App\Model\Entity\PostImage[] $post_images
 * @property \App\Model\Entity\UserCard[] $user_cards
 * @property \App\Model\Entity\UserReadNotification[] $user_read_notification
 * @property \App\Model\Entity\UserVerification[] $user_verification
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'full_name' => true,
        'email' => true,
        'phone' => true,
        'password' => true,
        'lat' => true,
        'phone_verified' => true,
        'lng' => true,
        'updated_at' => true,
        'created_at' => true,
        'facebook_id' => true,
        'google_id' => true,
        'singup_from' => true,
        'image' => true,
        'image_name' => true,
        'language' => true,
        'token' => true,
        'twitter_id' => true,
        'apple_id' => true,
        'push_token' => true,
        'facebook' => true,
        'google' => true,
        'twitter' => true,
        'apple' => true,
        'favourite' => true,
        'notification_count' => true,
        'post_images' => true,
        'user_cards' => true,
        'user_read_notification' => true,
        'user_verification' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token',
    ];
}
