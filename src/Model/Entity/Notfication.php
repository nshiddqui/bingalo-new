<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Notfication Entity
 *
 * @property int $id
 * @property string $notification_title
 * @property string $notification_description
 * @property int $notification_to
 * @property int $notification_by
 * @property int $notification_type
 * @property int $product_id
 * @property int $is_read
 * @property \Cake\I18n\FrozenTime $notification_date
 *
 * @property \App\Model\Entity\Product $product
 */
class Notfication extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'notification_title' => true,
        'notification_description' => true,
        'notification_to' => true,
        'notification_by' => true,
        'notification_type' => true,
        'notification_link' => true,
        'image' => true,
        'product_id' => true,
        'is_read' => true,
        'notification_date' => true,
        'product' => true,
    ];

}
