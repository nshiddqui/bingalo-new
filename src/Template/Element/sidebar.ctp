<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <?= $this->Html->image('user.png', ['class' => 'img-circle', 'alt' => 'User Image']) ?>
        </div>
        <div class="pull-left info">
            <p><?= $authUser['client_name'] ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
            <?= $this->Html->link('<i class="fa fa-dashboard"></i> <span>Dashboard</span>', ['controller' => 'dashboard'], ['escape' => false]) ?>
        </li>
        <li>
            <?= $this->Html->link('<i class="fa fa-users"></i> <span>Users Management</span>', ['controller' => 'users', 'action' => 'index'], ['escape' => false]) ?>
        </li>
        <li>
            <?= $this->Html->link('<i class="fa fa-line-chart"></i> <span>Ads Management</span>', ['controller' => 'ads', 'action' => 'index'], ['escape' => false]) ?>
        </li>
        <li>
            <?= $this->Html->link('<i class="fa fa-bell"></i> <span>Notification Management</span>', ['controller' => 'notfications', 'action' => 'index'], ['escape' => false]) ?>
        </li>
        <li>
            <?= $this->Html->link('<i class="fa fa-globe"></i> <span>Top Cities Management</span>', ['controller' => 'top_cities', 'action' => 'index'], ['escape' => false]) ?>
        </li>
        <li>
            <?= $this->Html->link('<i class="fa fa-suitcase"></i> <span>Categories Management</span>', ['controller' => 'categories', 'action' => 'index'], ['escape' => false]) ?>
        </li>
        <li>
            <?= $this->Html->link('<i class="fa fa-product-hunt"></i> <span>Products Management</span>', ['controller' => 'product', 'action' => 'index'], ['escape' => false]) ?>
        </li>
        <li>
            <?= $this->Html->link('<i class="fa fa-product-hunt"></i> <span>Bump Products Management</span>', ['controller' => 'bump_products', 'action' => 'index'], ['escape' => false]) ?>
        </li>
        <li>
            <?= $this->Html->link('<i class="fa fa-product-hunt"></i> <span>Sponsored Locations Management</span>', ['controller' => 'sponsored_locations', 'action' => 'index'], ['escape' => false]) ?>
        </li>
        <li class="treeview">
            <?= $this->Html->link('<i class="fa fa-file"></i> <span>Reported Content Management</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>', '#', ['escape' => false]) ?>
            <ul class="treeview-menu">
                <li><?= $this->Html->link('<i class="fa fa-circle-o"></i> Users Reported', ['controller' => 'reported_content', 'action' => 'index', '?' => ['content' => 0]], ['escape' => false]) ?></li>
                <li><?= $this->Html->link('<i class="fa fa-circle-o"></i> Products Reported', ['controller' => 'reported_content', 'action' => 'index', '?' => ['content' => 1]], ['escape' => false]) ?></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-list-alt"></i> <span>Page Managements</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <?php foreach ($page_lists as $sidebar_page_id => $sidebar_page) { ?>
                    <li><?= $this->Html->link("<i class='fa fa-circle-o'></i>{$sidebar_page} Page", ['controller' => 'pages', 'action' => 'index', $sidebar_page_id], ['escape' => false]) ?></li>
                <?php } ?>
            </ul>
        </li>
        <li>
            <?= $this->Html->link('<i class="fa fa-rocket"></i><span>App Info Management</span>', ['controller' => 'appInfos'], ['escape' => false]) ?>
        </li>
        <li>
            <?= $this->Html->link('<i class="fa fa-rocket"></i> <span> App Message Management</span>', ['controller' => 'messages'], ['escape' => false]) ?>
        </li>
    </ul>
</section>