<div class="account-popup login-popup popup-box hide-popup-box" style="overflow: hidden;">
    <div class="account-header account-form">
        <h2><?= __('Don\'t have an account?') ?> <a class="signup-btn" style="background: inherit;font-weight:bold"><?= __('Sign up') ?></a></h2>
        <a class="close-btn" href="#">
            <?= $this->Html->image('mobile/cancel.svg') ?>
        </a>
    </div>
    <div style="height: 100%;overflow: hidden auto;padding-bottom: 85px;">

        <h2 style="font-weight: bolder;"><?= __('Login') ?></h2>

        <div class="buttons-container have-border-bottom">
            <?= $this->Form->postLink(
                $this->Html->image('mobile/google.svg') . __('Continue with Google'),
                ['controller' => 'authenticated', '?' => ['provider' => 'Google'], '_ssl' => true],
                ['escape' => false, 'class' => 'have-border white-bg']
            ) ?>

            <!-- <?= $this->Form->postLink(
                $this->Html->image('mobile/facebook.svg') . __('Continue with Facebook'),
                ['controller' => 'authenticated', '?' => ['provider' => 'Facebook'], '_ssl' => true],
                ['escape' => false, 'class' => 'facebook-btn']
            ) ?>

            <?= $this->Form->postLink(
                $this->Html->image('mobile/twitter.svg') . __('Continue with Twitter'),
                ['controller' => 'authenticated', '?' => ['provider' => 'Twitter'], '_ssl' => true],
                ['escape' => false, 'class' => 'twitter-btn']
            ) ?> -->

            <?php if (!$isAndroid) { ?>
                <!-- <?= $this->Form->postLink(
                    $this->Html->image('mobile/apple.svg') . __('
                Continue with Apple'),
                    ['controller' => 'authenticated', '?' => ['provider' => 'Apple'], '_ssl' => true],
                    ['escape' => false, 'class' => 'apple-btn']
                ) ?> -->
            <?php } ?>
        </div>

        <?= $this->Form->create(null, ['url' => '/login', 'class' => 'form-container', 'id' => 'form-login']) ?>
        <div class="single-field">
            <?= $this->Form->control('email', ['placeholder' => __('Email'), 'label' => false, 'required' => true, 'id' => 'email-form-login']) ?>
        </div>

        <div class="single-field">
            <?= $this->Form->control('password', ['placeholder' => __('Password'), 'label' => false, 'required' => true, 'id' => 'password-form-login', 'autocomplete' => 'current-password']) ?>
        </div>

        <div class="single-field have-grid">
            <label class="checkbox">
                <?= $this->Form->control('remember_me', ['type' => 'checkbox', 'label' => false, 'hiddenField' => false]) ?> <?= __('Remember me') ?>
            </label>

            <span class="show-password" current="show" lang="<?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'hb' : 'en') ?>">
                <?= $this->Html->image('mobile/visible.png', ['style' => 'height:12px']) ?> <?= __('Show Password') ?></span>
        </div>
        <div class="single-field text-center my-2">
            <div class="g-recaptcha" data-sitekey="6LcJ-jccAAAAAKLmfa65fV74PDwTBP6lTGZsfGsO"></div>
        </div>

        <div class="buttons-container">
            <button type="button">
                <?= ('Login') ?>
            </button>
        </div>
        <?= $this->Form->end() ?>

        <p class="forget-password">
            <a class="forget-btn"><?= __('Forgot Password?') ?></a>
        </p>
    </div>
</div>

<div class="account-popup signup-popup popup-box hide-popup-box" style="overflow: hidden;">
    <div class="account-header account-form">
        <h2><?= __('Already a member?') ?> <a class="login-btn" style="font-weight: bolder;"><?= __('Login') ?></a></h2>
        <a class="close-btn" href="#">
            <?= $this->Html->image('mobile/cancel.svg') ?>
        </a>
    </div>
    <div style="height: 100%;overflow: hidden auto;padding-bottom: 85px;">

        <h2 style="font-weight: bolder;"><?= __('Sign Up') ?></h2>
        <div style="text-align: center;margin-bottom:15px">
            <?= $this->Html->image((isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? '/img/mobile/logo_herbew.png' : '/img/mobile/logo.svg'), ['url' => '/', 'style' => 'width:100px']) ?>
        </div>
        <div class="buttons-container have-border-bottom">
            <?= $this->Form->postLink(
                $this->Html->image('mobile/google.svg') . __('Continue with Google'),
                ['controller' => 'authenticated', '?' => ['provider' => 'Google'], '_ssl' => true],
                ['escape' => false, 'class' => 'have-border white-bg']
            ) ?>

            <!-- <?= $this->Form->postLink(
                $this->Html->image('mobile/facebook.svg') . __('Continue with Facebook'),
                ['controller' => 'authenticated', '?' => ['provider' => 'Facebook'], '_ssl' => true],
                ['escape' => false, 'class' => 'facebook-btn']
            ) ?>

            <?= $this->Form->postLink(
                $this->Html->image('mobile/twitter.svg') . __('Continue with Twitter'),
                ['controller' => 'authenticated', '?' => ['provider' => 'Twitter'], '_ssl' => true],
                ['escape' => false, 'class' => 'twitter-btn']
            ) ?> -->

            <?php if (!$isAndroid) { ?>
                <!-- <?= $this->Form->postLink(
                    $this->Html->image('mobile/apple.svg') . __('
                Continue with Apple'),
                    ['controller' => 'authenticated', '?' => ['provider' => 'Apple'], '_ssl' => true],
                    ['escape' => false, 'class' => 'apple-btn']
                ) ?> -->
            <?php } ?>
        </div>

        <div class="form-container">
            <div class="buttons-container email-signup-container">
                <button type="button" id="contine-with-email" class="dark-color">
                    <?= __('Continue with Email') ?>
                </button>
            </div>
            <div style="display: none;" id="form-signup-container">
                <?= $this->Form->create(null, ['url' => 'signup', 'class' => 'form-container', 'id' => 'form-signup']) ?>
                <div id="singup-details-form">
                    <div class="single-field">
                        <?= $this->Form->control('full_name', ['placeholder' => __('Full Name'), 'label' => false, 'required' => true]) ?>
                    </div>

                    <div class="single-field">
                        <?= $this->Form->control('email', ['placeholder' => __('Email'), 'label' => false, 'required' => true, 'id' => 'email-form-singup']) ?>
                    </div>

                    <div class="single-field">
                        <?= $this->Form->control('password', ['placeholder' => __('Password'), 'label' => false, 'required' => true, 'id' => 'password-form-signup', 'autocomplete' => 'current-password']) ?>
                    </div>
                    <div class="single-field have-grid">
                        <span class="show-password" current="show" lang="<?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'hb' : 'en') ?>">
                            <?= $this->Html->image('mobile/visible.png', ['style' => 'height:12px']) ?> <?= __('Show Password') ?></span>
                    </div>

                    <div class="single-field text-center my-2">
                        <div class="g-recaptcha" data-sitekey="6LcJ-jccAAAAAKLmfa65fV74PDwTBP6lTGZsfGsO"></div>
                    </div>

                    <div class="buttons-container">
                        <button type="button" id="first-email-form-submit">
                            <?= __('Sign Up') ?>
                        </button>
                    </div>
                </div>
                <div id="verify-email-model" style="display: none;">
                    <h5>Verify Email</h5>
                    <p style="text-align: left;">Code is sent to <span id="sent-otp-number-singup"></span></p>
                    <div class="code_div my-2" style="text-align: center; margin:20px 0;">
                        <input type="tel" name="otp[]" class="form-control" required style="padding: 0 15px;width: 44px !important;height: 44px !important;text-align:center" maxlength="1">
                        <input type="tel" name="otp[]" class="form-control" required style="padding: 0 15px;width: 44px !important;height: 44px !important;text-align:center" maxlength="1">
                        <input type="tel" name="otp[]" class="form-control" required style="padding: 0 15px;width: 44px !important;height: 44px !important;text-align:center" maxlength="1">
                        <input type="tel" name="otp[]" class="form-control" required style="padding: 0 15px;width: 44px !important;height: 44px !important;text-align:center" maxlength="1">
                        <input type="tel" name="otp[]" class="form-control" required style="padding: 0 15px;width: 44px !important;height: 44px !important;text-align:center" maxlength="1">
                        <input type="tel" name="otp[]" class="form-control" required style="padding: 0 15px;width: 44px !important;height: 44px !important;text-align:center" maxlength="1">
                    </div>
                    <div class="buttons-container">
                        <button type="submit" style=" background-color: #0872bb;">
                            <?= __('Verify') ?>
                        </button>
                        <button type="button" style=" background-color: #0872bb;" onclick="sendOtpEmail($('#form-signup input[name=\'email\']').val())">
                            Request a new code
                        </button>
                    </div>
                </div>
                <?= $this->Form->end() ?>
                <script src="https://www.jqueryscript.net/demo/Pincode-Plugin-Auto-Tab-jQuery/dist/jquery-pincode-autotab.js"></script>
                <script>
                    $(".code_div input").jqueryPincodeAutotab();
                </script>
            </div>
        </div>
    </div>
</div>
<div class="account-popup forget-content popup-box forget-password hide-popup-box" style="overflow: hidden;">
    <div class="account-header padding-0" style="border-bottom: inherit;">
        <a class="close-btn close-forget" href="#" style="margin-top: 30px;">
            <?= $this->Html->image('mobile/cancel.svg') ?>
        </a>
    </div>
    <div style="height: 100%;overflow: hidden auto;padding-bottom: 85px;">
        <h2><?= __('Forgot password') ?></h2>

        <p class="mini-title">
            <?= __('Please enter a valid e-mail address. We will send you further instructions to change your Password.') ?>
        </p>

        <?= $this->Form->create(null, ['url' => 'forgot-password', 'class' => 'form-container']) ?>
        <div class="single-field">
            <?= $this->Form->control('email', ['placeholder' => __('Email'), 'label' => false, 'required' => true, 'id' => 'email-form-forgot-password']) ?>
        </div>

        <div class="buttons-container">
            <button class="dark-bg">
                <?= __('Send reset link') ?>
            </button>
        </div>
        <?= $this->Form->end() ?>

        <p class="back-to-login">
            <a class="back-to-login-button"><?= __('Back to login') ?></a>
        </p>
    </div>
</div>