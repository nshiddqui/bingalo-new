<div class="account-popup signup-popup popup-box" style="overflow: hidden;">
    <div class="account-header account-form">
        <h2 style="font-weight: bolder;font-size:19px">Phone Verification Needed</h2>
    </div>
    <div style="height: 100%;overflow: hidden auto;padding-bottom: 85px;">
        <p style="margin-bottom:10px">Your Bingalo Account Registration will not completed until Phone Verification is set up.</p>
        <div id="form-signup-container">
            <?= $this->Form->create(null, ['url' => '/socialVerification', 'id' => 'social-login-signup']) ?>
            <div id="send-otp-singup-first">
                <div style="text-align: center;">
                    <?= $this->Form->hidden('phone', ['id' => 'signup-phone-form-input']) ?>
                    <?= $this->Form->control('phone_1', ['placeholder' => 'Phone Number', 'type' => 'tel', 'id' => 'form-mobile-input-signup', 'label' => false, 'style' => 'width:80vw', 'readonly ' => true]) ?>
                </div>
                <div class="buttons-container" style="margin-top: 10px;">
                    <button type="button" id="first-phone-form-submit">
                        <?= __('Continue') ?>
                    </button>
                </div>
                <script>
                    $(document).ready(function() {
                        $('#form-mobile-input-signup').prop('readonly', false);
                    })
                    const phoneInputSignupField = document.querySelector("#form-mobile-input-signup");
                    const phoneInputSignup = window.intlTelInput(phoneInputSignupField, {
                        initialCountry: "il",
                        onlyCountries: ["il", "in", "us"],
                        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
                    });
                </script>
            </div>
            <div id="send-otp-singup-secound" style="display: none;">
                <p>Code is sent to <span id="sent-otp-number-singup"></span></p>
                <div class="my-2" style="text-align: center; margin:20px 0;">
                    <input type="tel" name="otp" class="form-control" required style="padding: 0 15px;width: 150px !important;height: 44px !important;text-align:center" maxlength="1">
                </div>
                <div class="buttons-container">
                    <button type="submit" style=" background-color: #0872bb;">
                        <?= __('Verify') ?>
                    </button>
                    <button type="button" style=" background-color: #0872bb;" onclick="$('#send-otp-singup-secound').hide();$('#send-otp-singup-first').show();">
                        Request a new code
                    </button>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script>
    function sendOtpSingup() {
        var mobile_number = phoneInputSignup.getNumber();
        if (mobile_number == "") {
            if ($('.iti__flag.iti__in').length > 0) {
                mobile_number = "+91" + $('#form-mobile-input-signup').val();
            } else if ($('.iti__flag.iti__us').length > 0) {
                mobile_number = "+1" + $('#form-mobile-input-signup').val();
            } else {
                mobile_number = "+972" + $('#form-mobile-input-signup').val();
            }
        }
        $.ajax({
            url: '<?= $this->Url->build(['controller' => 'Authorized', 'action' => 'sendEmailOTP']) ?>',
            data: {
                phone: mobile_number
            },
            type: 'GET',
            success: function() {
                $('#signup-phone-form-input').val(mobile_number)
                $('#sent-otp-number-singup').html(mobile_number);
                $('#send-otp-singup-first').hide();
                $('#send-otp-singup-secound').show();
            }
        });
    }
</script>