<?php
$this->Form->setTemplates([
    'inputContainer' => '{{content}}',
]);
?>
<html>

<head>
    <title>Bingalo</title>
    <meta name="Bingalo: Buy & Sell near you" content="app-id=6443841412, app-argument=<?= $isAndroid ? $app_info_data->playstore : $app_info_data->appstore ?>">
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>

    <meta property="og:title" content="<?= $this->fetch('og-title', 'Bingalo') ?>" />
    <meta property="og:image" content="<?= $this->fetch('og-image', 'https://bingalo.com/favicon.ico') ?>" />

    <?= $this->Html->css('mobile/w3') ?>

    <?= $this->Html->css('mobile/style') ?>
    <!-- FONTS -->
    <?= $this->Html->css('https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap') ?>
    <?= $this->Html->css('mobile/slick') ?>
    <?= $this->Html->css('mobile/slick-theme') ?>

    <?= $this->Html->css('cookieconsent') ?>

    <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css') ?>

    <?= $this->Html->css('bootstrap-msg-0.4.0.min') ?>

    <?php // $this->Html->css('mobile/preloader') 
    ?>


    
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-WP59ZGZSQ6"></script>
    

    <?= $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js') ?>
    <?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>

    <?= $this->Html->script('https://www.google.com/recaptcha/api.js') ?>

    <?= $this->Html->script('https://maps.googleapis.com/maps/api/js?libraries=places&key=' . GOOGL_API_KEY) ?>

    <?= $this->Html->script('bootstrap-msg-0.4.0.min') ?>

    <?= $this->Html->css('mobile/custom') ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <?= $this->fetch('css') ?>
    <meta name="facebook-domain-verification" content="1y01bit9xp3m27zb3azys1gzi7h×38" />
    
    <script>
        var pageContent = 0;
        const lang = '<?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'hb' : 'en') ?>';
        const IP_ADDRESS = '<?= $ipAddress ?>';
        const exists_language = <?= json_encode($exists_language) ?>;
    </script>
</head>

<body>
    <?= $this->Html->script('cookieconsent') ?>
    <?= $this->Html->script('cookieconsent-init') ?>
    <?= $this->Html->css('subscribe-better') ?>
    <?= $this->Html->script('subscribe-better') ?>
    <!-- Preloader Start -->
    <div class="loader-body" id="loader">
        <div class="loader"></div>
    </div>
    <!-- Preloader Start -->
    <div class="main-container" style="margin-top: 77px;">
        <div class="site-notification" style="position: fixed; top: 0px; z-index: 99;background: white;left:0;right:0;">
            <div class="app-install">
                <span class="close-btn">
                    <?= $this->Html->image('mobile/Group 88.svg') ?>
                </span>
                <div class="bingalo">
                    <div class="image">
                        <?= $this->Html->image('mobile/Image 8.svg') ?>
                    </div>
                    <div class="star">
                        <span><?= __('Bingalo for ' . ($isAndroid ? 'Android' : 'iOS')) ?></span>
                        <div>
                            <ul>
                                <li><a href="javascript:void(0)"> <?= $this->Html->image('mobile/star (2).svg') ?></a></li>
                                <li><a href="javascript:void(0)"> <?= $this->Html->image('mobile/star (2).svg') ?></a></li>
                                <li><a href="javascript:void(0)"> <?= $this->Html->image('mobile/star (2).svg') ?></a></li>
                                <li><a href="javascript:void(0)"> <?= $this->Html->image('mobile/star (2).svg') ?></a></li>
                                <li><a href="javascript:void(0)"> <?= $this->Html->image('mobile/star (2).svg') ?></a></li>
                            </ul>
                            <span>17k</span>
                        </div>
                    </div>
                </div>
                <div class="button">
                    <button onclick="window.location.href='<?= $isAndroid ? $app_info_data->playstore : $app_info_data->appstore ?>'"><?= __('INSTALL') ?></button>
                </div>
            </div>

        </div>
        <!-- HEADER START -->
        <header>
            <div class="menu">
                <div class="mobile-menu-button">
                    <?= $this->Html->image('mobile/bars.svg', ['alt' => 'menu button']) ?>
                </div>
                <div class="menu-content hide-menu">
                    <h2>
                        <?= __('Categories') ?>
                        <a class="reset-cat-btn" style="display:none;top: 50%; right: 25px; position: absolute; transform: translate(0, -50%);color: #0872BB;font-weight: 500;font-size:16px;" href="#">
                            <?= __('Reset') ?>
                        </a>
                        <a class="close-btn" href="#">
                            <?= $this->Html->image('mobile/cancel.svg') ?>
                        </a>
                        <input type="hidden" id="cat-ids">
                    </h2>

                    <ul>
                        <?php foreach ($categories as $category) { ?>
                            <li class="category-item" id="cat-<?= $category->id ?>">
                                <a href="javascript:loadSubCat(<?= $category->id ?>)">
                                    <?= $this->Html->image($category->category_image, ['style' => 'height:20px']) ?><span><?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? $category->herbew_category_name : $category->category_name) ?></span>
                                    <?= $this->Html->image('mobile/arrow-right.svg', ['class' => 'cat-img']) ?>
                                </a>
                                <?php if (count($category['sub_categories'])) { ?>
                                    <ul class="sub-cat" id="sub-cat-<?= $category->id ?>">
                                        <?php foreach ($category['sub_categories'] as $sub_categories) { ?>
                                            <li id="sub-cat-item-<?= $sub_categories->id ?>">
                                                <a href="javascript:loadCat(<?= $category->id ?>,<?= $sub_categories->id ?>,'<?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? $sub_categories->herbew_name : $sub_categories->name) ?>')">
                                                    <span><?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? $sub_categories->herbew_name : $sub_categories->name) ?></span>
                                                    <?= $this->Html->image('checked (3).svg', ['style' => 'display: none;']) ?>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>

                    <div class="button">
                        <button onclick="$('.menu-content').addClass('hide-menu');loadProduct(true)"><?= __('Apply') ?></button>
                    </div>

                    <div class="button">
                        <?php if ($isAndroid) { ?>
                            <button class="available-on-mobile"><?= __('Sell Your Stuff') ?></button>
                        <?php } else { ?>
                            <button class="available-on-mobile"><?= __('Sell Your Stuff') ?></button>
                        <?php } ?>
                    </div>

                    <ul class="have-border">
                        <li>
                            <a href="https://page.bingalo.com/about-us/">
                                <?= $this->Html->image('mobile/information.svg') ?> <span><?= __('About Us') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="mailto:support@bingalo.com">
                                <?= $this->Html->image('mobile/megaphone.svg') ?> <span><?= __('Advertise') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="https://page.bingalo.com/contact-us/">
                                <?= $this->Html->image('mobile/icn_contact_us.png') ?> <span><?= __('Contact') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="https://page.bingalo.com/faq-1/">
                                <?= $this->Html->image('mobile/question%20%281%29.svg') ?> <span><?= __('FAQ') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="https://page.bingalo.com/safety-tips/">
                                <?= $this->Html->image('mobile/icn_safety_tips.png') ?> <span><?= __('Safety Tips') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="https://page.bingalo.com/prohibited-items/">
                                <?= $this->Html->image('mobile/icn_pohibited_items.png') ?> <span><?= __('Prohibited Items') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="https://page.bingalo.com/terms-of-service/">
                                <?= $this->Html->image('mobile/icn_terms_of_service.png') ?> <span><?= __('Terms of Service') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="https://page.bingalo.com/privacy-policy/">
                                <?= $this->Html->image('mobile/icn_privacy_policy.png') ?> <span><?= __('Privacy Policy') ?></span>
                            </a>
                        </li>


                    </ul>
                    <?php if ($authUser === false) { ?>
                        <ul class="have-border account-form">
                            <li>
                                <a class="signup-btn" style="background: inherit;">
                                    <?= $this->Html->image('mobile/add-user.svg') ?>
                                    <span><?= __('Sign up') ?></span>
                                </a>
                            </li>

                            <li>
                                <a class="login-btn">
                                    <?= $this->Html->image('mobile/enter.svg') ?> <span><?= __('Log in') ?></span>
                                </a>
                            </li>
                        </ul>
                    <?php } else { ?>
                        <ul class="have-border account-form">
                            <li>
                                <a href="/logout" class="login-btn" onclick="return confirm('<?= __('Are you sure to logout?') ?>')">
                                    <?= $this->Html->image('mobile/enter.svg') ?> <span><?= __('Logout') ?></span>
                                </a>
                            </li>
                        </ul>
                    <?php } ?>

                    <div class="app-link">
                        <h2><?= __('Download the Bingalo app') ?>.</h2>

                        <?php if ($isAndroid) { ?>
                            <?= $this->Html->image('mobile/download-on-play-store-png.png', ['style' => 'height:50px', 'url' => $app_info_data->playstore]) ?>
                        <?php } else { ?>
                            <?= $this->Html->image('mobile/download-on-app-store-png.png', ['style' => 'height:50px', 'url' => $app_info_data->appstore]) ?>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <h1 class="logo">
                <a href="/" style="background-image: url(<?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? '/img/mobile/logo_herbew.png' : '/img/logo.png') ?>);"><?= __('Bingalo') ?></a>
            </h1>

            <?php if ($authUser) { ?>
                <div class="right-section account-form " style="width: auto;grid-template-columns: 0fr 0fr 0fr;">
                    <span><?= $this->Html->image('mobile/message.png', ['style' => 'height: 20px; width: 20px; margin-top: 4px;']) ?></span>
                    <span><a href="/profile" style="background: #fff;padding:0"><?= $this->Html->image(!empty($authUser['image']) ? str_replace('http://', 'https://', $authUser['image']) : 'mobile/user_placeholder.png', ['style' => 'height: 26px; width: 26px;border-radius:100%;object-fit:cover']) ?></a></span>
                    <!-- <div style="position: relative;display: inline-block;">
                        <span class="more-btn open-dropdown" style="display: flow-root;" target="#user-menu-option"><?= $this->Html->image(!empty($authUser['image']) ? str_replace('http://', 'https://', $authUser['image']) : 'mobile/user_placeholder.png', ['style' => 'height: 26px; width: 26px;border-radius:100%;object-fit:cover']) ?></span>
                        <div id="user-menu-option" style="position: absolute;min-width: 150px;padding: 10px;z-index: 1;right: 0;top: 20px;display:none;">
                            <?= $this->Html->link(__('View Profile'), '/profile', ['class' => 'more-btn', 'style' => 'margin:2px']) ?>
                            <?= $this->Html->link(__('Logout'), '/logout', ['class' => 'more-btn', 'style' => 'margin:2px']) ?>
                        </div>
                    </div> -->
                    <div style="position: relative;display: inline-block;font-size:12px">
                        <span class="more-btn open-dropdown" style="display: flow-root;line-height:25px" target="#translate-menu-option"><?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'עב׳' : 'En') ?></span>
                        <div id="translate-menu-option" style="position: absolute;min-width: 150px;padding: 10px;z-index: 1;right: 0;top: 20px;display:none;">
                            <?= $this->Html->link((isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'En' : 'עב׳'), 'javascript:translateLanguage(\'' . (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'en' : 'en_HB') . '\');', ['class' => 'more-btn', 'style' => 'margin:2px']) ?>
                        </div>
                    </div>
                </div>

            <?php } else { ?>
                <div class="right-section account-form ">
                    <a class="login-btn"><?= __('Login') ?></a>
                    <a class="signup-btn"><?= __('Signup') ?></a>
                    <div style="position: relative;display: inline-block;font-size:12px">
                        <span class="more-btn open-dropdown" style="display: flow-root;line-height:25px" target="#translate-menu-option"><?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'עב׳' : 'En') ?></span>
                        <div id="translate-menu-option" style="position: absolute;min-width: 150px;padding: 10px;z-index: 1;right: 0;top: 20px;display:none;">
                            <?= $this->Html->link((isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'En' : 'עב׳'), 'javascript:translateLanguage(\'' . (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'en' : 'en_HB') . '\');', ['class' => 'more-btn', 'style' => 'margin:2px']) ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
    </div>
    </header>
    <!-- HEADER END -->
    <?php if ($authUser === false) { ?>
        <?= $this->element('login_elements') ?>
    <?php  } else if (empty($authUser['phone'])) { ?>
        <?= $this->element('login_phone_elements') ?>
    <?php } ?>
    <div class="filter-section">
        <?php if ($this->Url->build() == '/') { ?>
            <div class="search-container">
                <?= $this->Html->image('search.svg', ['src' => '/images/search.svg', 'alt' => 'search icon', 'class' => 'search-icon']) ?>
                <input id="searh-bar-input" onclick="$('#search-icon').show();" onsearch="loadProduct(true);$('#searh-bar-input').blur();" value="" type="search" placeholder="<?= __('Search for items to buy or sell...') ?>">
                <?= $this->Html->image('mobile/search_arrow.png', ['id' => 'search-icon', 'style' => 'position: absolute; height: 12.73px; top: 50%; transform: translate(0, -50%); right: 5px;display:none;', 'onclick' => "loadProduct(true);$('#searh-bar-input').blur();$(this).hide();"]) ?>
            </div>

            <div class="filter-container">
                <span>
                    <?= $this->Html->image('filter.svg', ['alt' => 'filter icon']) ?>
                    <?= __('Filter') ?>
                </span>
            </div>
            <div class="tagComponent">
                <ul id="tag-search">
                </ul>
            </div>
        <?php } ?>
    </div>
    <?= $this->fetch('content') ?>
    <div class="filter-content popup-box hide-popup-box" style="overflow: hidden;">
        <div class="popup-header">
            <span class="close-filter">
                <?= $this->Html->image('mobile/arrow.png') ?>
            </span>
            <h4><?= __('Filter') ?></h4>
            <a href="javascript:clearFilter();" class="close-filter"><?= __('Reset') ?></a>
        </div>
        <div style="height: 100%;overflow: hidden auto;padding-bottom: 85px;">

            <div class="filter-box">
                <div class="single-box">
                    <h2><?= __('LOCATION') ?></h2>
                    <div class="box location-box">
                        <p id="current-location" style="max-width: 50vh;"><?= __('Jerusalem, Israel') ?></p>
                        <input type="hidden" id="product-lat">
                        <input type="hidden" id="product-lng">
                        <siv class="arrow-icon">
                            <a href="#"><?= __('Change Location') ?></a>
                            <?= $this->Html->image('mobile/arrow-right.png') ?>
                        </siv>

                    </div>
                </div>

                <div class="single-box">
                    <h2><?= __('PRICE') ?></h2>
                    <div class="box prize">
                        <div class=" input">
                            <span><?= __('MIN') ?> ₪</span>
                            <input type="number" id="min-price" placeholder="000">
                        </div>
                        <div class="input">
                            <span><?= __('MAX') ?> ₪</span>
                            <input type="number" id="max-price" placeholder="1000">
                        </div>
                    </div>
                </div>

                <div class="single-box">
                    <h2><?= __('DISTANCE') ?></h2>
                    <div id="form-wrapper">
                        <?= $this->Html->image('mobile/ico-location.svg', ['class' => 'img-location']) ?>
                        <form action="" method="GET">
                            <div id="debt-amount-slider">
                                <input type="radio" name="debt-amount" data-distance="5" id="1" value="1" required>
                                <label for="1" data-debt-amount="5"></label>
                                <input type="radio" name="debt-amount" data-distance="10" id="2" value="2" required>
                                <label for="2" data-debt-amount="10"></label>
                                <input type="radio" name="debt-amount" data-distance="30" id="3" value="3" required>
                                <label for="3" data-debt-amount="30"></label>
                                <input type="radio" name="debt-amount" data-distance="50" id="4" value="4" required>
                                <label for="4" data-debt-amount="50"></label>
                                <input type="radio" name="debt-amount" data-distance="100" id="5" value="5" required>
                                <label for="5" data-debt-amount="100"></label>
                                <div id="debt-amount-pos"></div>
                            </div>
                        </form>
                        <?= $this->Html->image('mobile/ico-car.svg', ['class' => 'img-car']) ?>
                    </div>
                </div>

                <div class="single-box">
                    <h2><?= __('SORT BY') ?></h2>
                    <div class="box sort">
                        <div class="menu">
                            <ul>
                                <li class="filter-sort" data-sort="1" data-order="distance">
                                    <a href="javascript:void(0)"><?= __('Closest') ?></a>
                                    <?= $this->Html->image('mobile/check (1).svg') ?>
                                </li>
                                <li class="filter-sort" data-sort="2" data-order="Product.created_at DESC">
                                    <a href="javascript:void(0)"><?= __('Newest') ?></a>
                                    <?= $this->Html->image('mobile/check (1).svg') ?>
                                </li>
                                <li class="filter-sort" data-sort="3" data-order="Product.product_price DESC">
                                    <a href="javascript:void(0)"><?= __('Price: High to Low') ?></a>
                                    <?= $this->Html->image('mobile/check (1).svg') ?>
                                </li>
                                <li class="filter-sort" data-sort="4" data-order="Product.product_price ASC">
                                    <a href="javascript:void(0)"><?= __('Price: Low to High') ?></a>
                                    <?= $this->Html->image('mobile/check (1).svg') ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button">
                <button onclick="applyFilter()" class="close-filter"><?= __('Apply Filter') ?></button>
            </div>
        </div>
    </div>

    <div class="location-content popup-box hide-popup-box" style="overflow: hidden;">
        <div class="popup-header">
            <span class="close-location">
                <?= $this->Html->image('mobile/arrow.png') ?>
            </span>

            <h4><?= __("Location") ?></h4>

            <span class="close-location">
                <?= $this->Html->image('mobile/Group 116.png') ?>
            </span>
        </div>
        <div style="height: 100%;overflow: hidden auto;padding-bottom: 85px;">
            <div class="zip">
                <span><?= __('Search') ?></span>
            </div>

            <div class="input filter-section">
                <div class="search-container">
                    <input type="text" value="" id="autocomplete-filter-address" placeholder="<?= __('Search') ?>">
                    <?= $this->Html->image('mobile/search.svg', ['class' => 'search']) ?>
                </div>
                <div class="search-result">
                    <p>SA, Itly</p>
                    <p>SA lika, London</p>
                </div>

            </div>
            <div class="span">
                <span><?= __('OR') ?></span>
            </div>
            <div class="button">
                <button onclick="getCurrentLocation()">
                    <?= $this->Html->image('mobile/Path 320.png') ?>
                    <?= __('Get my location') ?>
                </button>
            </div>
        </div>
    </div>
    <?php if ($this->fetch('footer') != 'no-footer') { ?>
        <!-- FOOTER START -->
        <footer>
            <div class="footer-upper">
                <div class="logo">
                    <?= $this->Html->image((isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? '/img/mobile/logo_herbew.png' : '/img/mobile/logo.svg'), ['url' => '/', 'style' => 'width:45px']) ?>
                </div>

                <ul>
                    <li><?= $this->Html->image('mobile/f-facebook.svg', ['url' => $app_info_data->facebook]) ?></li>
                    <li><?= $this->Html->image('mobile/f-twitter.svg', ['url' => $app_info_data->twitter]) ?></li>
                    <li><?= $this->Html->image('mobile/f-instagram.svg', ['url' => $app_info_data->instagram]) ?></li>
                    <li><?= $this->Html->image('youtube.png', ['url' => $app_info_data->youtube]) ?></li>
                    <li><?= $this->Html->image('tiktok.png', ['url' => $app_info_data->tiktok]) ?></li>
                </ul>

                <ul class="footer-menu">
                    <li><?= $this->Html->link(__('About us'), 'https://page.bingalo.com/about-us/') ?></li>
                    <li><?= $this->Html->link(__('Contact'), 'https://page.bingalo.com/contact-us/') ?></li>
                    <li><?= $this->Html->link(__('FAQ'), 'https://page.bingalo.com/faq-1/') ?></li>
                    <li><?= $this->Html->link(__('Advertise'), 'https://page.bingalo.com/') ?></li>
                    <li><?= $this->Html->link(__('Safety Tips'), 'https://page.bingalo.com/safety-tips/') ?></li>

                    <li><?= $this->Html->link(__('Prohibited Items'), 'https://page.bingalo.com/prohibited-items/') ?></li>
                    <li><?= $this->Html->link(__('Sitemap'), '/sitemap') ?></li>
                </ul>
                <div class="footer-btn">
                    <select onchange="translateLanguage(this.value);">
                        <div>
                            <option id="english-lang" value="en">
                                English
                            </option>
                        </div>

                        <option id="hebrew-lang" value="en_HB" <?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB') ? 'selected' : '' ?>>
                            Hebrew
                        </option>
                    </select>
                </div>
            </div>

            <div class="footer-bottom">
                <p style="font-weight: bolder;">
                    <?= __('Bingalo: Buy and Sell almost any secondhand products in Israel quickly, safely and locally on the free Bingalo app. Start selling and make money. Say Hello to Bingalo; Israel\'s Largest Mobile Marketplace.') ?>
                </p>
                <span>
                    © 2022. <?= __('All right reserved') ?>. <?= __('Bingalo') ?> <?= $this->Html->link(__('Terms & Conditions'), 'https://page.bingalo.com/terms-of-service/') ?> <?= __('and') ?> <?= $this->Html->link(__('Privacy Policy'), 'https://page.bingalo.com/privacy-policy/') ?>
                </span>
            </div>
        </footer>
        <!-- FOOTER END -->
    <?php } ?>
    </div>
    <!-- The Modal -->
    <div id="available-on-app" class="available-modal" style="z-index: 999999999;">

        <!-- Modal content -->
        <div class="available-modal-content">
            <span class="available-close"><?= $this->Html->image('mobile/Group 116.png', ['style' => 'height:25px']) ?></span>
            <p style="font-weight: bolder;font-size: 12px;margin-right: 25px;"><?= __('Use this Features in the Bingalo App') ?> </p>
            <p style="font-size: 11px;color:#c1c1c1;margin-right: 25px;"><?= __('To use this features on Bingalo, please download the app') ?></p>
            <div style="margin-right:25px;text-align:center;padding:5px;">
                <?= $this->Html->image('mobile/img_appstore.png', ['url' => $app_info_data->appstore, 'style' => 'height: 40px; display: block; margin: auto;margin-top:4px;']) ?>
                <?= $this->Html->image('mobile/img_playstore.png', ['url' => $app_info_data->playstore, 'style' => 'height: 40px; display: block; margin: auto;margin-top:4px;']) ?>
            </div>
        </div>

    </div>
    <?= $this->Html->script('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js') ?>
    <?= $this->html->script('mobile/custom') ?>
    <?= $this->fetch('script') ?>
    <script>
        <?php
        echo "var pageContent = 0;";
        echo "var BASE_URL_IMAGE = '" . $this->Url->build('/img/') . "';";
        echo "var VERIFY_OTP_URL = '" . $this->Url->build('/api/v1/users/verify-otp/') . "';";
        echo "var FAVOURITE_URL = '" . $this->Url->build('/api/v1/authorized/favourite/') . "';";
        echo '
if (localStorage.getItem("distance") && localStorage.getItem("distance") != \'\') {
    $(\'[data-distance="\' + localStorage.getItem("distance") + \'"]\').prop(\'checked\', true);
}
if (localStorage.getItem("min_price") && localStorage.getItem("min_price") != \'\') {
    $(\'#min-price\').val(localStorage.getItem("min_price"));
}
if (localStorage.getItem("max_price") && localStorage.getItem("max_price") != \'\') {
    $(\'#max-price\').val(localStorage.getItem("max_price"));
}
if (localStorage.getItem("order") && localStorage.getItem("order") != \'\') {
    $(\'.filter-sort a:contains(\' + localStorage.getItem("order") + \')\').parent(\'.filter-sort\').addClass(\'grid\');
}
if (localStorage.getItem("location") && localStorage.getItem("location") != \'\') {
    $(\'#current-location\').html(localStorage.getItem("location"));
    $(\'#product-lat\').val(localStorage.getItem("product_lat"));
    $(\'#product-lng\').val(localStorage.getItem("product_lng"));
} else {
    var cokie = getCookie(\'localInformation\');
    if (cokie !== \'\') {
        var cokie = JSON.parse(getCookie(\'localInformation\'));
        $(\'#current-location\').html(cokie.city + " " + cokie.zip);
        $(\'#product-lat\').val(cokie.latitude);
        $(\'#product-lng\').val(cokie.longitude);
    }
}
$(document).ready(function() {
    applyFilter();
    if (localStorage.getItem("cat_ids") && localStorage.getItem("cat_ids") != \'\') {
        var cat_ids  = [...new Set(trimChar(localStorage.getItem("cat_ids"),\',\').split(\',\'))];
        $("#cat-ids").val(cat_ids);
        cat_ids.forEach(function(subCatId){
            var catId = $("#sub-cat-item-"+subCatId).parent(".sub-cat").attr("id").split("-")[2];
            var Text = $("#sub-cat-item-"+subCatId).text().trim();
            var img = $(\'#sub-cat-item-\' + subCatId + \' img\');
            console.log(catId,Text,subCatId);
            console.log(img.is(\':hidden\'));
            if (img.is(\':hidden\')) {
                img.show();
                $(\'#tag-search\').append(`
                    <li id="sub-cat-${subCatId}">
                        ${Text}
                        <img src="${BASE_URL_IMAGE}mobile/cancel.svg" onclick="closeLoadCat(${catId}, ${subCatId})" alt="x">
                    </li>
                `);
            } else {
                img.hide();
                $(\'#sub-cat-\' + subCatId).remove();
            }
            var need_to_show = false;
            $(\'#cat-\' + catId + \' .sub-cat li\').each(function() {
                if ($(this).find(\'img\').is(\':hidden\') === false) {
                    need_to_show = true;
                }
            });
            if (need_to_show) {
                $(\'#cat-\' + catId + \' .cat-img\').attr(\'src\', BASE_URL_IMAGE + \'checked (3).svg\');
            } else {
                $(\'#cat-\' + catId + \' .cat-img\').attr(\'src\', BASE_URL_IMAGE + \'mobile/arrow-right.svg\');
            }
        });
    }
    loadProduct(true);
    });';
        ?>
    </script>
    <?= $this->Flash->render() ?>
    <script>
        // document.body.onload = function() {
        //     setTimeout(function() {
        //         var preloader = document.getElementById('loader');
        //         if (!preloader.classList.contains('done')) {
        //             preloader.classList.add('done');
        //         }
        //     }, 1000)
        // }
    </script>
    <script>
        function getCurrentLocation() {
            if ("geolocation" in navigator) {
                //check geolocation available 

                //try to get user current location using getCurrentPosition() method
                navigator.geolocation.getCurrentPosition(function(position) {
                        if (position.coords.latitude) {
                            $.ajax({
                                url: "https://maps.google.com/maps/api/geocode/json",
                                data: {
                                    'latlng': position.coords.latitude + "," + position.coords.longitude,
                                    'key': 'AIzaSyCehnlNlZBui59vgOGNYN8XoN5rjYE9HJE'
                                },
                                type: 'GET',
                                success: function(res) {
                                    var address = {};
                                    if (res['results']) {
                                        res['results'].forEach(function(value, index) {
                                            value['address_components'].forEach(function(value, index) {
                                                address[value['types'][0]] = {
                                                    'short_name': value['short_name'],
                                                    'long_name': value['long_name']
                                                };
                                            });
                                        });
                                    }
                                    if (address !== {}) {
                                        $('#product-lat').val(position.coords.latitude);
                                        $('#product-lng').val(position.coords.longitude);
                                        $('#current-location').html(address.locality.long_name + ' ' + address.postal_code.long_name);
                                        localStorage.setItem("product_lat", position.coords.latitude);
                                        localStorage.setItem("product_lng", position.coords.longitude);
                                        localStorage.setItem("location", address.locality.long_name + ' ' + address.postal_code.long_name);
                                        $('.location-content').addClass('hide-popup-box');
                                        $('#product-lat').change();
                                    } else {
                                        alert('Unable to fetch your location, please search manually.')
                                    }
                                },
                                error: function() {
                                    $.ajax({
                                        "async": true,
                                        "crossDomain": true,
                                        url: 'https://ip-geolocation-ipwhois-io.p.rapidapi.com/json/?ip=' + IP_ADDRESS,
                                        type: 'GET',
                                        headers: {
                                            "X-RapidAPI-Key": "0975eaa22emsh838ef6943ae2108p1fe97cjsn69034e533326",
                                            "X-RapidAPI-Host": "ip-geolocation-ipwhois-io.p.rapidapi.com"
                                        },
                                        success: function(res) {
                                            $('#product-lat').val(res.latitude);
                                            $('#product-lng').val(res.longitude);
                                            $('#current-location').html(res.city + ' ' + res.country);
                                            localStorage.setItem("product_lat", res.latitude);
                                            localStorage.setItem("product_lng", res.longitude);
                                            localStorage.setItem("location", res.city + ' ' + res.country);
                                            $('.location-content').addClass('hide-popup-box');
                                            $('#product-lat').change();
                                        },
                                        error: function(error) {
                                            $('#product-lat').val(31.7964452);
                                            $('#product-lng').val(35.1051475);
                                            $('#current-location').html('Jerusalem, Israel');
                                            localStorage.setItem("product_lat", 31.7964452);
                                            localStorage.setItem("product_lng", 35.1051475);
                                            localStorage.setItem("location", 'Jerusalem, Israel');
                                            $('.location-content').addClass('hide-popup-box');
                                            $('#product-lat').change();
                                        }
                                    });
                                }
                            });
                        } else {
                            $.ajax({
                                "async": true,
                                "crossDomain": true,
                                url: 'https://ip-geolocation-ipwhois-io.p.rapidapi.com/json/?ip=' + IP_ADDRESS,
                                type: 'GET',
                                headers: {
                                    "X-RapidAPI-Key": "0975eaa22emsh838ef6943ae2108p1fe97cjsn69034e533326",
                                    "X-RapidAPI-Host": "ip-geolocation-ipwhois-io.p.rapidapi.com"
                                },
                                success: function(res) {
                                    $('#product-lat').val(res.latitude);
                                    $('#product-lng').val(res.longitude);
                                    $('#current-location').html(res.city + ' ' + res.country);
                                    localStorage.setItem("product_lat", res.latitude);
                                    localStorage.setItem("product_lng", res.longitude);
                                    localStorage.setItem("location", res.city + ' ' + res.country);
                                    $('.location-content').addClass('hide-popup-box');
                                    $('#product-lat').change();
                                },
                                error: function(error) {
                                    $('#product-lat').val(31.7964452);
                                    $('#product-lng').val(35.1051475);
                                    $('#current-location').html('Jerusalem, Israel');
                                    localStorage.setItem("product_lat", 31.7964452);
                                    localStorage.setItem("product_lng", 35.1051475);
                                    localStorage.setItem("location", 'Jerusalem, Israel');
                                    $('.location-content').addClass('hide-popup-box');
                                    $('#product-lat').change();
                                }
                            });
                        }
                    },
                    function(err) {
                        $.ajax({
                            "async": true,
                            "crossDomain": true,
                            url: 'https://ip-geolocation-ipwhois-io.p.rapidapi.com/json/?ip=' + IP_ADDRESS,
                            type: 'GET',
                            headers: {
                                "X-RapidAPI-Key": "0975eaa22emsh838ef6943ae2108p1fe97cjsn69034e533326",
                                "X-RapidAPI-Host": "ip-geolocation-ipwhois-io.p.rapidapi.com"
                            },
                            success: function(res) {
                                $('#product-lat').val(res.latitude);
                                $('#product-lng').val(res.longitude);
                                $('#current-location').html(res.city + ' ' + res.country);
                                localStorage.setItem("product_lat", res.latitude);
                                localStorage.setItem("product_lng", res.longitude);
                                localStorage.setItem("location", res.city + ' ' + res.country);
                                $('.location-content').addClass('hide-popup-box');
                                $('#product-lat').change();
                            },
                            error: function(error) {
                                $('#product-lat').val(31.7964452);
                                $('#product-lng').val(35.1051475);
                                $('#current-location').html('Jerusalem, Israel');
                                localStorage.setItem("product_lat", 31.7964452);
                                localStorage.setItem("product_lng", 35.1051475);
                                localStorage.setItem("location", 'Jerusalem, Israel');
                                $('.location-content').addClass('hide-popup-box');
                                $('#product-lat').change();
                            }
                        });
                    });
            } else {
                $.ajax({
                    "async": true,
                    "crossDomain": true,
                    url: 'https://ip-geolocation-ipwhois-io.p.rapidapi.com/json/?ip=' + IP_ADDRESS,
                    type: 'GET',
                    headers: {
                        "X-RapidAPI-Key": "0975eaa22emsh838ef6943ae2108p1fe97cjsn69034e533326",
                        "X-RapidAPI-Host": "ip-geolocation-ipwhois-io.p.rapidapi.com"
                    },
                    success: function(res) {
                        $('#product-lat').val(31.7964452);
                        $('#product-lng').val(35.1051475);
                        $('#current-location').html('Jerusalem, Israel');
                        localStorage.setItem("product_lat", 31.7964452);
                        localStorage.setItem("product_lng", 35.1051475);
                        localStorage.setItem("location", 'Jerusalem, Israel');
                        $('.location-content').addClass('hide-popup-box');
                        $('#product-lat').change();
                    },
                    error: function(error) {
                        $('#product-lat').val(31.7964452);
                        $('#product-lng').val(35.1051475);
                        $('#current-location').html('Jerusalem, Israel');
                        localStorage.setItem("product_lat", 31.7964452);
                        localStorage.setItem("product_lng", 35.1051475);
                        localStorage.setItem("location", 'Jerusalem, Israel');
                        $('.location-content').addClass('hide-popup-box');
                        $('#product-lat').change();
                    }
                });
            }
        }
    </script>
    <div class="popup-newsletter gee-popup-newsletter" style="width: 100%;">
        <div class="popup-newsletter-inner">
            <a class="close-button popup-newsletter-close-button">
                &times;
            </a>
            <div class="popup-newsletter-header">
                <h3 class="popup-newsletter-title"></h3>
            </div>
            <p class="paragraph"></p>
            <div class="user-content"></div>
            <button class="btn btn-info btn-block btn-lg opup-newsletter-button"></button>
        </div>
    </div>
    <script>
        $("html").jPopup({
            heading: "Newsletter",
            paragraph: "Sty up to date with our latest news and products",
            buttonText: "Subscribe",
            buttonClass: "btn btn-info",
        });
    </script>
</body>

</html>