<?php

foreach ($results as $result) {
    $icons = [];
    if (!empty($result['user_verification'])) {
        foreach ($result['user_verification'] as $user_verification) {
            $icons[] = $this->Html->image($user_verification->verified_by . '-icon.png', ['style' => 'height:20px;margin: 0 1.8px']);
        }
    }
    $this->DataTables->prepareData([
        $this->Html->link($this->Html->image($result->image, ['height' => '50', 'width' => '50', 'class' => 'img-circle']), $result->image, ['fancybox' => true, 'escape' => false]),
        h($result->full_name),
        h($result->email),
        h($result->phone),
        implode(' ', $icons),
        h(@$result['product'][0]->product_location),
        $this->Html->link('<i class="fa fa-eye"></i>', ['action' => 'view', $result->id], ['escape' => false, 'class' => 'btn btn-primary']) . '&nbsp;' .
        $this->Html->link('<i class="fa fa-envelope"></i>', '#', ['escape' => false, 'class' => 'btn btn-info', 'data-toggle' => 'modal', 'data-target' => '#modal-default', 'onclick' => "$('#email').val('" . $result->id . "')"]) . '&nbsp;' .
        $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->client_name), 'escape' => false, 'class' => 'btn btn-danger'])
    ]);
}
echo $this->DataTables->response();
