<?php
$this->assign('title', 'View User Profile');
$icons = [];
if (!empty($user['user_verification'])) {
    foreach ($user['user_verification'] as $user_verification) {
        $icons[] = $this->Html->image($user_verification->verified_by . '-icon.png', ['style' => 'height:20px;margin: 0 1.8px']);
    }
}
?>
<div class="box-body">
    <div class="row">
        <div class="col-md-3">&nbsp;</div>
        <div class="col-md-6">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active text-center">
                    <?= $this->Html->link($this->Html->image((!empty($user['image']) ? $user['image'] : 'not-found.png'), ['class' => 'img-circle', 'style' => 'height:110px;width: 110px;object-fit: contain;background: white;']), (!empty($user['image']) ? $user['image'] : '/img/not-found.png'), ['fancybox' => true, 'escape' => false]) ?>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        <li><a href='javascript:void(0)'><strong>User ID</strong><span class="pull-right"><?= $user['id'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Full Name</strong><span class="pull-right"><?= $user['full_name'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Email</strong><span class="pull-right"><?= $user['email'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Phone No.</strong><span class="pull-right"><?= $user['phone'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Location</strong><span class="pull-right"><?= @$user['product'][0]['product_location'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Verified By</strong><span class="pull-right"><?= implode(' ', $icons) ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Registration Date & Time</strong><span class="pull-right"><?= $user['created_at'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><?= $this->Html->link('<i class="fa fa-envelope"></i> Send Email', '#', ['escape' => false, 'class' => 'btn bg-aqua-active', 'data-toggle' => 'modal', 'data-target' => '#modal-default']) ?></a></li>
                    </ul>
                </div>
            </div>
            <!-- /.widget-user -->
        </div>
    </div>
</div>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create() ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Mail</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->hidden('email', ['value' => $user['id']]) ?>
                <?= $this->Form->control('subject') ?>
                <?= $this->Form->control('message', ['type' => 'textarea']) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?= $this->Form->submit('Send', ['class' => 'btn btn-primary']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->