<?php
$this->assign('title', 'App Message Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List App Message</h3>
        <?= $this->Html->link('Add App Message', ['action' => 'add'], ['class' => 'pull-right btn btn-primary']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Messages') ?>
    </div>
</div>