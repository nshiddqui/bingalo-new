<?php
$this->assign('title', 'App Message Management');
?>
<?= $this->Form->create($message) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Add App Message') ?></h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <?= $this->Form->control('message') ?>
                </div>
                <div class="col-md-12">
                    <?= $this->Form->control('category_id', ['options' => $categories]) ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Form->button(__('Add')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>