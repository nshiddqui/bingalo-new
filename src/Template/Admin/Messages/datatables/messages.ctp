<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        $result['category']->category_name,
        h($result->message),
        $result->created,
        $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit', $result->id], ['escape' => false, 'class' => 'btn btn-info']) . '&nbsp;' .
        $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->client_name), 'escape' => false, 'class' => 'btn btn-danger'])
    ]);
}
echo $this->DataTables->response();
