<?php
$this->assign('title', 'Top Cities Management');
?>
<?= $this->Form->create($topCity) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Add Top Cities') ?></h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <?= $this->Form->control('name') ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Form->button(__('Add')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>