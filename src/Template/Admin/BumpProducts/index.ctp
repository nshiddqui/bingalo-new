<?php
$this->assign('title', 'Bump Products Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Bump Products</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('BumpProducts') ?>
    </div>
</div>