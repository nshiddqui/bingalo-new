<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        $this->Html->link($this->Html->image($result->category_image, ['height' => '50', 'width' => '50']), $result->category_image, ['fancybox' => true, 'escape' => false]),
        h($result->category_name),
        h($result->herbew_category_name),
        $result->position,
        (!empty($result['sub_categories']) ? $result['sub_categories'][0]['count'] : 0),
        $this->Html->link('<i class="fa fa-eye"></i>', ['action' => 'view', $result->id, '?' => ['category_id' => $result->id]], ['escape' => false, 'class' => 'btn btn-primary']) . '&nbsp;' .
            $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit', $result->id], ['escape' => false, 'class' => 'btn btn-info']) . '&nbsp;' .
            $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->category_name), 'escape' => false, 'class' => 'btn btn-danger'])
    ]);
}
echo $this->DataTables->response();
