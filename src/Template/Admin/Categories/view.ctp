<?php
$this->assign('title', 'Categories Management');
?>
<div class="box">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-primary">
                    <div class="widget-user-image text-center">
                        <?= $this->Html->link($this->Html->image($category->category_image, ['height' => '150', 'width' => '150']), $category->category_image, ['fancybox' => true, 'class' => 'img-circle', 'escape' => false]) ?>
                    </div>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        <li><a href="#">English Category Name <span class="pull-right"><?= $category->category_name ?></span></a></li>
                        <li><a href="#">Herbew Category Name <span class="pull-right"><?= $category->herbew_category_name ?></span></a></li>
                        <li><a href="#">Category Position <span class="pull-right"><?= $category->position ?></span></a></li>
                    </ul>
                </div>
            </div>
            <!-- /.widget-user -->
        </div>
    </div>
</div>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Sub Categories</h3>
        <?= $this->Html->link('Add Sub Categories', ['action' => 'subadd', '?' => ['category_id' => $category->id]], ['class' => 'pull-right btn btn-primary']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('SubCategories') ?>
    </div>
</div>