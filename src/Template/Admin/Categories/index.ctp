<?php
$this->assign('title', 'Categories Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Categories</h3>
        <?= $this->Html->link('Add Categories', ['action' => 'add'], ['class' => 'pull-right btn btn-primary']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Categories') ?>
    </div>
</div>