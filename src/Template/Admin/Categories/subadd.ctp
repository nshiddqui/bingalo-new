<?php
$this->assign('title', 'Sub Categories Management');
?>
<?= $this->Form->create($category) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __((!empty($category->id) ? 'Update' : 'Add') . ' Sub Categories') ?></h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <?= $this->Form->control('name', ['label' => 'English Category Name']) ?>
                </div>
                <div class="col-md-12">
                    <?= $this->Form->control('herbew_name', ['label' => 'Herbew Category Name']) ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Form->button(__((!empty($category->id) ? 'Update' : 'Add'))) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>