<?php
$this->assign('title', 'Ads');
?>
<?= $this->html->css('ads', ['block' => true]) ?>
<?= $this->html->script('ads', ['block' => true]) ?>
<?= $this->Form->create($ads, ['type' => 'file']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Add Ads') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <label for="image">
                        <?= $this->Html->image((!empty($ads['image']) ? $ads['image'] : 'not-found.png'), ['class' => 'thumbnail-image', 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                    </label>
                    <p class="thumbnail-paragraph">Banner Image For Ads</p>
                </div>
                <div class="col-md-12">
                    <?= $this->Form->control('title') ?>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $this->Form->control('start_date', ['type' => 'text', 'datetimepicker' => true, 'autocomplete' => 'off', 'value' => (!empty($ads['start_date']) ? date('Y-m-d h:i:s', strtotime($ads['start_date'])) : '')]) ?>
                            <?= $this->Form->control('link', ['type' => 'url']) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->control('end_date', ['type' => 'text', 'datetimepicker' => true, 'autocomplete' => 'off', 'value' => (!empty($ads['end_date']) ? date('Y-m-d h:i:s', strtotime($ads['end_date'])) : '')]) ?>
                            <?= $this->Form->control('position', ['options' => $position]) ?>
                        </div>
                    </div>
                    <?php
                    echo $this->Form->control('image', ['type' => 'file', 'class' => 'hidden', 'label' => false, 'accept' => 'image/*', 'required' => false]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Form->button(__('Add')) ?>
    </div>
    <?= $this->Form->end() ?>
    <!-- /.box-footer-->
</div>
<!-- /.box -->