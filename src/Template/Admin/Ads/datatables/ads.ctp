<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $this->Html->link($this->Html->image($result->image, ['class' => 'img-circle-border']), $result->image, ['fancybox' => true, 'escape' => false]),
        h($result->title),
        $this->Html->link($result->link, $result->link, ['target' => '_BLANK']),
        $result->start_date,
        $result->end_date,
        $position[$result->position],
        $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit', $result->id], ['escape' => false, 'class' => 'btn btn-info']) . '&nbsp;' .
        $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->client_name), 'escape' => false, 'class' => 'btn btn-danger'])
    ]);
}
echo $this->DataTables->response();
