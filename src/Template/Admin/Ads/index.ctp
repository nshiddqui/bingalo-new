<?php
$this->assign('title', 'Ads');
echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css');
echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js');
?>
<style>
    .img-circle-border{
        height: 60px;
        object-fit: cover;
        max-width: 120px;
        border-radius: 5px;
    }
</style>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Ads</h3>
        <?= $this->Html->link('<i class="fa fa-plus"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-primary pull-right']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Ads') ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        loadElement();
    });
    $(document).ajaxComplete(function () {
        loadElement();
    });
    function loadElement() {
        $('[fancybox]').fancybox();
    }
</script>