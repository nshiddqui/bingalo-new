<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result['reported_user']->full_name,
        h($result->reported_reason),
        h($result->reported_desc),
        ( $type ? @$result['product']->product_title : (!empty($result['user']) ? $this->Html->link($result['user']->full_name, ['controller' => 'users', 'action' => 'view', $result['user']->id]) : '' )),
        $result->created_at,
        $this->Html->link('<i class="fa fa-envelope"></i>', '#', ['escape' => false, 'class' => 'btn btn-info', 'data-toggle' => 'modal', 'data-target' => '#modal-default', 'onclick' => "$('#email').val('" . ( $type ? @$result['product']->uploaded_by_user_id : @$result['user']->id ) . "')"]) . '&nbsp;' .
        $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->reported_reason), 'escape' => false, 'class' => 'btn btn-danger'])
    ]);
}
echo $this->DataTables->response();
