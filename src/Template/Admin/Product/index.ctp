<?php
$this->assign('title', 'Products Management');
?>
<style>
    .img-circle-border{
        height: 60px;
        object-fit: cover;
        max-width: 120px;
        border-radius: 5px;
    }
</style>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Products</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Product') ?>
    </div>
</div>