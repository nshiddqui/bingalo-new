<?php
$this->assign('title', 'View Product Details');
?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css') ?>
<?= $this->Html->script('https://maps.googleapis.com/maps/api/js?key=' . $googleApiKey) ?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js') ?>
<div class="box-body">
    <div class="row">
        <div class="col-md-7">
            <div class="owl-carousel">
                <?php
                foreach ($product['post_images'] as $post_images) {
                    echo $this->Html->link($this->Html->image($post_images->image_url, ['alt' => $post_images->image_name]), $post_images->image_url, ['escape' => false, 'fancybox' => true]);
                }
                ?>
            </div>
        </div>
        <div class="col-md-5">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user-2">
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        <li><a href='javascript:void(0)'><strong>Product ID</strong><span class="pull-right"><?= $product->id ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Product Name</strong><span class="pull-right"><?= $product->product_title ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Product Price</strong><span class="pull-right">$ <?= $product->product_price ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Product Category</strong><span class="pull-right"><?= $product->product_category ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Product Is Sold?</strong><span class="pull-right"><?= $product->product_is_sold == '0' ? 'No' : 'Yes' ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Product Posted By</strong><span class="pull-right"><?= $product['user']->full_name ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Product Location</strong><span class="pull-right"><?= $product->product_location ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Product Specifications</strong><span class="pull-right"><?= $product->product_specifications ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Created Date & Time</strong><span class="pull-right"><?= $product->created_at ?></span></a></li>
                    </ul>
                </div>
            </div>
            <!-- /.widget-user -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label>Product Description</label>
            <br>
            <pre>
                <?= $product->product_description ?>
            </pre>
        </div>
        <div class="col-md-12">
            <label>Location</label>
            <div class="map" id="map" style="height:250px"></div>
        </div>
    </div>
</div>
</div>
<script>
    function initialize() {
        const center = {
            lat: <?= $product->product_lat ?>,
            lng: <?= $product->product_lng ?>
        };
        // Create the map.
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 13,
            center: center,
            mapTypeId: "terrain",
        });
        new google.maps.Marker({
            position: center,
            map
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
    jQuery(".owl-carousel").owlCarousel({
        autoplay: true,
        lazyLoad: true,
        loop: true,
        margin: 20,
        dots: false,
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        nav: true,
        responsive: {
            0: {
                items: 1
            },

            600: {
                items: 1
            },

            1024: {
                items: 1
            },

            1366: {
                items: 1
            }
        }
    });
</script>