<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $this->Html->link($this->Html->image($result['post_image']->image_url, ['class' => 'img-circle-border']), $result['post_image']->image_url, ['fancybox' => true, 'escape' => false]),
        $result->product_title,
        $result->parent_category,
        $result->product_price,
        $this->Html->link($result['user']->full_name, ['controller' => 'users', 'action' => 'view', $result['user']->id]),
        $result->created_at,
//        $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit', $result->id], ['escape' => false, 'class' => 'btn btn-info']) . '&nbsp;&nbsp;&nbsp;&nbsp;' .
        $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'delete', $result->id], ['escape' => false, 'class' => 'btn btn-danger', 'confirm' => __('Are you sure you want to delete # {0}?', $result->product_title)])
    ]);
}
echo $this->DataTables->response();
