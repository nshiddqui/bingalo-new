<?php
$this->assign('title', 'Faq');
?>
<style>
    .pagination ul li:before {
        background: #fff;
    }

    .form-control {
        padding-left: 2.375rem;
    }

    .form-control-feedback {
        position: absolute;
        z-index: 5;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 2.375rem;
        text-align: center;
        pointer-events: none;
        color: #aaa;
    }

    h3.panel-title {
        font-size: 15px;
    }

    .panel-body p {
        font-size: 15px;
    }
</style>
<main class="site-content">
    <header class="section section-color-bar section-color-bar-blue">
        <div class="container">
            <h1 class="color-bar-title">We're here to help!</h1>
        </div>
    </header>
    <div class="bg-white mt-5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-6 py-4">
                    <h6 class="font-weight-bold text-primary">Popular Topics</h6>
                    <div class="panel-group mt-4" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading mb-3" role="tab" id="heading0">
                                <h3 class="panel-title">
                                    <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
                                        <?= $this->Html->image('question-mark.png', ['style' => 'height: 20px; margin-right: 5px;']) ?>What are the benefits of Solodev CMS? <?= $this->Html->image('down_arrow.png', ['class' => 'float-right']) ?>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapse0" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading0">
                                <div class="panel-body px-3 mb-4">
                                    <p>With Solodev CMS, you and your visitors will benefit from a finely-tuned technology stack that drives the highest levels of site performance, speed and engagement - and contributes more to your bottom line. Our users fell in love with:</p>
                                    <ul>
                                        <li>Light speed deployment on the most secure and stable cloud infrastructure available on the market.</li>
                                        <li>Scalability – pay for what you need today and add-on options as you grow.</li>
                                        <li>All of the bells and whistles of other enterprise CMS options but without the design limitations - this CMS simply lets you realize your creative visions.</li>
                                        <li>Amazing support backed by a team of Solodev pros – here when you need them.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="panel panel-default">
                            <div class="panel-heading mb-3" role="tab" id="heading1">
                                <h3 class="panel-title">
                                    <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                        <?= $this->Html->image('question-mark.png', ['style' => 'height: 20px; margin-right: 5px;']) ?>How easy is it to build a website with Solodev CMS? <?= $this->Html->image('down_arrow.png', ['class' => 'float-right']) ?>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                                <div class="panel-body px-3 mb-4">
                                    <p>Building a website is extremely easy. With a working knowledge of HTML and CSS you will be able to have a site up and running in no time.</p>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="panel panel-default">
                            <div class="panel-heading mb-3" role="tab" id="heading2">
                                <h3 class="panel-title">
                                    <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                        <?= $this->Html->image('question-mark.png', ['style' => 'height: 20px; margin-right: 5px;']) ?>What is the uptime for Solodev CMS? <?= $this->Html->image('down_arrow.png', ['class' => 'float-right']) ?>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                <div class="panel-body px-3 mb-4">
                                    <p>Using Amazon AWS technology which is an industry leader for reliability you will be able to experience an uptime in the vicinity of 99.95%.</p>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="panel panel-default">
                            <div class="panel-heading mb-3" role="tab" id="heading3">
                                <h3 class="panel-title">
                                    <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                        <?= $this->Html->image('question-mark.png', ['style' => 'height: 20px; margin-right: 5px;']) ?>Can Solodev CMS handle multiple websites for my company? <?= $this->Html->image('down_arrow.png', ['class' => 'float-right']) ?>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                <div class="panel-body px-3 mb-4">
                                    <p>Yes, Solodev CMS is built to handle the needs of any size company. With our Multi-Site Management, you will be able to easily manage all of your websites.</p>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <h6 class="font-weight-bold text-primary">Known issues</h6>
                    <div class="panel-group mt-4" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading mb-3" role="tab" id="heading0">
                                <h3 class="panel-title">
                                    <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
                                        <?= $this->Html->image('question-mark.png', ['style' => 'height: 20px; margin-right: 5px;']) ?>What are the benefits of Solodev CMS? <?= $this->Html->image('down_arrow.png', ['class' => 'float-right']) ?>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapse0" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading0">
                                <div class="panel-body px-3 mb-4">
                                    <p>With Solodev CMS, you and your visitors will benefit from a finely-tuned technology stack that drives the highest levels of site performance, speed and engagement - and contributes more to your bottom line. Our users fell in love with:</p>
                                    <ul>
                                        <li>Light speed deployment on the most secure and stable cloud infrastructure available on the market.</li>
                                        <li>Scalability – pay for what you need today and add-on options as you grow.</li>
                                        <li>All of the bells and whistles of other enterprise CMS options but without the design limitations - this CMS simply lets you realize your creative visions.</li>
                                        <li>Amazing support backed by a team of Solodev pros – here when you need them.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="panel panel-default">
                            <div class="panel-heading mb-3" role="tab" id="heading1">
                                <h3 class="panel-title">
                                    <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                        <?= $this->Html->image('question-mark.png', ['style' => 'height: 20px; margin-right: 5px;']) ?>How easy is it to build a website with Solodev CMS? <?= $this->Html->image('down_arrow.png', ['class' => 'float-right']) ?>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                                <div class="panel-body px-3 mb-4">
                                    <p>Building a website is extremely easy. With a working knowledge of HTML and CSS you will be able to have a site up and running in no time.</p>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="panel panel-default">
                            <div class="panel-heading mb-3" role="tab" id="heading2">
                                <h3 class="panel-title">
                                    <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                        <?= $this->Html->image('question-mark.png', ['style' => 'height: 20px; margin-right: 5px;']) ?>What is the uptime for Solodev CMS? <?= $this->Html->image('down_arrow.png', ['class' => 'float-right']) ?>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                <div class="panel-body px-3 mb-4">
                                    <p>Using Amazon AWS technology which is an industry leader for reliability you will be able to experience an uptime in the vicinity of 99.95%.</p>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="panel panel-default">
                            <div class="panel-heading mb-3" role="tab" id="heading3">
                                <h3 class="panel-title">
                                    <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                        <?= $this->Html->image('question-mark.png', ['style' => 'height: 20px; margin-right: 5px;']) ?>Can Solodev CMS handle multiple websites for my company? <?= $this->Html->image('down_arrow.png', ['class' => 'float-right']) ?>
                                    </a>
                                </h3>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                <div class="panel-body px-3 mb-4">
                                    <p>Yes, Solodev CMS is built to handle the needs of any size company. With our Multi-Site Management, you will be able to easily manage all of your websites.</p>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>