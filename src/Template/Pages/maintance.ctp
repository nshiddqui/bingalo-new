<?php
$this->enableAutoLayout(false);
?>
<!doctype html>
<title>BIngalo: Site Maintenance</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
<style>
    @font-face {
        font-family: avenir-regular;
        src: url(/font/mobile/AvenirNextLTPro-Regular.otf);
    }

    @font-face {
        font-family: avenir-bold;
        src: url(/font/mobile/AvenirNextLTPro-Bold.otf);
    }

    html,
    body {
        padding: 0;
        margin: 0;
        width: 100%;
        height: 100%;
    }

    * {
        box-sizing: border-box;
    }

    body {
        text-align: center;
        padding: 0;
        background: #fff;
        color: #2a80c7;
        font-family: Open Sans;
    }

    h1 {
        font-family: avenir-bold;
        font-size: 50px;
        font-weight: 100;
        text-align: center;
    }

    body {
        font-family: avenir-regular;
        font-weight: 100;
        font-size: 20px;
        color: #2a80c7;
        text-align: center;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    article {
        display: block;
        width: 700px;
        padding: 50px;
        margin: 0 auto;
    }

    a {
        color: #2a80c7;
        font-weight: bold;
    }

    a:hover {
        text-decoration: none;
    }

    svg {
        width: 75px;
        margin-top: 1em;
    }
</style>
<?= $this->Html->image('logo.png', ['style' => 'height:40px;position: absolute;top: 20px;']) ?>
<article>
    <?= $this->Html->image('animation_500_lbuxjzmt.gif', ['style' => 'width:100vw']) ?>
    <h1 style="margin-bottom: 0;"><?= $app_maintaince->maintaince_message ?></h1>
    <div>
        <p style="margin-top: 0;color: darkgrey;"><?= $app_maintaince->maintaince_detail ?></p>
    </div>
</article>