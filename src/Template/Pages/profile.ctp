<div class="main-container">
    <div class="banner">
        <div class="row-home" onclick="window.location.href = '/'">
            <?= $this->Html->image('mobile/arrow.png') ?>
            <h2><?= __('Home') ?></h2>
        </div>
    </div>

    <div class="banner">
        <div class="man" style="width: fit-content;">
            <span style="color: #0872BB; background: #fff; float: right; padding: 2px 4px 2px 10px; border-radius: 5px; font-weight: bold;">
                <?= $user->full_name ?> <?php if ($user->profile_verified == '1') { ?><?= $this->Html->image('profile_verified.png', ['style' => 'height: 16px;margin-top: -2px;']) ?><?php } ?>
                <div style="margin-top: -5px;">
                    <?php if ($user->rating >= 1) { ?>
                        <span><?= $this->Html->image('rate_star_filled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } else if ($user->rating > 0 && $user->rating < 1) { ?>
                        <span><?= $this->Html->image('rate_star_half_filled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } else { ?>
                        <span><?= $this->Html->image('rate_star_unfilled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } ?>
                    <?php if ($user->rating >= 2) { ?>
                        <span><?= $this->Html->image('rate_star_filled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } else if ($user->rating > 1 && $user->rating < 2) { ?>
                        <span><?= $this->Html->image('rate_star_half_filled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } else { ?>
                        <span><?= $this->Html->image('rate_star_unfilled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } ?>
                    <?php if ($user->rating >= 3) { ?>
                        <span><?= $this->Html->image('rate_star_filled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } else if ($user->rating > 2 && $user->rating < 3) { ?>
                        <span><?= $this->Html->image('rate_star_half_filled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } else { ?>
                        <span><?= $this->Html->image('rate_star_unfilled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } ?>
                    <?php if ($user->rating >= 4) { ?>
                        <span><?= $this->Html->image('rate_star_filled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } else if ($user->rating > 3 && $user->rating < 4) { ?>
                        <span><?= $this->Html->image('rate_star_half_filled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } else { ?>
                        <span><?= $this->Html->image('rate_star_unfilled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } ?>
                    <?php if ($user->rating >= 5) { ?>
                        <span><?= $this->Html->image('rate_star_filled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } else if ($user->rating > 4 && $user->rating < 5) { ?>
                        <span><?= $this->Html->image('rate_star_half_filled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } else { ?>
                        <span><?= $this->Html->image('rate_star_unfilled.png', ['style' => 'height: 10px;']) ?></span>
                    <?php } ?>
                </div>
            </span>
            <div style="position:relative">
                <?= $this->Html->image($user->image, ['style' => 'height:52px;width:52px;border-radius:100%;object-fit: cover;']) ?>
            </div>
        </div>
        <div style="margin-top: 10px;">
            <style>
                .w3-bar .w3-bar-item {
                    border-radius: 0;
                }

                .w3-bar .w3-bar-item.active {
                    color: #0872BB;
                    border-bottom: 3px solid #0872BB;
                }

                .w3-button:hover {
                    color: #0872BB !important;
                    background-color: #fff !important;
                }

                .slider-section .single-item {
                    display: block;
                    float: left;
                }

                .slider-section .single-item {
                    width: 44%;
                    margin-bottom: 20px;
                }
            </style>
            <div class="w3-bar" style="color:#7B7B7B;font-weight:bold">
                <button class="w3-bar-item w3-button active" onclick="openCity('selling');$('.w3-bar-item').removeClass('active');$(this).addClass('active')"><?= __('Selling') ?></button>
                <button class="w3-bar-item w3-button" onclick="openCity('sold');$('.w3-bar-item').removeClass('active');$(this).addClass('active')"><?= __('Sold') ?></button>
                <?php if ($favourite_required) { ?>
                    <button class="w3-bar-item w3-button" onclick="openCity('favourite');$('.w3-bar-item').removeClass('active');$(this).addClass('active')"><?= __('Favourite') ?></button>
                <?php } ?>
            </div>

            <div id="selling" class="w3-container city" style="padding: 0;margin-top:10px">
                <div class="slider-section" style="padding: 0;width: calc(100% + 20px);">
                    <?php foreach ($selling as $key => $product) {
                    ?>
                        <div class="slide single-item" onclick="window.location.href='/product/<?= $product->id ?>'">
                            <div class="product-image">
                                <span class="fav-btn">
                                    <?= $this->Html->image(((isset($product['favourite']) && !empty($product['favourite'])) ? 'mobile/heart_fill.png' : 'mobile/heart.svg'), ['url' => ($authUser ? "javascript:markFavourite({$product->id});" : 'javascript:void(0)'), 'login' => '1', 'class' => "img-favourite-" . $product->id, 'style' => ((isset($product['favourite']) && !empty($product['favourite'])) ? 'width: 25px;height: 25px;' : '')]) ?>
                                </span>
                                <?= $this->Html->image($product['post_image']->image_url, ['class' => 'top-listing-image']) ?>
                            </div>
                            <div class="single-item-content">
                                <h3><?= $product->product_title ?></h3>
                                <span><?= $product->product_price ?>₪</span>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div id="sold" class="w3-container city" style="display:none;padding: 0;margin-top:10px">
                <div class="slider-section" style="padding: 0;width: calc(100% + 20px);">
                    <?php foreach ($sold as $key => $product) {
                    ?>
                        <div class="slide single-item" onclick="window.location.href='/product/<?= $product->id ?>'">
                            <div class="product-image">
                                <span class="fav-btn">
                                    <?= $this->Html->image(((isset($product['favourite']) && !empty($product['favourite'])) ? 'mobile/heart_fill.png' : 'mobile/heart.svg'), ['url' => ($authUser ? "javascript:markFavourite({$product->id});" : 'javascript:void(0)'), 'login' => '1', 'class' => "img-favourite-" . $product->id, 'style' => ((isset($product['favourite']) && !empty($product['favourite'])) ? 'width: 25px;height: 25px;' : '')]) ?>
                                </span>
                                <?= $this->Html->image($product['post_image']->image_url, ['class' => 'top-listing-image']) ?>
                            </div>
                            <div class="single-item-content">
                                <h3><?= $product->product_title ?></h3>
                                <span><?= $product->product_price ?>₪</span>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php if ($favourite_required) { ?>
                <div id="favourite" class="w3-container city" style="display:none;padding: 0;margin-top:10px">
                    <div class="slider-section" style="padding: 0;width: calc(100% + 20px);">
                        <?php foreach ($favourite as $key => $product) {
                        ?>
                            <div class="slide single-item" onclick="window.location.href='/product/<?= $product->id ?>'">
                                <div class="product-image">
                                    <span class="fav-btn">
                                        <?= $this->Html->image(((isset($product['favourite']) && !empty($product['favourite'])) ? 'mobile/heart_fill.png' : 'mobile/heart.svg'), ['url' => ($authUser ? "javascript:markFavourite({$product->id});" : 'javascript:void(0)'), 'login' => '1', 'class' => "img-favourite-" . $product->id, 'style' => ((isset($product['favourite']) && !empty($product['favourite'])) ? 'width: 25px;height: 25px;' : '')]) ?>
                                    </span>
                                    <?= $this->Html->image($product['post_image']->image_url, ['class' => 'top-listing-image']) ?>
                                </div>
                                <div class="single-item-content">
                                    <h3><?= $product->product_title ?></h3>
                                    <span><?= $product->product_price ?>₪</span>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
            <script>
                function openCity(cityName) {
                    var i;
                    var x = document.getElementsByClassName("city");
                    for (i = 0; i < x.length; i++) {
                        x[i].style.display = "none";
                    }
                    document.getElementById(cityName).style.display = "block";
                }
            </script>
        </div>
    </div>
</div>