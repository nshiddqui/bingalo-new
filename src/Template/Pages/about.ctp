<?php
$this->assign('title', 'About Us');
?>
<main class="site-content">
    <header class="section section-color-bar section-color-bar-blue">
        <div class="container">
            <h1 class="color-bar-title">Bingalo is your marketplace.</h1>
        </div>
    </header>
    <section class="section section-text">
        <header class="section-header section-header-hidden">
            <div class="container">
                <h1 class="section-header-title">Who We Are</h1>
            </div>
        </header>
        <div class="section-content">
            <div class="container" style='text-align: left'>
                <p>It’s the perfect place to go to declutter or discover items that will breathe new life into your space. Letting go has never felt so good – it’s almost as good as finding your new favorite thing. And we’re here to help.</p>

                <p>We connect millions of people across the U.S. to shop and sell almost anything. We all have things we don’t use, never used, or simply outgrew. But these treasured items still have value. Our team is always trying to find new ways to make exchanging items even easier. This means at-home pickups, same-day delivery, and a curated list of items you’ll love.</p>

                <p>Say ‘goodbye’ to the things you’re no longer using and ‘hello’ to delightful new finds.</p>
            </div>
        </div>
    </section>
    <section class="section section-orange section-statistics">
        <header class="section-header section-header-hidden">
            <div class="container">
                <h1 class="section-header-title">How We're Doing</h1>
            </div>
        </header>
        <div class="section-content">
            <div class="container statistics-group">
                <div class="statistic">
                    <h2 class="statistic-title"><span id="statistic-downloads">0</span>M+</h2>
                    <div class="statistic-description">
                        <p>Downloads in the U.S.</p>
                    </div>
                </div>
                <div class="statistic">
                    <h2 class="statistic-title"><span id="statistic-items">0</span>K+</h2>
                    <div class="statistic-description">
                        <p>New items listed every&nbsp;day</p>
                    </div>
                </div>
                <div class="statistic">
                    <h2 class="statistic-title"><span id="statistic-rating-app-store">0.0</span></h2>
                    <div class="statistic-description">
                        <p>Average <a href="https://itunes.apple.com/us/app/Bingalo-the-selling-app/id896130944?mt=8" target="_blank" title="Bingalo on the App Store">App Store</a> app&nbsp;rating</p>
                    </div>
                </div>
                <div class="statistic">
                    <h2 class="statistic-title"><span id="statistic-rating-google-play">0.0</span></h2>
                    <div class="statistic-description">
                        <p>Average <a href="https://play.google.com/store/apps/details?id=com.Bingaloapp.Bingalo&hl=en" target="_blank" title="Bingalo on Google Play">Google Play</a> app&nbsp;rating</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <header class="section-header">
            <div class="container">
                <h1 class="section-header-title">Meet the team.</h1>
            </div>
        </header>

        <div class="section-content">
            <div class="tech-member-group">
                <div class="tech-member">
                    <div class="team-member-image">
                        <picture>
                            <?= $this->Html->image('attachment.png') ?>
                        </picture>
                    </div>
                    <div class="tech-bio">
                        <h2 class="team-member-name">John Lagerling</h2>
                        <p class="team-member-title tech-title">CEO, Bingalo U.S.</p>
                        <p class="tech-desc">John leads the Bingalo U.S. team, in addition to serving on Bingalo’s global board of directors. Originally from Sweden, John started his career with NTT DoCoMo in Japan before moving to the U.S. to lead business development teams at Google and Facebook.<br>A private pilot and longtime video gamer, John loves searching Bingalo for the nostalgic flight simulators and other old-school video games that carried him through childhood. John also buys and sells electronics, and an occasional sneaker or two.</p>
                    </div>
                </div>
                <div class="tech-member">
                    <div class="team-member-image">
                        <picture>
                            <?= $this->Html->image('attachment.png') ?>
                        </picture>
                    </div>
                    <div class="tech-bio">
                        <h2 class="team-member-name">Dr. Mok Oh</h2>
                        <p class="team-member-title tech-title">Chief Technology Officer</p>
                        <p class="tech-desc">Mok leads Bingalo’s engineering teams in Palo Alto, CA, Cambridge, MA, and Tokyo, Japan, from client and backend technologies, to machine learning, data engineering, computer vision and operations scaling. Before Bingalo, Mok’s career included executive roles at Samsung Electronics and PayPal, as well as in venture capital and his own startups. Mok holds a Ph.D. from MIT, an M.S. from the University of Pennsylvania, and two B.A. degrees (Computer Science and Art History) from Oberlin College. He holds 12 patents.<br>Mok and his wife use Bingalo to keep their hallways clear of their two kids’ outgrown and forgotten clothes, toys and electronics.</p>
                    </div>
                </div>
                <div class="tech-member">
                    <div class="team-member-image">
                        <picture>
                            <?= $this->Html->image('attachment.png') ?>
                        </picture>
                    </div>
                    <div class="tech-bio">
                        <h2 class="team-member-name">Jenny Kim</h2>
                        <p class="team-member-title tech-title">General Counsel</p>
                        <p class="tech-desc">Keeping Bingalo and its employees on the right side of the law is Jenny’s main job. Jenny honed her organizational management skills as a VP at Intel, and acquired her payments expertise at Visa where she focused on its mobile and e-commerce payment offerings. Following Visa’s IPO, she served as Senior VP in charge of Visa’s corporate securities practice. With over 25 years of experience advising her clients [C-suites and board of directors] on business and regulatory risks, Jenny now serves as an integral partner to Bingalo’s business teams.<br>Jenny’s Bingalo sales range from athletic and leisurewear to household items. She recently completed a monumental decluttering of art supplies, which she credits to Bingalo’s easy interface which allows her to list a bunch of items quickly.</p>
                    </div>
                </div>
                <div class="tech-member">
                    <div class="team-member-image">
                        <picture>
                            <?= $this->Html->image('attachment.png') ?>
                        </picture>
                    </div>
                    <div class="tech-bio">
                        <h2 class="team-member-name">Lisa Lechner</h2>
                        <p class="team-member-title tech-title">Chief Compliance Officer</p>
                        <p class="tech-desc">Lisa leads a team of compliance and risk management superheroes working to protect Bingalo’s customers and platform 24/7. Her responsibilities include creating and enforcing policies intended to prevent bad experiences for our sellers and buyers. Her career has included leadership roles with PayPal, Venmo, Braintree, RNA and AVG.<br>While waiting for her children to stop growing -- and stop changing their minds about what’s “in” and “out” – Lisa keeps busy selling her fashionista daughter’s castoffs as well as her sports-obsessed son’s forgotten gear.</p>
                    </div>
                </div>
                <div class="tech-member">
                    <div class="team-member-image">
                        <picture>
                            <?= $this->Html->image('attachment.png') ?>
                        </picture>
                    </div>
                    <div class="tech-bio">
                        <h2 class="team-member-name">Robin Clark</h2>
                        <p class="team-member-title tech-title">SVP, Special Projects</p>
                        <p class="tech-desc">Robin serves as a key member of Bingalo’s U.S. executive team, helping guide the business and managing strategic projects to improve Bingalo’s value to sellers and buyers. Before Bingalo, Robin directed finance and sales operations at Trulia and played a key role in the company’s IPO. Before Trulia, Robin held senior finance positions with StubHub, StepUp, Wells Fargo and Waste Management, among others.<br>With a passion for the culinary arts Robin also collects vintage Pyrex pieces, which she sells and buys on Bingalo.</p>
                    </div>
                </div>
                <div class="tech-member">
                    <div class="team-member-image">
                        <picture>
                            <?= $this->Html->image('attachment.png') ?>
                        </picture>
                    </div>
                    <div class="tech-bio">
                        <h2 class="team-member-name">Shida Schubert</h2>
                        <p class="team-member-title tech-title">VP of Product</p>
                        <p class="tech-desc">Shida leads Bingalo’s product innovation team, focused on our mission of making selling easier than buying while meeting the evolving needs of our users. Before Bingalo, he was building new businesses as chief technology officer at Digital Garage US, while contributing his technical expertise and product insights to the company’s investment arm as a director.<br>Shida can often be seen at Ocean Beach and other Bay Area surf breaks, riding the waves by himself or with his son. Shida is an active seller and buyer of electronics and sporting goods on Bingalo.</p>
                    </div>
                </div>
                <div class="tech-member">
                    <div class="team-member-image">
                        <picture>
                            <?= $this->Html->image('attachment.png') ?>
                        </picture>
                    </div>
                    <div class="tech-bio">
                        <h2 class="team-member-name">Sho Masuda</h2>
                        <p class="team-member-title tech-title">VP of Growth</p>
                        <p class="tech-desc">Sho’s role as Vice President of Growth focuses on driving user growth and user engagement. He’s passionate about growth and has built multiple $100+ million consumer businesses from scratch. His prior experience includes Vice President of Growth at Poshmark and a founding member of GREE’s San Francisco mobile gaming studio.<br>A gadget enthusiast, Sho’s Bingalo sales include Wii U, Apple TV, Bose Headphones, and more. He uses the earnings to keep fulfilling his self-interest.</p>
                    </div>
                </div>
            </div>
        </div>

        <header class="section-header section-header--tech">
            <div class="container">
                <h1 class="section-header-title">Technology@Bingalo</h1>
            </div>
        </header>

        <div class="section-text">
            <div class="section-content">
                <div class="container">
                    <p>To continuously improve our services and take advantage of emerging technologies, Bingalo employs some of the world’s most innovative thinkers. Technology teams in Tokyo, Japan; Palo Alto, California; and Cambridge, Massachusetts pursue applied research projects individually and collaboratively with a shared focus on machine learning, data engineering, computer vision, and operations scaling. We’re also proud of our affiliations with leading research universities in Japan and the U.S.</p>
                </div>
            </div>
        </div>
        <div class="section-content">
            <div class="tech-member-group">
                <div class="tech-member">
                    <div class="team-member-image">
                        <picture>
                            <?= $this->Html->image('attachment.png') ?>
                        </picture>
                    </div>
                    <div class="tech-bio">
                        <h2 class="team-member-name">Dr. Frédo Durand</h2>
                        <p class="team-member-title tech-title">Bingalo Technology Advisor, Cambridge, Massachusetts</p>
                        <p class="tech-desc">Dr. Frédo Durand is a renowned researcher and professor in computer vision and computational photography. He is a professor of Electrical Engineering and Computer Science at the Massachusetts Institute of Technology (MIT), and a member of MIT’s Computer Science and Artificial Intelligence Laboratory (CSAIL). He received his PhD from Grenoble University (France) in 1999, and was a post-doc in the MIT Computer Graphics Group. His research interests span most aspects of picture generation and creation, with emphasis on mathematical analysis, signal processing, and inspiration from perceptual sciences. He received an inaugural Eurographics Young Researcher Award in 2004, an NSF CAREER award in 2005, an inaugural Microsoft Research New Faculty Fellowship in 2005, a Sloan fellowship in 2006, a Spira award for distinguished teaching in 2007, and the ACM SIGGRAPH Computer Graphics Achievement Award in 2016.</p>
                    </div>
                </div>
                <div class="tech-member">
                    <div class="team-member-image">
                        <picture>
                            <?= $this->Html->image('attachment.png') ?>
                        </picture>
                    </div>
                    <div class="tech-bio">
                        <h2 class="team-member-name">Dr. Wojciech Matusik</h2>
                        <p class="team-member-title tech-title">Bingalo Technology Advisor, Cambridge, Massachusetts</p>
                        <p class="tech-desc">Dr. Wojciech Matusik is widely-recognized professor and entrepreneur in 3D vision, 3D printing, and machine learning. He is a professor of Electrical Engineering and Computer Science at the Computer Science and Artificial Intelligence Laboratory (CSAIL) at the Massachusetts Institute of Technology (MIT), where co-directs the Computer Graphics Group. He studied computer graphics at MIT and received his PhD in 2003. He also received a BS in EECS from the University of California at Berkeley in 1997, and an MS in EECS from MIT in 2001. His research interests are in computer graphics, vision, computational design, human-computer interaction, and robotics. In 2004, he was named one of the world's top 100 young innovators by MIT's Technology Review Magazine. In 2009, he received the Significant New Researcher Award from ACM Siggraph. In 2012, Matusik received the DARPA Young Faculty Award and he was named a Sloan Research Fellow.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <header class="section-header">
            <div class="container">
                <h1 class="section-header-title">Our offices.</h1>
            </div>
        </header>
        <div class="section-content">
            <div class="container offices-group">
                <div class="office">
                    <div class="office-image"></div>
                    <div class="office-text">
                        <h2 class="office-title">Japan</h2>
                        <p class="office-description">Tokyo, Sendai, Fukuoka</p>
                    </div>
                </div>
                <div class="office">
                    <div class="office-image"></div>
                    <div class="office-text">
                        <h2 class="office-title">United States</h2>
                        <p class="office-description">Palo Alto, Portland, Boston</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-color-bar section-color-bar-orange">
        <div class="container">
            <h1 class="color-bar-title-long">Want to join us?</h1>
            <div class="color-bar-text">
                <p><a href="/careers" title="View openings" class="button">View openings</a></p>
            </div>
        </div>
    </section>
    <section class="section section-app-cta">
        <picture>
            <source srcset="/img/marketplace.png" media="(min-width: 640px)"><img src="/corporate-assets/img/app-cta-phone-600.png" srcset="/img/marketplace.png" alt="Bingalo app screenshot" class="app-cta-phone">
        </picture>
        <div class="container">
            <div class="app-cta-content">
                <h1 class="app-cta-title">Your Marketplace</h1>
                <div class="app-cta-links">
                    <ul>
                        <li class="app-cta-link-apple"><a href="https://itunes.apple.com/us/app/Bingalo-buy-sell-anywhere/id896130944?mt=8" target="_blank" title="Download Bingalo on the App Store">Download Bingalo on the App Store</a></li>
                        <li class="app-cta-link-google"><a href="https://play.google.com/store/apps/details?id=com.Bingaloapp.Bingalo&hl=en" target="_blank" title="Get Bingalo on Google Play">Get Bingalo on Google Play</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</main>