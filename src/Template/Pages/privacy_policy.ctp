<?php
$this->assign('title', 'Privacy Policy');
?>
<style>
    .pagination ul li:before {
        background: #fff;
    }

    .form-control {
        padding-left: 2.375rem;
    }

    .form-control-feedback {
        position: absolute;
        z-index: 5;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 2.375rem;
        text-align: center;
        pointer-events: none;
        color: #aaa;
    }

    h3.panel-title {
        font-size: 15px;
    }

    .panel-body p {
        font-size: 15px;
    }

    .content h3 {
        font-size: 1.5em;
        margin-bottom: 0.6666em;
    }
</style>
<main class="site-content">
    <header class="section section-color-bar section-color-bar-blue">
        <div class="container">
            <h1 class="color-bar-title">Bingalo Privacy Policy</h1>
        </div>
    </header>
    <div class="bg-white mt-5">
        <div class="container-fluid">
            <div class="row justify-content-center content">
                <div class="col-12">
                    <div class="content" style="margin:32px 16px">
                        <div class="column">
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">In this Policy, we, Bingalo Inc., headquartered in Palo Alto, California, inform you about our personal data collection and use practices relating to your use of Bingalo's mobile and website consumer-to-consumer marketplace service. It does not cover other data collection or processing by Bingalo or data that other companies collect or sites or services that are linked from our service.</p>
                        </div>
                        <div class="column">
                            <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Changes</h3>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Carefully review this policy. We will amend it from time to time as our technology, services, features and business models change, so you should review our current privacy policy every time you use our service. Our privacy practices are summarized in this
                                <!-- --> <a href="#privacy_notice">Privacy Policy</a> and our Privacy Notice.
                            </p>
                        </div>
                        <div class="column">
                            <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Children</h3>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Children under 18 are not allowed to use the Bingalo service. If you are a parent or guardian of such a child and become aware that your child has provided personal information to us, please contact us as described in this Privacy Policy and we will take reasonable steps immediately to remove such information.</p>
                        </div>
                        <div class="column">
                            <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Account Data</h3>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">When you sign up for our service, you actively provide us with account registration information that you can access and update within the Bingalo app or website at any time, including:</p>
                            <ul class="List-sc-1ekie5o-0 gqYahi">
                                <li class="ListItem-qv73n8-0 gdbGle">Contact information, such as your name, address, phone, and email</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Financial information, such as bank account and/or credit card numbers</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Detailed personal information such as your date of birth or tax identification number</li>
                            </ul>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">We use your email address to communicate with you about transactions, new services and other topics we believe are of interest to you. We use your mailing address for delivery of items you purchase through our service. We collect your credit card or other payment card information for payment processing purposes and your bank account information to post deposits when you sell items through our service. We collect social network info for login and authentication purposes.</p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">When we are required by applicable law, we collect additional information from you such as your social security number or tax identification number or date of birth, and we may request you to provide us with such information. You consent to promptly comply with such a request from us if you wish to continue using the service. We use this additional information, in combination with other information we may collect from you such as information about your government-issued identification, for identity verification and fraud prevention purposes, or any other purposes necessary to comply with applicable law.</p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">We may also update your registration information, for example, if you provide a new email or mailing address on your account.</p>
                        </div>
                        <div class="column">
                            <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Usage Data</h3>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">When you use the Bingalo service, we process and deliver to you and other users of our service information about your Bingalo transactions, such as information about items that you post for sale through our service and messages exchanged with other Bingalo users. In this context, we receive information about your device, software, who you communicate with, message content, locations and our access logs, which includes IP address, device ID, your actions within our service and language settings.</p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Our browser-based app version and website also places cookies (session cookies and persistent cookies) as necessary to provide functionality to you. We associate usage data with your device and your personal account. We process usage information to deliver, improve and optimize our Bingalo service, to protect you, us and other users from actual or potential fraud and misuse, and to determine what information, features, promotional information and other services you need or may be interested in. We may combine your usage information with information we collect from other companies and use it to improve and personalize your Bingalo experience.</p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">The information Bingalo collects may be sent to us by your computer, mobile phone or other access device. The information includes, but is not limited to, the following: data about the pages you access, computer IP address, device ID or unique identifier, device type, geo-location information (IP country and city), computer and connection information, internet service provider (ISP), mobile network information, statistics on page views, traffic to and from the sites, referral URL, ad data, and standard web log data and other information. You may opt out of geo-location information collection by editing the setting at your device level.</p>
                        </div>
                        <div class="column">
                            <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Third Party Sources</h3>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">We may obtain information about you from third parties such as identity verification services.</p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">We may combine your usage data with information we collect from other companies such as Google Analytics to customize and improve your user experience. Google Analytics collects information regarding the use of other websites, apps and online resources ( For more information, please visit
                                <!-- --> <a href="https://policies.google.com/privacy/partners" target="_blank" rel="noopener noreferrer">https://policies.google.com/privacy/partners</a>.)
                            </p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">You may choose to provide us with access to certain personal information stored by third parties such as social media sites (e.g., Facebook
                                <!-- --> <a href="https://www.facebook.com/policy.php" target="_blank" rel="noopener noreferrer">https://www.facebook.com/policy.php</a>). The information we may receive varies by site and is controlled by that site. By associating an account managed by a third party with your Bingalo account and authorizing Bingalo to have access to this information, you agree that we may collect, store and use this information in accordance with this Privacy Policy.
                            </p>
                        </div>
                        <div class="column">
                            <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Cookie Statement</h3>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">The cookies we use on our browser-based app and website do not hold any personal information about you. Where personally identifiable information is required to be gathered, the need for this information will be clearly communicated before you choose to provide the information. Information collected by cookies may be used to serve interest-based advertising about Bingalo’s service. If you wish to opt out of interest-based advertising click
                                <!-- --> <a href="http://optout.aboutads.info/" target="_blank" rel="noreferrer">here</a>. Please note you will continue to receive transactional data and general ads.
                            </p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Many Internet browsers allow you to adjust your cookie preferences or delete existing cookies. If you set your browser to reject cookies, you should be aware that certain browser-based app or website features, promotions or services may not be available to you or may not function correctly.</p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Certain Internet browsers may enable you to set "do not track"
                                preferences, to limit the collection of personally identifiable
                                information about your online activities over time and across
                                third-party websites or online services. Bingalo’s systems do not
                                respond to Internet browser "do not track" signals or similar
                                mechanisms.</p>
                        </div>
                        <div class="column">
                            <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Data Retention and Access</h3>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">You can review, access and edit your personal information at any time by logging in to your account and reviewing your account settings. You can also close your account.</p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">We retain account and usage data as long as required by applicable law. We may retain personal information from your account to collect any fees owed, resolve disputes, complete investigations, prevent fraud, enforce our Terms of Service User Agreement, or take other actions as required or permitted by law.</p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">If you close your Bingalo account we will inactivate the account within less than 10 business days. Please keep in mind that others can retain messages and content that you have sent them or posted publicly before you closed your account and, as we are a financial institution, we keep deactivated account information for a certain time period in our archives before we can delete any data.</p>
                        </div>
                        <div class="column">
                            <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Disclosures, Sharing</h3>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">We do not disclose your individual account or usage data to third parties, except:</p>
                            <ul class="List-sc-1ekie5o-0 gqYahi">
                                <li class="ListItem-qv73n8-0 gdbGle">with your consent or at your direction.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">as necessary to collect or process a payment.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">to service or process a product or service requested by you or otherwise provide our service to you.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">to enable service providers under contract to facilitate operations, such as fraud prevention &amp; risk management, bill collection, marketing, customer service and technology services.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">as necessary to resolve a problem between Bingalo users or with third parties.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">with affiliated and unaffiliated service providers in the United States and abroad that help us deliver, analyze, improve and manage our service, subject to confidentiality agreements.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">as required by law or to assert or protect legal rights.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">to comply with anti-money laundering and counter-terrorist financing verification requirements.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">as necessary to protect you, other users, us or third parties from
                                    actual or potential harm, including fraud, data security or
                                    confidentiality breaches, other liability, or where someone's
                                    physical safety seems at risk.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">to maintain or service your account.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">in connection with a proposed or actual sale or reorganization of our company, divisions or assets, subject to the acquirer accepting the commitments made in this policy and in compliance with applicable law.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">as described in this Privacy Policy.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">as otherwise permitted by law.</li>
                            </ul>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Note that when you publicly post content, such as sales listings, that content is visible and available to third parties.</p>
                        </div>
                        <div class="column">
                            <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">International Data Transfer</h3>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">To facilitate our operations, we may transfer, store and process your personal information in jurisdictions other than where you live, including in the United States. Laws in these countries may differ from the laws applicable to your country of residence. We will utilize appropriate safeguards governing the transfer and usage of your personal information.</p>
                        </div>
                        <div class="column">
                            <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Security</h3>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">We take required steps to protect your account information and account messages and to protect against data loss, misuse and unauthorized access. However, no internet transmission is ever completely secure or error-free. In particular, messages sent through the Bingalo service are not encrypted.</p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">It is your responsibility to control access to your device, including keeping your login information confidential and not sharing it with anyone. It is also your responsibility to notify Bingalo if you believe the security of the information in the Bingalo app or your Bingalo account has been compromised.</p>
                        </div>
                        <div class="column">
                            <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Your Choices</h3>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">We respect your communication preferences. If you no longer wish to receive notifications via our application, you can adjust your preferences by visiting the settings page of the Bingalo application.</p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Bingalo may use information collected for its own marketing purpose. If we send marketing messages, you can opt out by changing the settings in your Bingalo profile account or by contacting us at
                                <!-- --> <a href="mailto:contact-us@Bingalo.com" target="_blank" rel="noreferrer">contact-us@Bingalo.com</a> <!-- -->or by following the directions that may be provided within the communication itself to opt out. Note that if you opt out, we may still send you non-marketing messages, such as messages about your account, transactions or specifically in regards to products and services you have requested.
                            </p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">We do not sell or rent your personal information to third parties for their marketing purposes.</p>
                        </div>
                        <div class="column">
                            <p class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Questions, Requests, Rights</p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">If you have any questions or requests, please contact us in our Help Center within the mobile app or website or directly at
                                <!-- --> <a href="mailto:contact-us@Bingalo.com" target="_blank" rel="noreferrer">contact-us@Bingalo.com</a>. Please reference “Privacy Policy” in the subject line of your inquiry. You may also contact us at<br>Bingalo Inc: Attention Privacy<br>PO Box 60178<br>Palo Alto, CA 94306
                            </p>
                        </div>
                        <div style="margin-top:50px">
                            <div class="PageTitle__PageTitleWrapper-sc-2ko26e-0 dYtYfx">
                                <p color="white" class="Text-uqn6ov-0 Text__H0-uqn6ov-1 brawNo pnzey">Privacy Notice</p>
                            </div>
                            <div class="content" style="margin:16px;padding:16px">
                                <div class="privacy_notice">
                                    <div class="static-page-description">
                                        <table class="table is-bordered">
                                            <tbody>
                                                <tr>
                                                    <th>FACTS</th>
                                                    <td>WHAT DOES
                                                        <!-- --> <span class="has-text-weight-bold">Bingalo, INC.</span><br>DO WITH YOUR PERSONAL INFORMATION?
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tbody>
                                                <tr>
                                                    <th>Why?</th>
                                                    <td>Financial companies choose how they share your personal information. Federal law gives consumers the right to limit some but not all sharing. Federal law also requires us to tell you how we collect, share, and protect your personal information. Please read this notice carefully to understand what we do.</td>
                                                </tr>
                                            </tbody>
                                            <tbody>
                                                <tr>
                                                    <th>What?</th>
                                                    <td>The types of personal information we collect and share depend on the product or service you have with us. This information can include:<ul>
                                                            <li>Social Security number and name and gender and address information</li>
                                                            <li><span class="has-text-weight-bold">Credit card account</span> <!-- -->and
                                                                <!-- --> <span class="has-text-weight-bold">checking account information</span>
                                                            </li>
                                                            <li><span class="has-text-weight-bold">Transaction history</span></li>
                                                        </ul>When you are <span class="italic">no longer</span> our customer, we continue to share your information as described in this notice.</td>
                                                </tr>
                                            </tbody>
                                            <tbody>
                                                <tr>
                                                    <th>How?</th>
                                                    <td>All financial companies need to share
                                                        <!-- --> <span class="has-text-weight-bold">customers'</span> <!-- -->personal information to run their everyday business. In the section below, we list the reasons financial companies can share their
                                                        <!-- --> <span class="has-text-weight-bold">customers'</span> <!-- -->personal information; the reasons
                                                        <!-- --> <span class="has-text-weight-bold">Bingalo, Inc.</span> <!-- -->chooses to share; and whether you can limit this sharing.
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="qanda_table">
                                            <tbody>
                                                <tr>
                                                    <th>Reasons we can share your personal information</th>
                                                    <th>Does Bingalo share?</th>
                                                    <th>Can you limit this sharing?</th>
                                                </tr>
                                                <tr>
                                                    <td class="question"><span class="has-text-weight-bold">For our everyday business purposes—</span><br>such as to process your transactions, maintain your account(s), respond to court orders and legal investigations, or report to credit bureaus</td>
                                                    <td class="answer">YES</td>
                                                    <td class="answer">NO</td>
                                                </tr>
                                                <tr>
                                                    <td class="question"><span class="has-text-weight-bold">For our marketing purposes—</span><br>to offer our products and services to you</td>
                                                    <td class="answer">YES</td>
                                                    <td class="answer">NO</td>
                                                </tr>
                                                <tr>
                                                    <td class="question"><span class="has-text-weight-bold">For joint marketing with other financial companies</span></td>
                                                    <td class="answer">NO</td>
                                                    <td class="answer">We Don't Share</td>
                                                </tr>
                                                <tr>
                                                    <td class="question"><span class="has-text-weight-bold">For our affiliates’ everyday business purposes—</span><br>information about your transactions and experiences</td>
                                                    <td class="answer">NO</td>
                                                    <td class="answer">We Don't Share</td>
                                                </tr>
                                                <tr>
                                                    <td class="question"><span class="has-text-weight-bold">For our affiliates’ everyday business purposes—</span><br>information about your creditworthiness</td>
                                                    <td class="answer">NO</td>
                                                    <td class="answer">We Don't Share</td>
                                                </tr>
                                                <tr>
                                                    <td class="question"><span class="has-text-weight-bold">For our affiliates to market to you</span></td>
                                                    <td class="answer">NO</td>
                                                    <td class="answer">We Don't Share</td>
                                                </tr>
                                                <tr>
                                                    <td class="question"><span class="has-text-weight-bold">For nonaffiliates to market to you</span><br></td>
                                                    <td class="answer">NO</td>
                                                    <td class="answer">We Don't Share</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="question_table">
                                            <tbody>
                                                <tr>
                                                    <th>Questions?</th>
                                                    <td>Call
                                                        <!-- --> <span class="has-text-weight-bold">800-280-4904</span> or go to
                                                        <!-- --> <span class="has-text-weight-bold">https://www.Bingalo.com/</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="definitions_table">
                                            <tbody>
                                                <tr>
                                                    <th>Who we are</th>
                                                    <th></th>
                                                </tr>
                                                <tr>
                                                    <td class="question">Who is providing this notice?</td>
                                                    <td class="answer"><span class="has-text-weight-bold">Bingalo, Inc.</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="definitions_table">
                                            <tbody>
                                                <tr>
                                                    <th>What we do</th>
                                                    <th></th>
                                                </tr>
                                                <tr>
                                                    <td class="question">How does
                                                        <!-- --> <span class="has-text-weight-bold">Bingalo </span>protect my personal information?
                                                    </td>
                                                    <td class="answer">To protect your personal information from unauthorized access and use, we use security measures that comply with federal law. These measures include computer safeguards and secured files and buildings.</td>
                                                </tr>
                                                <tr>
                                                    <td class="question">How does
                                                        <!-- --> <span class="has-text-weight-bold">Bingalo </span>collect my personal information?
                                                    </td>
                                                    <td class="answer">We collect your personal information, for example, when you<ul>
                                                            <li><span class="has-text-weight-bold">open an account</span> <!-- -->or
                                                                <!-- --> <span class="has-text-weight-bold">provide account information</span>
                                                            </li>
                                                            <li><span class="has-text-weight-bold">give us your contact info</span> <!-- -->or
                                                                <!-- --> <span class="has-text-weight-bold">tell us where to send money</span>
                                                            </li>
                                                            <li><span class="has-text-weight-bold">use your credit or debit card</span></li>
                                                        </ul><span class="has-text-weight-bold">We also collect your personal information from others, such as credit bureaus, affiliates, or other companies.</span></td>
                                                </tr>
                                                <tr>
                                                    <td class="question">Why can’t I limit all sharing?</td>
                                                    <td class="answer">Federal law gives you the right to limit only<ul>
                                                            <li>sharing for affiliates’ everyday business purposes—information about your creditworthiness</li>
                                                            <li>affiliates from using your information to market to you</li>
                                                            <li>sharing for nonaffiliates to market to you</li>
                                                        </ul>State laws and individual companies may give you additional rights to limit sharing.</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="definitions_table">
                                            <tbody>
                                                <tr>
                                                    <th>Definitions</th>
                                                    <th></th>
                                                </tr>
                                                <tr>
                                                    <td class="question"><span class="has-text-weight-bold">Affiliates</span></td>
                                                    <td class="answer">Companies related by common ownership or control. They can be financial and nonfinancial companies.<ul>
                                                            <li><span class="bold italic">Bingalo does not share your personal information with our affiliates.</span></li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="question"><span class="has-text-weight-bold">Nonaffiliates</span></td>
                                                    <td class="answer">Companies not related by common ownership or control. They can be financial and nonfinancial companies.<ul>
                                                            <li><span class="bold italic">Bingalo does not share your personal information with nonaffiliates so that they can market to you.</span></li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="question"><span class="has-text-weight-bold">Joint marketing</span></td>
                                                    <td class="answer">A formal agreement between non-affiliated financial companies that together market financial products or services to you.<ul>
                                                            <li><span class="bold italic">Bingalo does not jointly market.</span></li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="other_important_information_table">
                                            <tbody>
                                                <tr>
                                                    <th>Other Important Information</th>
                                                </tr>
                                                <tr>
                                                    <td>We may transfer personal information to other countries, for example, for customer service or for data processing.</td>
                                                </tr>
                                                <tr>
                                                    <td><span class="has-text-weight-bold" style="text-decoration:underline">California:</span> <!-- -->Effective January 1, 2020, the California Consumer Act of 2018 (CCPA) permits consumers who are California residents to ask businesses covered under the act about personal data it has collected about the consumer, submit an access or deletion request, and opt-out of the sale of personal information, if applicable. Some provisions do not apply to personal data collected, processed, shared, or disclosed by financial institutions pursuant to federal law such as the Gramm-Leach-Bliley Act and applicable Anti-Money Laundering requirements. Contact us if you have questions about our privacy statement, this consumer notice, or your personal data. If your Bingalo account has a California address, we will not share personal information we collect about you except to the extent permitted under California law.</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">If you have an unresolved privacy or data use concern that we have not addressed satisfactorily, please contact our third party dispute resolution provider (free of charge) at
                                <!-- --> <a href="https://feedback-form.truste.com/watchdog/request" target="_blank" rel="noreferrer">https://feedback-form.truste.com/watchdog/request</a>.
                            </p>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj"><span class="has-text-weight-bold">Additional Notice for California Residents:</span> <!-- -->Residents of California may exercise their right to access information that we have about you and your account under the California Consumer Privacy Act (“CCPA”). Please review our
                                <!-- --> <a href="/help_center/ca_privacy_notice">California Privacy Notice</a> <!-- -->and specific instructions.
                            </p>
                        </div>
                        <ul class="List-sc-1ekie5o-0 gqYahi static-revision">
                            <li class="ListItem-qv73n8-0 gdbGle">Updated November 12, 2021</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Updated June 29, 2020</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Updated May 25, 2018</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Updated November 25, 2017</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Updated February 28, 2017</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Updated September 12, 2016</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Updated May 18, 2016</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Updated July 16, 2015</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Updated December 21, 2014</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Established July 11, 2014</li>
                        </ul>
                        <div class="column"><a href="//privacy.truste.com/privacy-seal/validation?rid=c909a10f-2ad3-440d-960b-0565fc72f06b" target="_blank"><img style="border:none" src="//privacy-policy.truste.com/privacy-seal/seal?rid=c909a10f-2ad3-440d-960b-0565fc72f06b" alt="TRUSTe"></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>