<?php
$this->assign('title', 'Prohibited Items Policy');
?>
<style>
    .pagination ul li:before {
        background: #fff;
    }

    .form-control {
        padding-left: 2.375rem;
    }

    .form-control-feedback {
        position: absolute;
        z-index: 5;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 2.375rem;
        text-align: center;
        pointer-events: none;
        color: #aaa;
    }

    h3.panel-title {
        font-size: 15px;
    }

    .panel-body p {
        font-size: 15px;
    }

    .content h3 {
        font-size: 1.5em;
        margin-bottom: 0.6666em;
    }
</style>
<main class="site-content">
    <header class="section section-color-bar section-color-bar-blue">
        <div class="container">
            <h1 class="color-bar-title">Prohibited Items Policy</h1>
        </div>
    </header>
    <div class="bg-white mt-5">
        <div class="container-fluid">
            <div class="row justify-content-center content">
                <div class="col-12">
                    <div class="content" style="margin:32px 16px">
                        <div class="column">
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Bingalo prohibits the listing or sale of any item that is set forth in the list of prohibited items. If a Seller lists a prohibited item, it will be deemed to be a violation of our Terms of Service regardless of whether the Seller acted intentionally or not. If Bingalo determines that a listing is in violation or is otherwise inappropriate Bingalo may, at its discretion, remove the listing and cancel any related transactions up to and including termination or suspension.</p>
                        </div>
                        <div class="column">
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Bingalo may not be used in connection with any product, service, transaction or activity that in general:</p>
                            <ul class="List-sc-1ekie5o-0 gqYahi">
                                <li class="ListItem-qv73n8-0 gdbGle">Violates any law or government regulation, or promotes or facilitates such action by third parties;</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Is fraudulent, deceptive, unfair or predatory;</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Causes or threatens to damage Bingalo’s reputation;</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Violates the terms of any bank, card or electronic funds transfer network;</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Results in or creates a significant risk of chargebacks, penalties, damages or other harm or liability;</li>
                            </ul>
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">The list of Prohibited Items may be updated from time to time at Bingalo’s discretion. Examples of items that fall under certain categories may also be expanded to provide better communication and context to our users.</p>
                        </div>
                        <div class="column">
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Bingalo may not be used to sell any of the following products or services:</p>
                            <ul class="List-sc-1ekie5o-0 gqYahi">
                                <li class="ListItem-qv73n8-0 gdbGle">Anything illegal</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Drugs including:</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Illegal drugs or narcotics</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Prescription medicines or devices, pharmaceuticals, or behind the counter drugs</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Products, tools, or services specifically offered or intended to be used to create or use drugs</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Grow ingredients for drugs (such as seeds for cannabis plants)</li>
                                </ul>
                                <li class="ListItem-qv73n8-0 gdbGle">FDA restricted items such as food, homemade food, food supplements, vitamins, diet products, muscle enhancers, home remedies and homemade cosmetics (any ingestibles):</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Listings or items descriptions that offer miracle cures such as "cancer protection"</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Listings that make false health claims or misuse terms (such as "virus", "epidemic")</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Items that contain cannabidiol (CBD)</li>
                                </ul>
                                <li class="ListItem-qv73n8-0 gdbGle">Inflated prices where listings attempt to profit from tragedies and disasters (such as "paradise fire", "coronavirus outbreak" etc)</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Stolen goods:</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Note: If a purchased item is reported as stolen, a demand for return may be received from the victim or another party, and the item may be confiscated according to the regulations of the Code of Criminal Procedure (Act no. 131 of 1948).</li>
                                </ul>
                                <li class="ListItem-qv73n8-0 gdbGle">Counterfeit goods or goods infringing on a third party's intellectual property rights:</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Listings of non-brand, non-genuine, imitation, fake, or replica</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Items in violation of a copyright, including handmade, or other items with copyrighted characters, brand logos, etc.</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Note: For brand-name products, serial numbers or receipts must be available when listing the item.</li>
                                </ul>
                                <li class="ListItem-qv73n8-0 gdbGle">Weapons including:</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Firearms and firearm parts; including airsoft and bb guns</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Ammunition and ammunition components</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Knives, such as switchblades, hunting knives, pocket knives, butterfly knives, knives that are concealed or hidden within other objects, or are made of materials that cannot be detected with a metal detector.</li>
                                    <ul class="List-sc-1ekie5o-0 gqYahi">
                                        <li class="ListItem-qv73n8-0 gdbGle">Note: Kitchen cutlery and multitools that include knives are permitted.</li>
                                    </ul>
                                    <li class="ListItem-qv73n8-0 gdbGle">Explosives or military ordinance</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Self defense items, including military-grade items.</li>
                                </ul>
                                <li class="ListItem-qv73n8-0 gdbGle">Alcohol</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Tobacco products:</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Cigarettes</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">E-cigarettes, e-hookahs, or other vaporizing instruments that contain nicotine or are associated with tobacco or nicotine usage</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Vaporizing liquid without nicotine</li>
                                </ul>
                                <li class="ListItem-qv73n8-0 gdbGle">Items used for identity theft (such as account credentials, user login information, and/or account access codes)</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Any item that contains a computer virus, malware, or spyware</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Digital items - any items where the order is fulfilled electronically or requires a download . such as:</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Ebooks, PDF files, user generated content (UGC) or items for online games</li>
                                </ul>
                                <li class="ListItem-qv73n8-0 gdbGle">Items not in your possession:</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Dropshipping</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Advertisements or listings for objects being sought</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Items you do not currently have, or that are on order, or will be shipped at a future date</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Coupons to purchase products</li>
                                </ul>
                                <li class="ListItem-qv73n8-0 gdbGle">Items that are a safety hazard</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Restricted from shipping in the mail or other delivery services</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Products that have been recalled by the Consumer Product Safety Commission</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Note: Flammable or combustible liquids and any other items that require special mailing or handling instructions must be sent using the ship on your own option.</li>
                                </ul>
                                <li class="ListItem-qv73n8-0 gdbGle">Products designed to circumvent copyright protection techniques or otherwise facilitate the unlicensed use of copyright materials (such as "mod chips" to break the encryption on game computers to allow the playing of unlicensed game copies)</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Age restricted products or products that require a legal approval, seller/buyer registration or licenses to be sold</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Gambling, including using this service for raffles and mystery purchases, or selling lottery tickets and pull tabs.</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Regulated financial products and services such as:</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Bonds, securities, warranties and insurance</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Cryptocurrency</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Currency exchange</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Buying and selling gift cards or prepaid cards</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Selling precious metals, including gold, silver, and platinum sold in any medium.</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Selling loose (unset) precious gemstones such as rubies, sapphires, diamonds, and emeralds.</li>
                                </ul>
                                <li class="ListItem-qv73n8-0 gdbGle">All live animals</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Taxidermy, wet specimens or trafficked materials (such as ivory or shark fins)</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Note: Bones, claws, pelts, and teeth are permitted if they are not from protected species</li>
                                </ul>
                                <li class="ListItem-qv73n8-0 gdbGle">Humans, human body parts, organs, cells, blood, body fluids, and items that are soiled with human materials such as used underwear</li>
                                <li class="ListItem-qv73n8-0 gdbGle">Explicit items:</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Pornographic or obscene materials</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Sexually related items such as sex toys and fetish items</li>
                                </ul>
                                <li class="ListItem-qv73n8-0 gdbGle">Offensive listings:</li>
                                <ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Items, listings, photos or content that promote or glorify hatred, violence, racism or discrimination aren't allowed (determined at our discretion).</li>
                                </ul>
                            </ul>
                        </div>
                        <div class="column">
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Note: Any item may be removed at the discretion of the company.</p>
                        </div>
                        <div class="column">
                            <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Updated:</p>
                            <ul class="List-sc-1ekie5o-0 gqYahi static-revision">
                                <li class="ListItem-qv73n8-0 gdbGle">11/12/2021</li>
                                <li class="ListItem-qv73n8-0 gdbGle">6/09/2020</li>
                                <li class="ListItem-qv73n8-0 gdbGle">3/17/2020</li>
                                <li class="ListItem-qv73n8-0 gdbGle">12/17/2019</li>
                                <li class="ListItem-qv73n8-0 gdbGle">1/27/2018</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>