<?php
$this->assign('title', 'Safety Guidelines');
?>
<style>
    .pagination ul li:before {
        background: #fff;
    }

    .form-control {
        padding-left: 2.375rem;
    }

    .form-control-feedback {
        position: absolute;
        z-index: 5;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 2.375rem;
        text-align: center;
        pointer-events: none;
        color: #aaa;
    }

    h3.panel-title {
        font-size: 15px;
    }

    .panel-body p {
        font-size: 15px;
    }

    .content h3 {
        font-size: 1.5em;
        margin-bottom: 0.6666em;
    }
</style>
<main class="site-content">
    <header class="section section-color-bar section-color-bar-blue">
        <div class="container">
            <h1 class="color-bar-title">Safety Guidelines</h1>
        </div>
    </header>
    <div class="bg-white mt-5">
        <div class="container-fluid">
            <div class="row justify-content-center content">
                <div class="col-12">
                    <div class="column">
                        <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">We strive to provide a safe, secure, and fun marketplace for buyers and sellers. If a transaction goes sour, someone is attempting to deceive others, or if you notice any
                            Prohibited Items or
                            Prohibited Conduct, just send us a message via the in-app Help Center and we’d be happy to help.
                        </p>
                    </div>
                    <div class="column">
                        <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Online safety:</h3>
                        <ul class="List-sc-1ekie5o-0 gqYahi">
                            <li class="ListItem-qv73n8-0 gdbGle">Don’t share your personal contact information, such as phone number or email address with anyone. We can’t help with interactions outside of the Bingalo App or website</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Communicate with your buyer or seller through our in App messaging feature, to ensure there is a record of it.</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Use a credit card instead of a debit card for online purchases. Credit cards often have better security features and purchase protection, and your credit provider is more likely to offer reimbursements for fraudulent purchases.</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Come up with a great password and change it frequently. Passwords that are non-dictionary phrases and include numbers and special characters are the strongest. Do not use the same password for more than one account or repeat passwords.</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Never share your Bingalo account or password with anyone, including third-party websites. We will never ask you for your password.
                                <!-- -->
                            </li>
                            <li class="ListItem-qv73n8-0 gdbGle">Do not click on emailed links and login to your Bingalo account from clicking a link. The most secure way to log into your Bingalo account is by going directly to Bingalo.com and logging in from there, or accessing through the App.</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Make sure to download our official App from the Apple App Store or the Google Play App Store, where the developer of the App is Bingalo Inc. Developers other than Bingalo Inc are not verified.</li>
                            <li class="ListItem-qv73n8-0 gdbGle">If you allow your children to use your account, closely monitor their purchases and interactions with sellers.</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Before buying online, read the listing and look at the photo(s) carefully. Read the reviews left for the seller. Ask the seller any questions before you make payment.</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Report any questionable or prohibited items through the Report link in the item listing page.</li>
                        </ul>
                    </div>
                    <div class="column">
                        <h3 style="padding-top:20px;padding-bottom:5px" class="Text-uqn6ov-0 Text__H3-uqn6ov-4 brawNo WUqVH">Financial safety:</h3>
                        <ul class="List-sc-1ekie5o-0 gqYahi">
                            <li class="ListItem-qv73n8-0 gdbGle">Never give out your personal financial (payment) information or contact information to another user, especially if they are asking for it outside of our App or site.</li>
                            <li class="ListItem-qv73n8-0 gdbGle">To protect you from becoming a victim of fraud, transactions must be done through the App or site. We do not support or protect transactions where payment is transmitted outside of the App or our site.</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Avoid transferring money to someone you do not know, for scenarios that seems “too good to be true” or to help resolve an “urgent” situation supposedly involving a friend or loved one. This may involve a request to rate an item you have not yet received.</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Routinely check your financial statements for questionable activity. Contact Bingalo immediately if you see any unauthorized charges.</li>
                            <li class="ListItem-qv73n8-0 gdbGle">Thoroughly inspect the item upon delivery. Buyers have 3 days from delivery to request a return. Once the
                                <!-- --> <span class="has-text-weight-bold">3</span> days have passed or the buyer has rated the transaction,
                                <!-- --> <span class="has-text-weight-bold">all sales are final</span>.<ul class="List-sc-1ekie5o-0 gqYahi">
                                    <li class="ListItem-qv73n8-0 gdbGle">Test the item prior to rating your seller, especially for expensive items or electronics.</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Some phone carriers are able to check if a phone has been identified as stolen.</li>
                                    <li class="ListItem-qv73n8-0 gdbGle">Luxury brands often have trademark information available online to verify authenticity or use Bingalo Authenticate if possible.</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="column">
                        <ul class="List-sc-1ekie5o-0 gqYahi">
                            <li class="ListItem-qv73n8-0 gdbGle">If you suspect that you may be a victim of fraud or other criminal activity, please call us at (888)325-2168 or report your suspicions to law enforcement.</li>
                        </ul>
                    </div>
                    <div class="column">
                        <p class="Text-uqn6ov-0 Text__T2-uqn6ov-10 brawNo cBSKDj">Updated:</p>
                        <ul class="List-sc-1ekie5o-0 gqYahi static-revision">
                            <li class="ListItem-qv73n8-0 gdbGle">6/29/2020</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>