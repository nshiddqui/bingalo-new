<?php
$this->assign('title', 'Marketing Guidelines');
?>
<style>
    .pagination ul li:before {
        background: #fff;
    }

    .form-control {
        padding-left: 2.375rem;
    }

    .form-control-feedback {
        position: absolute;
        z-index: 5;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 2.375rem;
        text-align: center;
        pointer-events: none;
        color: #aaa;
    }

    h3.panel-title {
        font-size: 15px;
    }

    .panel-body p {
        font-size: 15px;
    }

    .content h3 {
        font-size: 1.5em;
        margin-bottom: 0.6666em;
    }
</style>
<main class="site-content">
    <header class="section section-color-bar section-color-bar-blue">
        <div class="container">
            <h1 class="color-bar-title">Marketing Guidelines</h1>
        </div>
    </header>
    <div class="bg-white mt-5">
        <div class="container-fluid">
            <div class="row justify-content-center content">
                <div class="col-12">
                    <div data-testid="Article-Content" class="content">
                        <p>
                            We work hard to keep the marketplace safe. But we can’t do it alone. We need your help to keep selling and buying safe for everyone.
                        </p>

                        <p>
                            <b>Keep it neighborly.</b><br>
                        </p>
                        <p>
                            Buyers and sellers on Mercari are real people. They are not retailers and most are not professionals. Treat them like you would your neighbor.
                        </p>
                        <p>
                            Accurately represent yourself and your stuff to them. Only list items you intend to sell. At a price you intend to sell it at. If you negotiate, do so in good faith.
                        </p>
                        <p>
                            <u>Prohibited Conduct:</u><br>
                        </p>
                        <ul style="margin-left: 30px">
                            <li>Sending chat messages intended to threaten, degrade, or harass another user</li>
                            <li>Listing items associated with hate groups or glorifying violence</li>
                            <li>Sending messages or listing items with racial slurs</li>
                            <li>Listings or messages that provide false information</li>
                        </ul>

                        <p>Check out our full list of <a href="/prohibited_conduct_policy/">Prohibited Conduct</a> for more details. </p>

                        <p>
                            <b>Keep it safe. </b><br>
                        </p>
                        <p>
                            At Mercari, all items ship. There is no meeting up with strangers in strange places. To keep you safe, we ask that all transactions take place in the app. We can’t protect you if your sale or purchase takes place offline.
                        </p>

                        <p>
                            <u>Prohibited Conduct:</u>
                        </p>
                        <ul style="margin-left: 30px">
                            <li> Attempting to take a transaction off the Mercari app (meeting in real life) </li>
                            <li>Trades or partial trades (trading items so neither user pays or only partially pays) </li>
                        </ul>
                        <p>Check out our full list of <a href="/prohibited_conduct_policy/">Prohibited Conduct</a> for more details. </p>

                        <p>
                            <b>Keep it legal.</b><br>
                        </p>

                        <p>
                            We follow the law. So should you. All listed items must be legal to sell and legal to ship — under federal, state and local law. If you don’t own it, don’t sell it.
                        </p>
                        <p>
                            We are also required to comply with the regulations of the payment processors and other partners that we work with. Check out our full list of <a href="https://www.mercari.com/prohibited_items/">Prohibited Items</a> for more details.
                        </p>

                        <hr>

                        <p>See something that doesn’t feel right? We’re here for you. Reach out to us through the Help Center. </p>

                        <p>
                            Need to cancel or make a return? <br>
                            <a href="/help_center/article/251/">Cancellation Policy</a> <br>
                            <a href="https://www.mercari.com/us/help_center/refunds_and_returns">Return Policy</a> <br>
                        </p>
                        <p>
                            For more information, check out <a href="/help_center/getting_started">How it works</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>