<?php
$this->assign('footer', 'no-footer');
$this->assign('og-title',  $product->product_title);
$this->assign('og-image', $product['post_images'][0]->image_url);
?>
<?= $this->Html->css('https://pro.fontawesome.com/releases/v5.10.0/css/all.css', ['block' => true]) ?>
<?= $this->Html->script('https://kit.fontawesome.com/a076d05399.js', ['block' => true]) ?>
<?= $this->Html->script('https://js.braintreegateway.com/web/3.85.2/js/client.min.js', ['block' => true]) ?>
<?= $this->Html->script('https://js.braintreegateway.com/web/3.85.2/js/apple-pay.min.js', ['block' => true]) ?>
<?= $this->Html->script('https://js.braintreegateway.com/web/3.85.2/js/google-payment.min.js', ['block' => true]) ?>
<?= $this->Html->script('https://pay.google.com/gp/p/js/pay.js', ['block' => true]) ?>
<style>
    .StripeElement {
        box-sizing: border-box;

        padding: 12px 14px;

        height: 50px;

        border: 1px solid transparent;
        background-color: white;
        border-radius: 4px;


        box-shadow: 0 2px 6px 0 #e6ebf1;
        -webkit-transition: box-shadow 150ms ease;
        transition: box-shadow 160ms;
    }

    .StripeElement--focus {
        box-shadow: 0 2px 4px 0 #cff7df;
    }

    .StripeElement--invalid {
        border-color: #fa755a;
    }

    .StripeElement--webkit-autofill {
        background-color: #fefde5 !important;
    }
</style>
<div class="w3-content">
    <?php foreach ($product['post_images'] as $key => $post_images) { ?>
        <?= $this->Html->image($post_images->image_url, ['class' => 'mySlides', 'data-source' => 'pan-' . $key, 'style' => 'width:100%;object-fit:cover;border-radius: 5px;height:300px;' . ($key == 0 ? '' : 'display:none')]) ?>
    <?php } ?>
    <div class="navigation" style="position: absolute; top: 275px;padding: 0 20px;right: 10px;">
        <?php foreach ($product['post_images'] as $key => $post_images) { ?>
            <i style="height: 7px;width: 7px;background: #fff;border-radius: 100%;display: inline-block;" id="pan-<?= $key ?>"></i>
        <?php } ?>
    </div>
    <div class="man">
        <span><a href="/profile/<?= $product['user']->id ?>"><?= $product['user']->full_name ?></a> <?php if ($product['user']->profile_verified == '1') { ?><?= $this->Html->image('profile_verified.png', ['style' => 'height: 16px;margin-top: -2px;']) ?><?php } ?></span>
        <?= $this->Html->image($product['user']->image, ['style' => 'height:52px;width:52px;border-radius:100%;object-fit: cover;', 'url' => '/profile/' . $product['user']->id]) ?>
    </div>

    <div class="w3-section">
        <?php foreach ($product['post_images'] as $key => $post_images) { ?>
            <div class="s4">
                <?= $this->Html->image($post_images->image_url, ['class' => 'demo w3-opacity w3-hover-opacity-off', 'style' => 'width:100%;cursor:pointer', 'onclick' => 'currentDiv(' . ($key + 1) . ')']) ?>
            </div>
        <?php } ?>
    </div>

    <div class="img-icon">
        <ul>
            <li>
                <?= $this->Html->image(((isset($product['favourite']) && !empty($product['favourite'])) ? 'mobile/heart_fill.png' : 'mobile/heart.svg'), ['url' => ($authUser ? "javascript:markFavourite({$product->id});" : 'javascript:void(0)'), 'login' => '1', 'class' => "img-favourite-" . $product->id, 'style' => ((isset($product['favourite']) && !empty($product['favourite'])) ? 'width: 25px;height: 25px;' : '')]) ?>
            </li>

            <li>
                <a href="javascript:navigator.share({ title: document.title, text: '<?= $product->product_title ?>', url: window.location.href.replace(/m.bingalo.com/g, 'bingalo.com') })">
                    <i class="fa fa-upload "></i>
                </a>
            </li>

            <li class="more-dropdown-btn">
                <a>
                    <i class="fas fa-ellipsis-v"></i>
                </a>
            </li>
        </ul>
    </div>

</div>
<?php if ($authUser) { ?>
    <div class="more-btn-menu product-menu account-popup popup-box hide-popup-box">
        <ul>
            <li class="available-on-mobile"><a href="#"><?= __('Edit Product') ?></a></li>
            <?php if (empty($product['bump_product'])) { ?>
                <li class="bump-btn"><a href="#"><?= __('Bump your item') ?></a></li>
            <?php } ?>
            <li class="available-on-mobile"><a href="#"><?= __('Delete Product') ?></a></li>
            <li class="close-upload-dropdown"><a href="#" class="danger"><?= __('Cancel') ?></a></li>
        </ul>
    </div>
<?php } else { ?>
    <div class="more-btn-menu product-menu account-popup popup-box hide-popup-box">
        <ul>
            <li class="available-on-mobile"><a href="#"><?= __('Report Posting') ?></a></li>
            <li class="available-on-mobile"><a href="#"><?= __('Flag Seller') ?></a></li>
            <li class="close-more-dropdown"><a href="#" class="danger"><?= __('Cancel') ?></a></li>
        </ul>
    </div>
<?php } ?>
<div class="iphone-content">
    <p class="heading">
        <?= $product->product_title ?>
    </p>
    <p>

    </p>
    <h1 class="count">
        ₪<?= $product->product_price ?>
    </h1>

    <p class="fc grey">
        <span><?= __('Posted') ?>: </span><?= $this->Time->timeAgoInWords($product->created_at, ['accuracy' => array('month' => 'month'), 'end' => '1 year']) ?>
    </p>

    <p class="color">
        <?= $product->product_description ?>
    </p>

    <p class="fc">
        <span><?= __('Specifications') ?>: </span> <?= $product->product_specifications ?>
    </p>

    <p class="fc">
        <span><?= __('Category') ?>: </span><?= $product->product_category ?>
    </p>

    <p class="fc">
        <span><?= __('Condition') ?>: </span><?= str_replace('#', ' - ', $product->product_condition) ?>
    </p>
</div>
<?php if ($authUser && $authUser['id'] == $product->uploaded_by_user_id && empty($product['bump_product'])) { ?>
    <div class="bump">
        <div class="bump-item">
            <span class="close-btn">
                <?= $this->Html->image('mobile/btn_cross.png', ['style' => 'width: 30px;']) ?>
            </span>

            <div class="bump-content">
                <h2><?= __('Bump This Item') ?></h2>
                <p><?= __('Bumping your item in the feed Will help more buyers see it.') ?></p>
            </div>
            <button class="bump-btn">
                <?= $this->Html->image('mobile/btn_arrow_up.png', ['style' => 'height: 15px; margin: 3px;']) ?>
                <?= __('Bump!') ?>
            </button>
        </div>
    </div>
<?php } ?>
<style>
    .carousel-12 .slide,
    .carousel-12 .slide img {
        width: 96%;
        aspect-ratio: 261/268;
    }
</style>
<?= $this->Html->css('https://npmcdn.com/flickity@2/dist/flickity.css') ?>
<?= $this->Html->script('https://npmcdn.com/flickity@2/dist/flickity.pkgd.js') ?>
<?php if ($bottomBanner->count()) { ?>
    <hr style="margin: 10px 4%;">
    <div class="carousel-12" style="text-align: center">
        <?php foreach ($bottomBanner as $banner) { ?>
            <div class="slide"><a href="<?= $banner->link ?>" target="_BLANK" title="<?= $banner->title ?>"><?= $this->Html->image($banner->image, ['style' => 'border-radius:5px', 'lazy' => false]) ?></a></div>
        <?php } ?>
    </div>
    <hr style="margin: -5px 10px 4% 4%;">
<?php } ?>
<script>
    $('.carousel-12').flickity({
        autoPlay: true,
        accessibility: true,
        adaptiveHeight: true,
        draggable: '>1',
        pageDots: false,
        prevNextButtons: false,

    });
</script>
<div class="map-section ">
    <div class="map">
        <div class="map-link">
            <div class="map" id="map" style="height:260px;width:100%;"></div>
        </div>
        <p>
            <?= __('Map is approximate to keep the sellers location private.') ?>
        </p>
    </div>
    <?php if (($authUser && $authUser['id'] == $product->uploaded_by_user_id) === false) { ?>
        <div class="map-btn" style="position: fixed; bottom: 30px; top: unset;z-index:1">
            <button onclick="window.location.href='<?= $isAndroid ? '#' : 'https://apps.apple.com/us/app/bingalo/id1351229487' ?>'" class="available-on-mobile"><?= __('Ask a Question') ?></button>
            <button onclick="window.location.href='<?= $isAndroid ? '#' : 'https://apps.apple.com/us/app/bingalo/id1351229487' ?>'" class="color-change available-on-mobile"><?= __('Make an Offer') ?></button>
        </div>
    <?php } ?>
</div>
<?php if (!empty($topListings)) { ?>
    <div class="alex-offer">
        <h3><?= $product['user']->full_name ?>'s <?= __('Offers') ?></h3>
        <div class="images">
            <?php foreach ($topListings as $bump_products) { ?>
                <div class="square" onclick="window.location.href='/product/<?= $bump_products['id'] ?>'" style="background-image: url(<?= $bump_products['post_image']->image_url ?>);border-radius: 10px;background-size: cover; background-position: center;">
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>
<br>
<div class="menu phone-menu">
    <div class="button">
        <?php if ($isAndroid) { ?>
            <button class="available-on-mobile"><?= __('Sell Your Stuff') ?></button>
        <?php } else { ?>
            <button class="available-on-mobile"><?= __('Sell Your Stuff') ?></button>
        <?php } ?>
    </div>

    <ul class="have-border">
        <li>
            <a href="https://page.bingalo.com/about-us/">
                <?= $this->Html->image('mobile/information.svg') ?>
                <span><?= __('About Bingalo') ?></span>
            </a>
        </li>

        <li>
            <a href="https://page.bingalo.com/contact-us/">
                <?= $this->Html->image('mobile/question (1).svg') ?>
                <span><?= __('Help center') ?></span>
            </a>
        </li>

        <li>
            <a href="mailto:hello@bingalo.com">
                <?= $this->Html->image('megaphone.svg') ?>
                <span><?= __('Advertise on Bingalo') ?></span>
            </a>
        </li>
    </ul>
    <?php if ($authUser === false) { ?>
        <ul class="have-border account-form">
            <li>
                <a class="signup-btn" style="background: inherit;">
                    <?= $this->Html->image('add-user.svg') ?>
                    <span><?= __('Sign up') ?></span>
                </a>
            </li>

            <li>
                <a class="login-btn">
                    <?= $this->Html->image('mobile/enter.svg') ?>
                    <span><?= __('Log in') ?></span>
                </a>
            </li>
        </ul>
    <?php } ?>

    <div class="app-link">
        <h2><?= __('Download the Bingalo app') ?>.</h2>

        <?php if ($isAndroid) { ?>
            <?= $this->Html->image('mobile/download-on-play-store-png.png', ['style' => 'height:50px']) ?>
        <?php } else { ?>
            <?= $this->Html->image('mobile/download-on-app-store-png.png', ['style' => 'height:50px', 'url' => 'https://apps.apple.com/us/app/bingalo/id1351229487']) ?>
        <?php } ?>
    </div>
</div>
<div class="account-popup popup-box hide-popup-box sell-item" style="overflow: hidden;">
    <div class="popup-header">
        <?= $this->Html->image('mobile/arrow.png') ?>
        <h4><?= __('Bump an Item, Sell it faster') ?></h4>
        <?= $this->Html->image('mobile/Group 116.png') ?>
    </div>
    <div style="height: 100%;overflow: hidden auto;padding-bottom: 85px;">
        <div class="row-content">

            <div class="row-section">
                <div class="new-items">
                    <div class="new-single-items" style="border: 2px solid #0872BB; display: grid;">
                        <div class="new-item-image">
                            <div class="text-heart">
                                <p><?= __('Featured') ?></p>
                                <span></span>
                            </div>
                            <div class="laptop-pic">
                                <?= $this->Html->image($product['post_images'][0]->image_url, ['style' => 'height:125px']) ?>
                            </div>

                        </div>
                        <div class="new-item-content">
                            <h3>
                                <?= $product->product_title ?>
                            </h3>
                            <span>
                                ₪<?= $product->product_price ?>
                            </span>
                            <p>
                                <?= $this->Html->image('mobile/bump_description.png', ['style' => 'width:80%;margin-top:5px']) ?>
                            </p>
                        </div>
                    </div>

                    <div class="right-section">
                        <div class="single-item-image">
                            <div class="img-section">
                                <?= $this->Html->image('mobile/Rectangle 754.svg') ?>
                                <a href="#">
                                    <div></div>
                                </a>
                                <a href="#">
                                    <div class="sec"></div>
                                </a>
                            </div>
                            <div class="img-section">
                                <?= $this->Html->image('mobile/Rectangle 754.svg') ?>
                                <a href="#">
                                    <div></div>
                                </a>
                                <a href="#">
                                    <div class="sec"></div>
                                </a>
                            </div>
                            <div class="img-section">
                                <?= $this->Html->image('mobile/Rectangle 754.svg') ?>
                                <a href="#">
                                    <div></div>
                                </a>
                                <a href="#">
                                    <div class="sec"></div>
                                </a>
                            </div>
                            <div class="img-section">
                                <?= $this->Html->image('mobile/Rectangle 754.svg') ?>
                                <a href="#">
                                    <div></div>
                                </a>
                                <a href="#">
                                    <div class="sec"></div>
                                </a>
                            </div>

                        </div>

                        <div class="new-item-content">
                            <h3 style="text-align: left;">
                                <?= __('Want to sell it quickly?') ?>
                            </h3>
                            <p style="text-align: left;">
                                <?= __('Highlight your item so that more people see it. Many More!') ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="slider">
                    <div class="slider-box">
                        <span>3 days</span>
                        <h5>35₪</h5>
                    </div>

                    <div class="slider-box">
                        <span>7 days</span>
                        <h5>35₪</h5>
                        <span>Save 46%</span>
                    </div>

                    <div class="slider-box">
                        <span>15 days</span>
                        <h5>50₪</h5>
                        <span>Save 62%</span>
                    </div>
                </div> -->
        <div class="slider customer-logos">
            <div class="slide single-item slider-box">
                <span class="price-days"><?= __('3 days') ?></span>
                <h5 data-price="20">₪20</h5>
            </div>

            <div class="slide single-item slider-box">
                <span class="price-days"><?= __('7 days') ?></span>
                <h5 data-price="35">₪35</h5>
                <span class="save-price">Save 46%</span>
            </div>

            <div class="slide single-item slider-box">
                <span class="price-days"><?= __('15 days') ?></span>
                <h5 data-price="50">₪50</h5>
                <span class="save-price">Save 62%</span>
            </div>
        </div>

        <div class="span">
            <span style="font-weight:bold;font-family: AvenirNextBold;" class="promot-btn"><?= __('How does promoting work?') ?></span>
        </div>
        <div class="button">
            <button class="package-btn">
                <?= __('Next') ?>
            </button>
        </div>
    </div>
</div>

<div class="account-popup popup-box hide-popup-box promoting-popup" style="overflow: hidden;">
    <div class="promot-work">
        <?= $this->Html->image('mobile/arrow.png') ?>
        <h2><?= __('How does promoting work?') ?></h2>
    </div>
    <div style="height: 100%;overflow: hidden auto;padding-bottom: 85px;">
        <div class="promot-content">
            <div class="promot-single-item">
                <div class="promot-pic">
                    <?= $this->Html->image('mobile/demo1.svg') ?>
                </div>
                <div class="promot-text">
                    <h3><?= __('Get more views') ?></h3>
                    <p><?= __('Promoted posts appear among the first spots buyer see and get an average of 14x views.') ?></p>
                </div>
            </div>
            <div class="promot-single-item">
                <div class="promot-pic">
                    <?= $this->Html->image('mobile/demo2.svg') ?>
                </div>
                <div class="promot-text">
                    <h3><?= __('Add a promoted spot') ?></h3>
                    <p><?= __('Your item appears a new post in Search results as well as in the Promoted spot.') ?></p>
                </div>
            </div>
            <div class="promot-single-item">
                <div class="promot-pic">
                    <?= $this->Html->image('mobile/demo3.svg') ?>
                </div>
                <div class="promot-text">
                    <h3><?= __('Promoted spots are shared') ?></h3>
                    <p><?= __('Don\'t worry if you don\'t see your Items at the top of the feed. Spots Are shared between sellers.') ?></p>
                </div>
            </div>
        </div>
        <!-- <div class="promot-btn">
        <button>Next</button>
    </div> -->
    </div>
</div>

<div class="account-popup popup-box hide-popup-box checkout-popup" style="overflow: hidden;">
    <div class="promot-work" style="display: grid; grid-template-columns: auto 1fr; padding: 14px; align-items: center; grid-gap: 0; border-bottom: 1px solid #E1E1E1;">
        <?= $this->Html->image('mobile/arrow.png') ?>
        <h2 class="text-c"><?= __('Checkout') ?></h2>
    </div>
    <div style="height: 100%;overflow: hidden auto;padding-bottom: 85px;">
        <div class="menu-pay">
            <ul>
                <li>
                    <a href="#" class="grid-section">
                        <div class="left-section">
                            <h5>7 days</h5>
                            <p>Save 46%</p>
                        </div>
                        <h1>35₪</h1>
                    </a>
                </li>
                <li class="current-payment" data-payment="paypal">
                    <h6><?= __('Current Payment Method') ?></h6>
                    <a href="#">
                        <div class="pay" style="background: transparent;">
                            <?= $this->Html->image('mobile/paypal.png') ?>
                        </div>
                        <h3><?= __('Paypal') ?></h3>
                        <span class="payment-mode"><?= __('Change') ?></span>
                    </a>
                </li>
            </ul>
        </div>

        <div class="item-pay">
            <div class="single-item-pay">
                <h3><?= __('Item Price') ?></h3>
                <span class="price-total-pay">₪399.99</span>
            </div>
            <div class="single-item-pay">
                <h3><?= __('Service Fee') ?></h3>
                <span>₪10</span>
            </div>
            <div class="single-item-pay">
                <h3 class="ft-bold"><?= __('You Pay') ?></h3>
                <span class="price-total-pay-amount">₪399.99</span>
            </div>
        </div>

        <div class="purchase-btn">
            <button><?= __('Confirm Purchase') ?></button>
            <p><?= __('By Tapping “Confirm Purchase”. You agree our') ?></p>
            <a href="#"> <?= __('Terms &amp; Conditions.') ?></a>
        </div>
    </div>
</div>
<div class="account-popup popup-box hide-popup-box payment-popup" style="overflow: hidden;">
    <div class="promot-work" style="display: grid; grid-template-columns: auto 1fr; padding: 14px; align-items: center; grid-gap: 0; border-bottom: 1px solid #E1E1E1;">
        <?= $this->Html->image('mobile/arrow.png') ?>
        <h2 class="text-c"><?= __('Add Payment Method') ?></h2>
    </div>
    <div style="height: 100%;overflow: hidden auto;padding-bottom: 85px;">
        <div class="menu-pay">
            <ul>
                <!-- <li>
                    <a href="javascript:changePayment('stripe')">
                        <?= $this->Html->image('mobile/credit-card.svg') ?>
                        <h3><?= __('Credit or debit card') ?></h3>
                        <?= $this->Html->image('arrow-right-pay.svg') ?>
                    </a>
                </li> -->
                <li>
                    <a href="javascript:changePayment('paypal')">
                        <div class="pay white">
                            <?= $this->Html->image('mobile/paypal.png') ?>
                        </div>
                        <h3><?= __('Paypal') ?></h3>
                        <?= $this->Html->image('arrow-right-pay.svg') ?>
                    </a>
                </li>
                <!-- <li>
                        <a href="javascript:changePayment('gpay')">
                            <div class="pay white">
                                <?= $this->Html->image('mobile/gpay.png') ?>
                            </div>
                            <h3><?= __('Google Pay') ?></h3>
                            <?= $this->Html->image('arrow-right-pay.svg') ?>
                        </a>
                    </li> -->
                <?php if (!$isAndroid) { ?>
                    <!-- <li>
                        <a href="javascript:changePayment('apple')">
                            <div class="pay black">
                                <?= $this->Html->image('mobile/apple-logo (2).png') ?>
                            </div>
                            <h3><?= __('Apple Pay') ?></h3>
                            <?= $this->Html->image('arrow-right-pay.svg') ?>
                        </a>
                    </li> -->
                <?php } ?>
            </ul>
        </div>
    </div>
</div>

<div class="account-popup popup-box hide-popup-box credit-card-popup" style="overflow: hidden;">
    <div class="promot-work" style="display: grid; grid-template-columns: auto 1fr; padding: 14px; align-items: center; grid-gap: 0; border-bottom: 1px solid #E1E1E1;">
        <?= $this->Html->image('mobile/arrow.png') ?>
        <h2 class="text-c"><?= __('Enter Card Details') ?> </h2>
    </div>
    <div style="height: 100%;overflow: hidden auto;padding-bottom: 85px;">
        <div class="menu-pay">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-default credit-card-box">
                        <div class="panel-body" style="text-align: center;">

                            <?= $this->Form->create(null, ['url' => '/payments/stripe/', 'id' => 'payment-form']) ?>
                            <?= $this->Form->hidden('product_id', ['value' => $product->id, 'id' => 'product-id']) ?>
                            <?= $this->Form->hidden('amount', ['id' => 'paid-amount']) ?>
                            <div class="form-row">
                                <div id="card-element">
                                    <!-- A Stripe Element will be inserted here. -->
                                </div>

                                <!-- Used to display form errors. -->
                                <div id="card-errors" role="alert"></div>
                            </div>
                            <button style="margin-top: 20px;box-shadow: 0px 3px 20px #00000057; border-radius: 17px; width: 193px; height: 34px; border: none; outline: none; background-color: #0872BB; color: #ffffff; margin-bottom: 22px;"><?= __('Submit Payment') ?></button>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
    // Create a Stripe client.
    var stripe = Stripe('pk_test_51J8PosLTp72IeRov83jjcufswcn7C2O07lVhW20pohkHBbUkGFxDJPZ16gBfjckRTMQQuTrHAtD68MJoxTGMA8oU00jvQJBHgv');

    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {
        hidePostalCode: true,
        style: style
    });

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });

    // Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {
        event.preventDefault();

        stripe.createToken(card).then(function(result) {
            if (result.error) {
                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
            }
        });
    });

    // Submit the form with the token ID.
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        // Submit the form
        form.submit();
    }
</script>
<script>
    if (window.ApplePaySession && ApplePaySession.supportsVersion(3) && ApplePaySession.canMakePayments()) {
        function appleCheckout() {
            braintree.client.create({
                authorization: 'sandbox_4x5zhgmj_8qvhbp66n65srzv5'
            }, function(clientErr, clientInstance) {
                if (clientErr) {
                    alert('Error creating client:');
                    return;
                }

                braintree.applePay.create({
                    client: clientInstance
                }, function(applePayErr, applePayInstance) {
                    if (applePayErr) {
                        alert('Error creating applePayInstance:');
                        return;
                    }
                    var paymentRequest = applePayInstance.createPaymentRequest({
                        total: {
                            label: 'Bingalo Bump Product Subscription',
                            amount: $('#paid-amount').val()
                        },
                    });

                    var session = new ApplePaySession(3, paymentRequest);
                    // session.onvalidatemerchant = function(event) {
                    //     applePayInstance.performValidation({
                    //         validationURL: event.validationURL,
                    //         displayName: 'Bingalo'
                    //     }, function(err, merchantSession) {
                    //         if (err) {
                    //             alert('Unable to laod apple pay');
                    //             return;
                    //         }
                    //         session.completeMerchantValidation(merchantSession);
                    //     });
                    // };
                    session.onpaymentauthorized = function(event) {

                        applePayInstance.tokenize({
                            token: event.payment.token
                        }, function(tokenizeErr, payload) {
                            if (tokenizeErr) {
                                alert('Error tokenizing Apple Pay:');
                                session.completePayment(ApplePaySession.STATUS_FAILURE);
                                return;
                            }
                            window.location.href = '/apple/?amount=' + $('#paid-amount').val() + '&payerID=' + payload.nonce + '&productId=<?= $product->id ?>';
                            // After you have transacted with the payload.nonce,
                            // call `completePayment` to dismiss the Apple Pay sheet.
                            session.completePayment(ApplePaySession.STATUS_SUCCESS);
                        });
                    };

                    session.begin();

                    // Set up your Apple Pay button here
                });
            });
        }
    } else {
        $('a[href="javascript:changePayment(\'apple\')"]').hide();
    }
</script>
<script>
    function gpayCheckout() {
        var paymentsClient = new google.payments.api.PaymentsClient({
            environment: 'TEST' // Or 'PRODUCTION'
        });
        braintree.client.create({
            authorization: 'sandbox_4x5zhgmj_8qvhbp66n65srzv5'
        }, function(clientErr, clientInstance) {
            braintree.googlePayment.create({
                client: clientInstance,
                googlePayVersion: 2,
                // googleMerchantId: 'merchant-id-from-google' // Optional in sandbox; if set in sandbox, this value must be a valid production Google Merchant ID
            }, function(googlePaymentErr, googlePaymentInstance) {
                paymentsClient.isReadyToPay({
                    // see https://developers.google.com/pay/api/web/reference/object#IsReadyToPayRequest
                    apiVersion: 2,
                    apiVersionMinor: 0,
                    allowedPaymentMethods: googlePaymentInstance.createPaymentDataRequest().allowedPaymentMethods,
                    existingPaymentMethodRequired: true // Optional
                }).then(function(response) {
                    if (response.result) {
                        var paymentDataRequest = googlePaymentInstance.createPaymentDataRequest({
                            transactionInfo: {
                                currencyCode: 'USD',
                                totalPriceStatus: 'FINAL',
                                totalPrice: $('#paid-amount').val()
                            }
                        });

                        // We recommend collecting billing address information, at minimum
                        // billing postal code, and passing that billing postal code with all
                        // Google Pay card transactions as a best practice.
                        // See all available options at https://developers.google.com/pay/api/web/reference/object
                        var cardPaymentMethod = paymentDataRequest.allowedPaymentMethods[0];
                        cardPaymentMethod.parameters.billingAddressRequired = true;
                        cardPaymentMethod.parameters.billingAddressParameters = {
                            format: 'FULL',
                            phoneNumberRequired: true
                        };

                        paymentsClient.loadPaymentData(paymentDataRequest).then(function(paymentData) {
                            googlePaymentInstance.parseResponse(paymentData, function(err, result) {
                                if (err) {
                                    // Handle parsing error
                                }

                                window.location.href = '/gpay/?amount=' + $('#paid-amount').val() + '&payerID=' + result.nonce + '&productId=<?= $product->id ?>';
                                // Send result.nonce to your server
                                // result.type may be either "AndroidPayCard" or "PayPalAccount", and
                                // paymentData will contain the billingAddress for card payments
                            });
                        }).catch(function(err) {
                            // Handle errors
                        });
                    }
                }).catch(function(err) {
                    // Handle errors
                });
            });

            // Set up other Braintree components
        });
    }
</script>
<script>
    function initialize() {
        const center = {
            lat: <?= $product->product_lat ?>,
            lng: <?= $product->product_lng ?>
        };
        // Create the map.
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 13,
            center: center,
            maxZoom: 14,
            mapTypeId: "terrain",
        });
        // Add the circle for this city to the map.
        const cityCircle = new google.maps.Circle({
            strokeColor: "#88C98F",
            strokeOpacity: 0.7,
            strokeWeight: 2,
            fillColor: "#88C98F",
            fillOpacity: 0.7,
            map,
            center: center,
            radius: 1000,
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>