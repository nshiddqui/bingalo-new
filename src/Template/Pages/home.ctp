<?= $this->Html->css('https://cdn.jsdelivr.net/npm/flickity@latest/dist/flickity.min.css') ?>
<?= $this->Html->script('https://cdn.jsdelivr.net/npm/flickity@2.2.1/dist/flickity.pkgd.min.js') ?>
<div class="home-banner">
    <div class="banner-image mobile-banner-cursoul">
        <?php if ($topBanner->count()) { ?>
            <?php foreach ($topBanner as $banner) { ?>
                <?= $this->Html->image($banner->image, ['onclick' => "window.open('{$banner->link}','_BLANK')", 'onload' => 'renderImage()']) ?>
            <?php } ?>
        <?php } else { ?>
            <?= $this->Html->image('mobile/banner.svg', ['onclick' => "window.open('https://google.com','_BLANK')", 'onload' => 'renderImage()']) ?>
            <?= $this->Html->image('mobile/banner.svg', ['onclick' => "window.open('https://google.com','_BLANK')", 'onload' => 'renderImage()']) ?>
        <?php } ?>
    </div>
</div>
<script>
    var totalImageCount = 0;

    function renderImage() {
        totalImageCount = totalImageCount + 1;
        if (totalImageCount >= <?= $topBanner->count() ?> || <?= $topBanner->count() ?> == 0) {
            var mobile_banner = $('.mobile-banner-cursoul').flickity({
                autoPlay: true,
                accessibility: true,
                adaptiveHeight: true,
                draggable: '>1',
                pageDots: false,
                prevNextButtons: false,
            });
        }

    }
</script>
<div id="kmDialog" style="line-height: 1.15;
     direction: ltr;
     text-align: left;
     text-size-adjust: 100%;
     -webkit-font-smoothing: antialiased;
     font-family: Arial,Helvetica,sans-serif;
     -webkit-tap-highlight-color: transparent;
     color: #002f34;
     position: fixed;
     top: 20px;
     left: 50%;
     transform: translateX(-50%);
     width: auto;
     animation: _1RXhN .3s ease;
     z-index: 9999;
     min-width: 115px;
     display: none;">
    <button style="direction: ltr;
            text-size-adjust: 100%;
            -webkit-font-smoothing: antialiased;
            -webkit-tap-highlight-color: transparent;
            margin: 0;
            font-family: Arial,Helvetica,sans-serif;
            display: inline-flex;
            justify-content: center;
            align-items: center;
            box-sizing: border-box;
            background: none;
            outline: none;
            text-transform: lowercase;
            font-weight: 700;
            cursor: pointer;
            position: relative;
            overflow: hidden;
            line-height: normal;
            text-decoration: none;
            font-size: 14px;
            height: 30px;
            width: 100%;
            color: #002f34;
            padding: 0 10px;
            border-radius: 50px;
            box-shadow: 0 2px 8px 0 rgba(0,0,0,.15);
            background-color: #fff;
            border: none;">
        <span style="direction: ltr;
              text-size-adjust: 100%;
              -webkit-font-smoothing: antialiased;
              -webkit-tap-highlight-color: transparent;
              font-family: Arial,Helvetica,sans-serif;
              text-transform: lowercase;
              font-weight: 700;
              cursor: pointer;
              line-height: normal;
              font-size: 10px;
              top:0;
              color: #002f34;" id="kmDialogContent">Back to top</span>
    </button>
</div>
<div class="slider-section">
    <div class="heading">
        <h2>
            <?= __('Top Listings') ?>
        </h2>
        <?= $this->Html->image('mobile/top-games-star.svg') ?>
    </div>

    <div class="slider customer-logos" id="top-litning-products">
    </div>
</div>
<div class="appstore">
    <div class="appstore-box">
        <div class="box-content">
            <h3>
                <?= __('Make Money Selling') ?> <br><?= __('Things You Don\'t Need') ?>.
            </h3>
            <a href="#" style="font-weight: bolder;">
                <?= __('Download Our App') ?>
            </a>
        </div>
        <?php if ($isAndroid) { ?>
            <?= $this->Html->image('mobile/download-on-play-store-png.png', ['style' => 'height:50px', 'url' => $app_info_data->playstore]) ?>
        <?php } else { ?>
            <?= $this->Html->image('mobile/download-on-app-store-png.png', ['style' => 'height:50px', 'url' => $app_info_data->appstore]) ?>
        <?php } ?>
    </div>
</div>
<div class="row-content">

    <div class="row-section">
        <h2>
            <?= __('New') ?>
        </h2>
        <div class="new-items" id="product-list"></div>
        <?= $this->Html->image('mobile/no-product-found.png', ['style' => 'width:100%;display:none;', 'id' => 'no-product-image']) ?>
        <div class="new-item-btn" style="position: fixed;">
            <button class="available-on-mobile">
                <?= __('Sell Your Stuff') ?>
                <?= $this->Html->image('mobile/cam.svg') ?>
            </button>
        </div>
    </div>
    <div class="bottom-btn" style="display: none;" id="load-more-product-btn" onclick="pageContent += 1;loadProduct()">
        <button>
            <?= __('Load more') ?>
        </button>
    </div>

</div>
<script>
    $(document).ready(function() {
        $(window).scroll(function() {
            var body = document.body,
                html = document.documentElement;
            var height = Math.max(body.scrollHeight, body.offsetHeight,
                html.clientHeight, html.scrollHeight, html.offsetHeight);
            if ((height - $(window).scrollTop()) < 651) {
                pageContent += 1;
                loadProduct();
            }
        });
    });
</script>
<script>
    if ($('#kmDialog').length) {
        $(window).on('scroll', function() {
            var previewDialog;
            var cokie = getCookie('localInformation');
            if (cokie) {
                var cokie = JSON.parse(getCookie('localInformation'));
                latitude = cokie.latitude;
                longitude = cokie.longitude;
            } else {
                latitude = 31.7964452;
                longitude = 35.1051475;
            }

            previewDialog = false;
            var scroll = $(window).scrollTop();
            if (scroll > 624) {
                previewDialog = false;
                $('.new-single-items').each(function() {
                    var scrollItem = $(this).position().top;
                    if (scrollItem < scroll) {
                        previewDialog = getDistanceFromLatLonInKm($(this).attr('data-lat'), $(this).attr('data-lng'), latitude, longitude).toFixed(0);
                    }
                });
            }
            if (previewDialog) {
                $('#kmDialog').show();
                if (lang == 'en') {
                    $('#kmDialogContent').html(previewDialog + ' KM From You.');
                } else {
                    $('#kmDialogContent').html(previewDialog + ' KM ק״מ ממך.');
                }
            } else {
                $('#kmDialog').hide();
            }
        });

        function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2 - lat1);
            var dLon = deg2rad(lon2 - lon1);
            var lat1 = deg2rad(lat1);
            var lat2 = deg2rad(lat2);
            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c; // Distance in km
            if (d < 60 || lat2 == '') {
                d = 60;
            }
            return d;
        }

        function deg2rad(deg) {
            return deg * Math.PI / 180;
        }
    }
</script>