<table width="100%" style="background:#fff;min-width:320px" cellspacing="0" cellpadding="0">


    <tbody>
        <tr>
            <td>
                <table width="650" align="center" border="0" style="max-width:650px;margin:0 auto;background-color:#ffffff;border-spacing:0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td style="padding:28px 24px 10px">
                                <table align="center" width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td align="left" style="padding:0 0 24px">
                                                <?= $this->Html->image('logo.png', ['style' => 'width:65px;max-width:65px;font-size:10px;font-family:\'Whyte\',Arial,Helvetica,sans-serif;line-height:14px;background-color:#ffffff;color:#000000;vertical-align:top;text-align:center', 'url' => '/', 'fullBase' => true]) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>



                        <tr>
                            <td>

                                <table align="center" width="476" style="max-width:476px;margin:0 auto" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td align="center" style="font-size:24px;line-height:32px;font-weight:bold;font-family:'Whyte',Arial,Helvetica,sans-serif;color:#000;padding:10px 37px 32px;text-align:center">
                                                <strong>Forgot your password? It happens to the best of us.</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table align="center" width="540" style="max-width:540px;margin:0 auto" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td align="center" style="color:#000000;font-size:18px;line-height:24px;font-family:'Whyte',Arial,Helvetica,sans-serif;text-align:center;padding:10px 24px 20px">
                                                To reset your password, click the button below. The link will self-destruct after three days.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table align="center" width="476" style="max-width:476px;margin:0 auto" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td align="center" style="padding:20px 0 44px">
                                                <table align="center" style="margin:0 auto" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <td align="center" style="background:#fff;color:#000;font-weight:bold;font-size:18px;line-height:22px;font-family:'Whyte',Arial,Helvetica,sans-serif;border:2px solid #000;border-radius:5px">
                                                                <?= $this->Html->link('Reset Your Password', '/reset/' . $user->enc_id, ['style' => 'padding:12px 27px;color:#000;text-decoration:none;display:block', 'fullBase' => true]) ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table align="center" width="540" style="max-width:540px;margin:0 auto" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td align="center" style="color:#000000;font-size:14px;line-height:20px;font-family:'Whyte',Arial,Helvetica,sans-serif;text-align:center;padding:10px 24px 60px">
                                                If you do not want to change your password or didn't request a reset, you can ignore and delete this email.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>