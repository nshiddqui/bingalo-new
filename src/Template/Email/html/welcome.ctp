<table align="center" style="border-spacing:0;width:100%;margin:0;padding:0;border:0">
    <tbody>
        <tr style="border-spacing:0;margin:0;padding:0;border:0">
            <td style="border-spacing:0;margin:0;padding:0;border:0">
                <table align="center" style="border-spacing:0;width:600px;margin:0 auto;padding:0;border:0">
                    <tbody>
                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:0;border:0">
                                <table align="center" style="border-spacing:0;width:600px;margin:0;padding:0;border:0">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:600px;font-size:15px;margin:0;padding:0;border:0" align="center">
                                                <?= $this->Html->image('welcome/logo.png', ['fullBase' => true, 'style' => 'width:600px;display:block', 'url' => '/']) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:0;border:0">
                                <table align="center" style="border-spacing:0;width:600px;text-align:center;margin:0;padding:0;border:0">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center">
                                                <?= $this->Html->image('welcome/congrats.png', ['fullBase' => true, 'style' => 'width:600px;display:block']) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:0;border:0">
                                <table align="center" style="border-spacing:0;width:600px;text-align:center;margin:0;padding:0;border:0">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center">
                                                <?= $this->Html->image('welcome/click.png', ['fullBase' => true, 'style' => 'width:600px;display:block']) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:0;border:0">
                                <table align="center" style="border-spacing:0;width:600px;text-align:center;margin:0;padding:0;border:0">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center">
                                                <?= $this->Html->image('welcome/what happen.png', ['fullBase' => true, 'style' => 'width:600px;display:block']) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:0;border:0">
                                <table align="center" style="border-spacing:0;width:600px;text-align:center;margin:0;padding:0;border:0">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center">
                                                <?= $this->Html->image('welcome/step1.png', ['fullBase' => true, 'style' => 'width:600px;display:block']) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:0;border:0">
                                <table align="center" style="border-spacing:0;width:600px;text-align:center;margin:0;padding:0;border:0">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center">
                                                <?= $this->Html->image('welcome/step2.png', ['fullBase' => true, 'style' => 'width:600px;display:block']) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:0;border:0">
                                <table align="center" style="border-spacing:0;width:600px;text-align:center;margin:0;padding:0;border:0">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center">
                                                <?= $this->Html->image('welcome/step3.png', ['fullBase' => true, 'style' => 'width:600px;display:block']) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:0;border:0">
                                <table align="center" style="border-spacing:0;width:600px;text-align:center;margin:0;padding:0;border:0">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center">
                                                <?= $this->Html->image('welcome/ifwant.png', ['fullBase' => true, 'style' => 'width:600px;display:block']) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:0;border:0">
                                <table align="center" style="border-spacing:0;width:600px;text-align:center;margin:0;padding:0;border:0">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center">
                                                <?= $this->Html->image('welcome/gooffer.png', ['url' => '/', 'style' => 'width:600px;display:block', 'fullBase' => true]) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:0;border:0">
                                <table align="center" style="border-spacing:0;width:600px;text-align:center;margin:0;padding:0;border:0" bgcolor="#ffffff">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center" bgcolor="#ffffff">
                                                <?= $this->Html->image('welcome/team.png', ['url' => '/', 'style' => 'width:600px;display:block', 'fullBase' => true]) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="0" cellspacing="0" style="min-width:320px;border-spacing:0;margin:0;padding:0;border:0" width="100%" bgcolor="#fff">
    <tbody>
        <tr style="border-spacing:0;margin:0;padding:0;border:0">
            <td style="border-spacing:0;margin:0;padding:0;border:0">
                <table align="center" cellpadding="0" cellspacing="0" style="max-width:600px;border-spacing:0;margin:0 auto;padding:0;border:0" width="600">
                    <tbody>
                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:0;border:0">
                                <table style="border-spacing:0;width:600px;text-align:center;margin:0;padding:10px 0px 0px;border:0">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center">
                                                <?= $this->Html->image('welcome/underline.png', ['fullBase' => true, 'style' => 'width:600px;display:block']) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>

                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:0;border:0">
                                <table style="border-spacing:0;width:600px;text-align:center;margin:0;padding:0px 0px 15px;border:0">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center">
                                                <?= $this->Html->image('welcome/vision.png', ['fullBase' => true, 'style' => 'width:600px;display:block']) ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>

                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td align="center" style="border-spacing:0;margin:0;padding:0;border:0">
                                <table style="border-spacing:0;margin:0;padding:0;border:0">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td style="border-spacing:0;width:139px;margin:0;padding:10px;border:0" valign="top" align="center">
                                                <table style="border-spacing:0;margin:0;padding:0;border:0">
                                                    <tbody>
                                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center">
                                                                <?= $this->Html->image('welcome/playstore.png', ['fullBase' => true, 'style' => 'width:139px;height:41px;display:block']) ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td style="border-spacing:0;width:0px;margin:0;padding:0;border:0"></td>
                                            <td style="border-spacing:0;width:139px;margin:0;padding:10px;border:0" valign="top" align="center">
                                                <table style="border-spacing:0;margin:0;padding:0;border:0">
                                                    <tbody>
                                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                                            <td style="border-spacing:0;width:600px;margin:0;padding:0;border:0" align="center">
                                                                <?= $this->Html->image('welcome/appstore.png', ['fullBase' => true, 'style' => 'width:139px;height:41px;display:block']) ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>

                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                            <td style="border-spacing:0;margin:0;padding:15px;border:0">
                                <table align="center" cellpadding="0" cellspacing="0" style="width:100%!important;max-width:324px;border-spacing:0;margin:0 auto;padding:0;border:0" width="324">
                                    <tbody>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td align="center" style="color:#000;border-spacing:0;margin:0;padding:0 0 9px;border:0;font:12px/26px Lato,Helvetica,sans-serif">
                                                <?= $this->Html->link('Terms and Conditions', '/terms', ['style' => 'color:#00ab80;text-decoration:none', 'fullBase' => true]) ?> &nbsp; <span style="font-size:26px;line-height:0;vertical-align:-5px;color:#d0d0cf">•</span>&nbsp; &nbsp; <?= $this->Html->link('Contact Support', '/support', ['style' => 'color:#00ab80;text-decoration:none', 'fullBase' => true]) ?>
                                            </td>
                                        </tr>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td align="center" style="color:#6e6e6f;border-spacing:0;margin:0;padding:0 0 8px;border:0;font:10px/13px Lato,Helvetica,sans-serif">© 2021 Bingalo Inc. All rights reserved</td>
                                        </tr>
                                        <tr style="border-spacing:0;margin:0;padding:0;border:0">
                                            <td align="center" style="color:#6e6e6f;border-spacing:0;margin:0;padding:0;border:0;font:10px/15px Lato,Helvetica,sans-serif">Bingalo Inc. </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>