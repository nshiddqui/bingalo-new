<?php

namespace App\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Event\EventDispatcherTrait;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Inflector;

/**
 * HybridAuth Authenticate
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 */
class HybridAuthAuthenticate extends BaseAuthenticate
{

    use EventDispatcherTrait;

    /**
     * HybridAuth adapter.
     *
     * @var \Hybrid_Provider_Model
     */
    protected $_adapter;

    /**
     * HybridAuth user profile.
     *
     * @var \Hybrid_User_Profile
     */
    protected $_providerProfile;

    /**
     * Whether hybrid auth intialization is done.
     *
     * @var bool
     */
    protected $_initDone = false;

    /**
     * HybridAuth library.
     *
     * @var \Cake\Datasource\RepositoryInterface
     */
    protected $_hybridAuth;

    /**
     * Constructor
     *
     * @param \Cake\Controller\ComponentRegistry $registry The Component registry
     *   used on this request.
     * @param array $config Array of config to use.
     */
    public function __construct(ComponentRegistry $registry, $config)
    {
        Configure::load('hybridauth');
        $this->setConfig([
            'fields' => [
                'provider' => 'provider',
                'openid_identifier' => 'openid_identifier',
                'email' => 'email'
            ],
            'hauth_return_to' => null
        ]);

        parent::__construct($registry, $config);
    }

    /**
     * Initialize HybridAuth and this authenticator.
     *
     * @param \Cake\Network\Request $request Request instance.
     * @return void
     * @throws \RuntimeException Incase case of unknown error.
     */
    protected function _init(Request $request)
    {
        if ($this->_initDone) {
            return;
        }

        $request->getSession()->start();

        $hybridConfig = Configure::read('HybridAuth');

        if (empty($hybridConfig['callback'])) {
            $hybridConfig['callback'] = [
                'controller' => 'authenticated',
                'prefix' => false
            ];
        }
        $hybridConfig['callback'] = Router::url($hybridConfig['callback'], true);

        try {
            $this->_hybridAuth = new \Hybridauth\Hybridauth($hybridConfig);
        } catch (\Exception $e) {
            if ($e->getCode() < 5) {
                throw new \RuntimeException($e->getMessage());
            } else {
                $this->_registry->Auth->flash($e->getMessage());
                $this->_hybridAuth = new \Hybridauth\Hybridauth($hybridConfig);
            }
        }
    }

    /**
     * Get / set hybridauth adapter instance.
     *
     * @param \Hybrid_Provider_Model $adapter Hybrid auth adapter instance
     * @return \Hybrid_Provider_Model|void
     */
    public function adapter($adapter = null)
    {
        if ($adapter === null) {
            return $this->_adapter;
        }

        $this->_adapter = $adapter;
    }


    /**
     * Check if a provider is already connected, return user record if available.
     *
     * @param \Cake\Network\Request $request Request instance.
     * @return array|bool User array on success, false on failure.
     */
    public function getUser(Request $request)
    {
        $this->_init($request);

        $providers = $this->_hybridAuth->getConnectedProviders();
        foreach ($providers as $provider) {
            $adapter = $this->_hybridAuth->getAdapter($provider);
            Configure::write('provider', $provider);
            return $this->_getUser($adapter);
        }

        return false;
    }

    /**
     * Authenticate a user based on the request information.
     *
     * @param \Cake\Network\Request $request Request to get authentication information from.
     * @param \Cake\Network\Response $response A response object that can have headers added.
     * @return array|bool User array on success, false on failure.
     */
    public function authenticate(Request $request, Response $response)
    {
        if ($user = $this->getUser($request)) {
            return $user;
        }

        $provider = $this->_checkProvider($request->getQuery());
        if (!$provider) {
            $provider = $request->getSession()->read('HybridAuth');
            if (!$provider) {
                return false;
            }
        } else {
            $request->getSession()->write('HybridAuth', $provider);
        }

        $adapter =  $this->_hybridAuth->authenticate($provider);
        Configure::write('provider', $provider);
        $request->getSession()->delete('HybridAuth');

        if ($adapter) {
            return $this->_getUser($adapter);
        }

        return false;
    }

    /**
     * Checks whether provider is supplied.
     *
     * @param array $data Data array to check.
     * @return string|bool Provider name if it exists, false if required fields have
     *   not been supplied.
     */
    protected function _checkProvider($data)
    {
        $fields = $this->_config['fields'];
        if (empty($data[$fields['provider']])) {
            return false;
        }

        $provider = $data[$fields['provider']];

        if ($provider === 'OpenID' && empty($data[$fields['openid_identifier']])) {
            return false;
        }

        return $provider;
    }

    /**
     * Get user record for HybridAuth adapter and try to get associated user record
     * from your application's database.
     *
     * If app user record is not found a 'HybridAuth.newUser' event is dispatched
     * with profile info from HyridAuth. The event listener should create associated
     * user record and return user entity as event result.
     *
     * @param \Hybrid_Provider_Model $adapter Hybrid auth adapter instance.
     * @return array User record
     * @throws \Exception Thrown when a profile cannot be retrieved.
     * @throws \RuntimeException If profile entity cannot be persisted.
     */
    protected function _getUser($adapter)
    {
        try {
            $providerProfile = $adapter->getUserProfile();
            $this->adapter($adapter);
        } catch (\Exception $e) {
            $adapter->logout();
            throw $e;
        }
        return $providerProfile;
    }

    /**
     * Logout all providers
     *
     * @param \Cake\Event\Event $event Event.
     * @param array $user The user about to be logged out.
     * @return void
     */
    public function logout(Event $event, array $user)
    {
        $this->_init($event->subject()->request);
        $this->_hybridAuth->disconnectAllAdapters();
    }

    /**
     * Returns a list of all events that this authenticate class will listen to.
     *
     * @return array List of events this class listens to.
     */
    public function implementedEvents()
    {
        return ['Auth.logout' => 'logout'];
    }
}
