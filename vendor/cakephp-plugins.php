<?php
$baseDir = dirname(dirname(__FILE__));

return [
    'plugins' => [
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'DataTables' => $baseDir . '/vendor/allanmcarvalho/cakephp-datatables/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'MailgunEmail' => $baseDir . '/vendor/motsmanish/cakephp-mailgun/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'WyriHaximus/TwigView' => $baseDir . '/vendor/wyrihaximus/twig-view/',
    ],
];
